#!/bin/bash

sudo hostname www.quantumcommunities.cea-nj.org
bundle exec thin start --ssl --ssl-key-file ../privkey.pem --ssl-cert-file ../cert.pem -p 4000 &


