require "application_system_test_case"

class HourlyratesTest < ApplicationSystemTestCase
  setup do
    @hourlyrate = hourlyrates(:one)
  end

  test "visiting the index" do
    visit hourlyrates_url
    assert_selector "h1", text: "Hourlyrates"
  end

  test "creating a Hourlyrate" do
    visit hourlyrates_url
    click_on "New Hourlyrate"

    fill_in "Client", with: @hourlyrate.client
    fill_in "Enddate", with: @hourlyrate.enddate
    fill_in "Hourlyrate", with: @hourlyrate.hourlyrate
    fill_in "Startdate", with: @hourlyrate.startdate
    click_on "Create Hourlyrate"

    assert_text "Hourlyrate was successfully created"
    click_on "Back"
  end

  test "updating a Hourlyrate" do
    visit hourlyrates_url
    click_on "Edit", match: :first

    fill_in "Client", with: @hourlyrate.client
    fill_in "Enddate", with: @hourlyrate.enddate
    fill_in "Hourlyrate", with: @hourlyrate.hourlyrate
    fill_in "Startdate", with: @hourlyrate.startdate
    click_on "Update Hourlyrate"

    assert_text "Hourlyrate was successfully updated"
    click_on "Back"
  end

  test "destroying a Hourlyrate" do
    visit hourlyrates_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Hourlyrate was successfully destroyed"
  end
end
