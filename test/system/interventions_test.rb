require "application_system_test_case"

class InterventionsTest < ApplicationSystemTestCase
  setup do
    @intervention = interventions(:one)
  end

  test "visiting the index" do
    visit interventions_url
    assert_selector "h1", text: "Interventions"
  end

  test "creating a Intervention" do
    visit interventions_url
    click_on "New Intervention"

    fill_in "Activityconductedid", with: @intervention.ActivityConductedID
    fill_in "Employmenthistoryid", with: @intervention.EmploymentHistoryID
    fill_in "Clientprogress", with: @intervention.clientProgress
    fill_in "Dateofintervention", with: @intervention.dateOfIntervention
    fill_in "Employeeid", with: @intervention.employeeID
    fill_in "Employerid", with: @intervention.employerID
    fill_in "Endtime", with: @intervention.endTime
    fill_in "Generalcomments", with: @intervention.generalComments
    fill_in "Interventiondescription", with: @intervention.interventionDescription
    fill_in "Interventionid", with: @intervention.interventionID
    fill_in "Jobrequirements", with: @intervention.jobRequirements
    fill_in "Preplacementactivityid", with: @intervention.prePlacementActivityID
    fill_in "Skillsstatus", with: @intervention.skillsStatus
    fill_in "Starttime", with: @intervention.startTime
    fill_in "Workauthorizationid", with: @intervention.workAuthorizationID
    click_on "Create Intervention"

    assert_text "Intervention was successfully created"
    click_on "Back"
  end

  test "updating a Intervention" do
    visit interventions_url
    click_on "Edit", match: :first

    fill_in "Activityconductedid", with: @intervention.ActivityConductedID
    fill_in "Employmenthistoryid", with: @intervention.EmploymentHistoryID
    fill_in "Clientprogress", with: @intervention.clientProgress
    fill_in "Dateofintervention", with: @intervention.dateOfIntervention
    fill_in "Employeeid", with: @intervention.employeeID
    fill_in "Employerid", with: @intervention.employerID
    fill_in "Endtime", with: @intervention.endTime
    fill_in "Generalcomments", with: @intervention.generalComments
    fill_in "Interventiondescription", with: @intervention.interventionDescription
    fill_in "Interventionid", with: @intervention.interventionID
    fill_in "Jobrequirements", with: @intervention.jobRequirements
    fill_in "Preplacementactivityid", with: @intervention.prePlacementActivityID
    fill_in "Skillsstatus", with: @intervention.skillsStatus
    fill_in "Starttime", with: @intervention.startTime
    fill_in "Workauthorizationid", with: @intervention.workAuthorizationID
    click_on "Update Intervention"

    assert_text "Intervention was successfully updated"
    click_on "Back"
  end

  test "destroying a Intervention" do
    visit interventions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Intervention was successfully destroyed"
  end
end
