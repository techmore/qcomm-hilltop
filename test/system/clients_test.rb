require "application_system_test_case"

class ClientsTest < ApplicationSystemTestCase
  setup do
    @client = clients(:one)
  end

  test "visiting the index" do
    visit clients_url
    assert_selector "h1", text: "Clients"
  end

  test "creating a Client" do
    visit clients_url
    click_on "New Client"

    fill_in "Clientasl", with: @client.ClientASL
    fill_in "Clientallergies", with: @client.ClientAllergies
    fill_in "Clientcommmethod", with: @client.ClientCommMethod
    fill_in "Clienteducationlevel", with: @client.ClientEducationLevel
    fill_in "Clientfahours", with: @client.ClientFAHours
    fill_in "Clientfastartdate", with: @client.ClientFAStartDate
    fill_in "Clientgender", with: @client.ClientGender
    fill_in "Clientlivingarrangment", with: @client.ClientLivingArrangment
    fill_in "Clientmedicalinformation", with: @client.ClientMedicalInformation
    fill_in "Clientnotes", with: @client.ClientNotes
    fill_in "Clientprimaryimpairment", with: @client.ClientPrimaryImpairment
    fill_in "Clientprimarymhdisability", with: @client.ClientPrimaryMHDisability
    fill_in "Clientprimarynonmhdisability", with: @client.ClientPrimaryNonMHDisability
    fill_in "Clientprimarysource", with: @client.ClientPrimarySource
    fill_in "Clientprogram", with: @client.ClientProgram
    fill_in "Clientrace", with: @client.ClientRace
    fill_in "Clientreferraldate", with: @client.ClientReferralDate
    fill_in "Clientreferredby", with: @client.ClientReferredBy
    fill_in "Clientresume", with: @client.ClientResume
    fill_in "Clientsecondaryimpairment", with: @client.ClientSecondaryImpairment
    fill_in "Clientsecondarymhdisability", with: @client.ClientSecondaryMHDisability
    fill_in "Clientsecondarynonmhdisability", with: @client.ClientSecondaryNonMHDisability
    fill_in "Clientsecondarysource", with: @client.ClientSecondarySource
    fill_in "Clientservicestopdate", with: @client.ClientServiceStopDate
    fill_in "Clientstartdate", with: @client.ClientStartDate
    fill_in "Clienttypeoftermination", with: @client.ClientTypeOfTermination
    fill_in "Criminalrecord", with: @client.CriminalRecord
    fill_in "Criminalrecorddetail", with: @client.CriminalRecordDetail
    fill_in "Dddserialnbr", with: @client.DDDSerialNbr
    fill_in "Dvrscaseidnbr", with: @client.DVRSCaseIDNbr
    fill_in "Emergencycontact1cell", with: @client.EmergencyContact1Cell
    fill_in "Emergencycontact1home", with: @client.EmergencyContact1Home
    fill_in "Emergencycontact1name", with: @client.EmergencyContact1Name
    fill_in "Emergencycontact1relationship", with: @client.EmergencyContact1Relationship
    fill_in "Emergencycontact1work", with: @client.EmergencyContact1Work
    fill_in "Emergencycontact1workext", with: @client.EmergencyContact1WorkExt
    fill_in "Emergencycontact2cell", with: @client.EmergencyContact2Cell
    fill_in "Emergencycontact2home", with: @client.EmergencyContact2Home
    fill_in "Emergencycontact2name", with: @client.EmergencyContact2Name
    fill_in "Emergencycontact2relationship", with: @client.EmergencyContact2Relationship
    fill_in "Emergencycontact2work", with: @client.EmergencyContact2Work
    fill_in "Emergencycontact2workext", with: @client.EmergencyContact2WorkExt
    fill_in "Googledrive", with: @client.Googledrive
    fill_in "Guardianaddress1", with: @client.GuardianAddress1
    fill_in "Guardianaddress2", with: @client.GuardianAddress2
    fill_in "Guardiancell", with: @client.GuardianCell
    fill_in "Guardiancity", with: @client.GuardianCity
    fill_in "Guardianhome", with: @client.GuardianHome
    fill_in "Guardianname", with: @client.GuardianName
    fill_in "Guardianstate", with: @client.GuardianState
    fill_in "Guardianwork", with: @client.GuardianWork
    fill_in "Guardianworkext", with: @client.GuardianWorkExt
    fill_in "Guardianzip", with: @client.GuardianZip
    fill_in "Guardinarelationship", with: @client.GuardinaRelationship
    fill_in "Referralphoneext", with: @client.ReferralPhoneExt
    fill_in "Referralsourcenumber", with: @client.ReferralSourceNumber
    fill_in "Resume", with: @client.Resume
    fill_in "Clientaddress1", with: @client.clientAddress1
    fill_in "Clientaddress2", with: @client.clientAddress2
    fill_in "Clientcity", with: @client.clientCity
    fill_in "Clientcounty", with: @client.clientCounty
    fill_in "Clientdob", with: @client.clientDOB
    fill_in "Clientemail", with: @client.clientEmail
    fill_in "Clientfirstname", with: @client.clientFirstName
    fill_in "Clienthomephone", with: @client.clientHomePhone
    fill_in "Clientid", with: @client.clientID
    fill_in "Clientlastname", with: @client.clientLastName
    fill_in "Clientmiddlename", with: @client.clientMiddleName
    fill_in "Clientmobilephone", with: @client.clientMobilePhone
    fill_in "Clientssn", with: @client.clientSSN
    fill_in "Clientstate", with: @client.clientState
    fill_in "Clientworkphone", with: @client.clientWorkPhone
    fill_in "Clientworkphoneext", with: @client.clientWorkPhoneExt
    fill_in "Clientzip", with: @client.clientZip
    click_on "Create Client"

    assert_text "Client was successfully created"
    click_on "Back"
  end

  test "updating a Client" do
    visit clients_url
    click_on "Edit", match: :first

    fill_in "Clientasl", with: @client.ClientASL
    fill_in "Clientallergies", with: @client.ClientAllergies
    fill_in "Clientcommmethod", with: @client.ClientCommMethod
    fill_in "Clienteducationlevel", with: @client.ClientEducationLevel
    fill_in "Clientfahours", with: @client.ClientFAHours
    fill_in "Clientfastartdate", with: @client.ClientFAStartDate
    fill_in "Clientgender", with: @client.ClientGender
    fill_in "Clientlivingarrangment", with: @client.ClientLivingArrangment
    fill_in "Clientmedicalinformation", with: @client.ClientMedicalInformation
    fill_in "Clientnotes", with: @client.ClientNotes
    fill_in "Clientprimaryimpairment", with: @client.ClientPrimaryImpairment
    fill_in "Clientprimarymhdisability", with: @client.ClientPrimaryMHDisability
    fill_in "Clientprimarynonmhdisability", with: @client.ClientPrimaryNonMHDisability
    fill_in "Clientprimarysource", with: @client.ClientPrimarySource
    fill_in "Clientprogram", with: @client.ClientProgram
    fill_in "Clientrace", with: @client.ClientRace
    fill_in "Clientreferraldate", with: @client.ClientReferralDate
    fill_in "Clientreferredby", with: @client.ClientReferredBy
    fill_in "Clientresume", with: @client.ClientResume
    fill_in "Clientsecondaryimpairment", with: @client.ClientSecondaryImpairment
    fill_in "Clientsecondarymhdisability", with: @client.ClientSecondaryMHDisability
    fill_in "Clientsecondarynonmhdisability", with: @client.ClientSecondaryNonMHDisability
    fill_in "Clientsecondarysource", with: @client.ClientSecondarySource
    fill_in "Clientservicestopdate", with: @client.ClientServiceStopDate
    fill_in "Clientstartdate", with: @client.ClientStartDate
    fill_in "Clienttypeoftermination", with: @client.ClientTypeOfTermination
    fill_in "Criminalrecord", with: @client.CriminalRecord
    fill_in "Criminalrecorddetail", with: @client.CriminalRecordDetail
    fill_in "Dddserialnbr", with: @client.DDDSerialNbr
    fill_in "Dvrscaseidnbr", with: @client.DVRSCaseIDNbr
    fill_in "Emergencycontact1cell", with: @client.EmergencyContact1Cell
    fill_in "Emergencycontact1home", with: @client.EmergencyContact1Home
    fill_in "Emergencycontact1name", with: @client.EmergencyContact1Name
    fill_in "Emergencycontact1relationship", with: @client.EmergencyContact1Relationship
    fill_in "Emergencycontact1work", with: @client.EmergencyContact1Work
    fill_in "Emergencycontact1workext", with: @client.EmergencyContact1WorkExt
    fill_in "Emergencycontact2cell", with: @client.EmergencyContact2Cell
    fill_in "Emergencycontact2home", with: @client.EmergencyContact2Home
    fill_in "Emergencycontact2name", with: @client.EmergencyContact2Name
    fill_in "Emergencycontact2relationship", with: @client.EmergencyContact2Relationship
    fill_in "Emergencycontact2work", with: @client.EmergencyContact2Work
    fill_in "Emergencycontact2workext", with: @client.EmergencyContact2WorkExt
    fill_in "Googledrive", with: @client.Googledrive
    fill_in "Guardianaddress1", with: @client.GuardianAddress1
    fill_in "Guardianaddress2", with: @client.GuardianAddress2
    fill_in "Guardiancell", with: @client.GuardianCell
    fill_in "Guardiancity", with: @client.GuardianCity
    fill_in "Guardianhome", with: @client.GuardianHome
    fill_in "Guardianname", with: @client.GuardianName
    fill_in "Guardianstate", with: @client.GuardianState
    fill_in "Guardianwork", with: @client.GuardianWork
    fill_in "Guardianworkext", with: @client.GuardianWorkExt
    fill_in "Guardianzip", with: @client.GuardianZip
    fill_in "Guardinarelationship", with: @client.GuardinaRelationship
    fill_in "Referralphoneext", with: @client.ReferralPhoneExt
    fill_in "Referralsourcenumber", with: @client.ReferralSourceNumber
    fill_in "Resume", with: @client.Resume
    fill_in "Clientaddress1", with: @client.clientAddress1
    fill_in "Clientaddress2", with: @client.clientAddress2
    fill_in "Clientcity", with: @client.clientCity
    fill_in "Clientcounty", with: @client.clientCounty
    fill_in "Clientdob", with: @client.clientDOB
    fill_in "Clientemail", with: @client.clientEmail
    fill_in "Clientfirstname", with: @client.clientFirstName
    fill_in "Clienthomephone", with: @client.clientHomePhone
    fill_in "Clientid", with: @client.clientID
    fill_in "Clientlastname", with: @client.clientLastName
    fill_in "Clientmiddlename", with: @client.clientMiddleName
    fill_in "Clientmobilephone", with: @client.clientMobilePhone
    fill_in "Clientssn", with: @client.clientSSN
    fill_in "Clientstate", with: @client.clientState
    fill_in "Clientworkphone", with: @client.clientWorkPhone
    fill_in "Clientworkphoneext", with: @client.clientWorkPhoneExt
    fill_in "Clientzip", with: @client.clientZip
    click_on "Update Client"

    assert_text "Client was successfully updated"
    click_on "Back"
  end

  test "destroying a Client" do
    visit clients_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Client was successfully destroyed"
  end
end
