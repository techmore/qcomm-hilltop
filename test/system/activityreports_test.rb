require "application_system_test_case"

class ActivityreportsTest < ApplicationSystemTestCase
  setup do
    @activityreport = activityreports(:one)
  end

  test "visiting the index" do
    visit activityreports_url
    assert_selector "h1", text: "Activityreports"
  end

  test "creating a Activityreport" do
    visit activityreports_url
    click_on "New Activityreport"

    fill_in "Activities", with: @activityreport.activities
    fill_in "Areas", with: @activityreport.areas
    fill_in "Authid", with: @activityreport.authid
    fill_in "Clientid", with: @activityreport.clientid
    fill_in "Comments", with: @activityreport.comments
    fill_in "Completed by", with: @activityreport.completed_by
    fill_in "Date", with: @activityreport.date
    fill_in "Date medical start", with: @activityreport.date_medical_start
    fill_in "Dvrs office", with: @activityreport.dvrs_office
    fill_in "Emp address", with: @activityreport.emp_address
    fill_in "Emp name", with: @activityreport.emp_name
    fill_in "Emp start", with: @activityreport.emp_start
    fill_in "Emp sup", with: @activityreport.emp_sup
    fill_in "Hours provided", with: @activityreport.hours_provided
    fill_in "Hours wanted", with: @activityreport.hours_wanted
    fill_in "Invoice", with: @activityreport.invoice
    fill_in "Job goal", with: @activityreport.job_goal
    fill_in "Job title", with: @activityreport.job_title
    fill_in "Med provided", with: @activityreport.med_provided
    fill_in "Medical", with: @activityreport.medical
    fill_in "Name", with: @activityreport.name
    fill_in "Option1", with: @activityreport.option1
    fill_in "Option2", with: @activityreport.option2
    fill_in "Option3", with: @activityreport.option3
    fill_in "Pp", with: @activityreport.pp
    fill_in "Reporting period", with: @activityreport.reporting_period
    fill_in "Requested hours", with: @activityreport.requested_hours
    fill_in "Service", with: @activityreport.service
    fill_in "Telephone", with: @activityreport.telephone
    fill_in "Voucher num", with: @activityreport.voucher_num
    fill_in "Wages", with: @activityreport.wages
    fill_in "Work schedule", with: @activityreport.work_schedule
    click_on "Create Activityreport"

    assert_text "Activityreport was successfully created"
    click_on "Back"
  end

  test "updating a Activityreport" do
    visit activityreports_url
    click_on "Edit", match: :first

    fill_in "Activities", with: @activityreport.activities
    fill_in "Areas", with: @activityreport.areas
    fill_in "Authid", with: @activityreport.authid
    fill_in "Clientid", with: @activityreport.clientid
    fill_in "Comments", with: @activityreport.comments
    fill_in "Completed by", with: @activityreport.completed_by
    fill_in "Date", with: @activityreport.date
    fill_in "Date medical start", with: @activityreport.date_medical_start
    fill_in "Dvrs office", with: @activityreport.dvrs_office
    fill_in "Emp address", with: @activityreport.emp_address
    fill_in "Emp name", with: @activityreport.emp_name
    fill_in "Emp start", with: @activityreport.emp_start
    fill_in "Emp sup", with: @activityreport.emp_sup
    fill_in "Hours provided", with: @activityreport.hours_provided
    fill_in "Hours wanted", with: @activityreport.hours_wanted
    fill_in "Invoice", with: @activityreport.invoice
    fill_in "Job goal", with: @activityreport.job_goal
    fill_in "Job title", with: @activityreport.job_title
    fill_in "Med provided", with: @activityreport.med_provided
    fill_in "Medical", with: @activityreport.medical
    fill_in "Name", with: @activityreport.name
    fill_in "Option1", with: @activityreport.option1
    fill_in "Option2", with: @activityreport.option2
    fill_in "Option3", with: @activityreport.option3
    fill_in "Pp", with: @activityreport.pp
    fill_in "Reporting period", with: @activityreport.reporting_period
    fill_in "Requested hours", with: @activityreport.requested_hours
    fill_in "Service", with: @activityreport.service
    fill_in "Telephone", with: @activityreport.telephone
    fill_in "Voucher num", with: @activityreport.voucher_num
    fill_in "Wages", with: @activityreport.wages
    fill_in "Work schedule", with: @activityreport.work_schedule
    click_on "Update Activityreport"

    assert_text "Activityreport was successfully updated"
    click_on "Back"
  end

  test "destroying a Activityreport" do
    visit activityreports_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Activityreport was successfully destroyed"
  end
end
