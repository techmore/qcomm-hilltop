require "application_system_test_case"

class TwersTest < ApplicationSystemTestCase
  setup do
    @twer = twers(:one)
  end

  test "visiting the index" do
    visit twers_url
    assert_selector "h1", text: "Twers"
  end

  test "creating a Twer" do
    visit twers_url
    click_on "New Twer"

    fill_in "C1", with: @twer.c1
    fill_in "C2", with: @twer.c2
    fill_in "C3", with: @twer.c3
    fill_in "C4", with: @twer.c4
    fill_in "Client", with: @twer.client
    fill_in "Coachsig", with: @twer.coachsig
    fill_in "Days", with: @twer.days
    fill_in "Duties", with: @twer.duties
    fill_in "Employer", with: @twer.employer
    fill_in "Final", with: @twer.final
    fill_in "Hours", with: @twer.hours
    fill_in "Q1", with: @twer.q1
    fill_in "Q2", with: @twer.q2
    fill_in "Q3", with: @twer.q3
    fill_in "Q4", with: @twer.q4
    fill_in "Sidgdate", with: @twer.sidgdate
    fill_in "Ss1", with: @twer.ss1
    fill_in "Ss2", with: @twer.ss2
    fill_in "Ss3", with: @twer.ss3
    fill_in "Ss4", with: @twer.ss4
    fill_in "Ss5", with: @twer.ss5
    fill_in "Ss6", with: @twer.ss6
    fill_in "Ss7", with: @twer.ss7
    fill_in "Ss8", with: @twer.ss8
    fill_in "Week1", with: @twer.week1
    fill_in "Wp1", with: @twer.wp1
    fill_in "Wp2", with: @twer.wp2
    fill_in "Wp3", with: @twer.wp3
    fill_in "Wp4", with: @twer.wp4
    fill_in "Wp5", with: @twer.wp5
    fill_in "Wp6", with: @twer.wp6
    fill_in "Wp7", with: @twer.wp7
    click_on "Create Twer"

    assert_text "Twer was successfully created"
    click_on "Back"
  end

  test "updating a Twer" do
    visit twers_url
    click_on "Edit", match: :first

    fill_in "C1", with: @twer.c1
    fill_in "C2", with: @twer.c2
    fill_in "C3", with: @twer.c3
    fill_in "C4", with: @twer.c4
    fill_in "Client", with: @twer.client
    fill_in "Coachsig", with: @twer.coachsig
    fill_in "Days", with: @twer.days
    fill_in "Duties", with: @twer.duties
    fill_in "Employer", with: @twer.employer
    fill_in "Final", with: @twer.final
    fill_in "Hours", with: @twer.hours
    fill_in "Q1", with: @twer.q1
    fill_in "Q2", with: @twer.q2
    fill_in "Q3", with: @twer.q3
    fill_in "Q4", with: @twer.q4
    fill_in "Sidgdate", with: @twer.sidgdate
    fill_in "Ss1", with: @twer.ss1
    fill_in "Ss2", with: @twer.ss2
    fill_in "Ss3", with: @twer.ss3
    fill_in "Ss4", with: @twer.ss4
    fill_in "Ss5", with: @twer.ss5
    fill_in "Ss6", with: @twer.ss6
    fill_in "Ss7", with: @twer.ss7
    fill_in "Ss8", with: @twer.ss8
    fill_in "Week1", with: @twer.week1
    fill_in "Wp1", with: @twer.wp1
    fill_in "Wp2", with: @twer.wp2
    fill_in "Wp3", with: @twer.wp3
    fill_in "Wp4", with: @twer.wp4
    fill_in "Wp5", with: @twer.wp5
    fill_in "Wp6", with: @twer.wp6
    fill_in "Wp7", with: @twer.wp7
    click_on "Update Twer"

    assert_text "Twer was successfully updated"
    click_on "Back"
  end

  test "destroying a Twer" do
    visit twers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Twer was successfully destroyed"
  end
end
