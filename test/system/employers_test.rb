require "application_system_test_case"

class EmployersTest < ApplicationSystemTestCase
  setup do
    @employer = employers(:one)
  end

  test "visiting the index" do
    visit employers_url
    assert_selector "h1", text: "Employers"
  end

  test "creating a Employer" do
    visit employers_url
    click_on "New Employer"

    fill_in "Employeraddress1", with: @employer.employerAddress1
    fill_in "Employeraddress2", with: @employer.employerAddress2
    fill_in "Employercity", with: @employer.employerCity
    fill_in "Employercompanymaincontact", with: @employer.employerCompanyMainContact
    fill_in "Employercounty", with: @employer.employerCounty
    fill_in "Employeremail", with: @employer.employerEmail
    fill_in "Employerfaxnumber", with: @employer.employerFaxNumber
    fill_in "Employerid", with: @employer.employerID
    fill_in "Employername", with: @employer.employerName
    fill_in "Employerstate", with: @employer.employerState
    fill_in "Employerworkphone", with: @employer.employerWorkPhone
    fill_in "Employerzip", with: @employer.employerZip
    click_on "Create Employer"

    assert_text "Employer was successfully created"
    click_on "Back"
  end

  test "updating a Employer" do
    visit employers_url
    click_on "Edit", match: :first

    fill_in "Employeraddress1", with: @employer.employerAddress1
    fill_in "Employeraddress2", with: @employer.employerAddress2
    fill_in "Employercity", with: @employer.employerCity
    fill_in "Employercompanymaincontact", with: @employer.employerCompanyMainContact
    fill_in "Employercounty", with: @employer.employerCounty
    fill_in "Employeremail", with: @employer.employerEmail
    fill_in "Employerfaxnumber", with: @employer.employerFaxNumber
    fill_in "Employerid", with: @employer.employerID
    fill_in "Employername", with: @employer.employerName
    fill_in "Employerstate", with: @employer.employerState
    fill_in "Employerworkphone", with: @employer.employerWorkPhone
    fill_in "Employerzip", with: @employer.employerZip
    click_on "Update Employer"

    assert_text "Employer was successfully updated"
    click_on "Back"
  end

  test "destroying a Employer" do
    visit employers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Employer was successfully destroyed"
  end
end
