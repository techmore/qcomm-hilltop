require "application_system_test_case"

class AuthorizationsTest < ApplicationSystemTestCase
  setup do
    @authorization = authorizations(:one)
  end

  test "visiting the index" do
    visit authorizations_url
    assert_selector "h1", text: "Authorizations"
  end

  test "creating a Authorization" do
    visit authorizations_url
    click_on "New Authorization"

    fill_in "Flatratevoucher", with: @authorization.FlatRateVoucher
    fill_in "Vouchercompleted", with: @authorization.VoucherCompleted
    fill_in "Voucherfacounty", with: @authorization.VoucherFACounty
    fill_in "Vouchergoal", with: @authorization.VoucherGoal
    fill_in "Voucherinvoicenumber", with: @authorization.VoucherInvoiceNumber
    fill_in "Voucherrate", with: @authorization.VoucherRate
    fill_in "Voucherreceiveddate", with: @authorization.VoucherReceivedDate
    fill_in "Voucherstatus", with: @authorization.VoucherStatus
    fill_in "Clientid", with: @authorization.clientID
    fill_in "Counselorname", with: @authorization.counselorName
    fill_in "Funderid", with: @authorization.funderID
    fill_in "Hoursauthorized", with: @authorization.hoursAuthorized
    fill_in "Hoursremaining", with: @authorization.hoursRemaining
    fill_in "Servicetype", with: @authorization.serviceType
    fill_in "Totalhourssupported", with: @authorization.totalHoursSupported
    fill_in "Voucheramountinvoiced", with: @authorization.voucherAmountInvoiced
    fill_in "Voucherinvoiceadjustment", with: @authorization.voucherInvoiceAdjustment
    fill_in "Voucherinvoicedate", with: @authorization.voucherInvoiceDate
    fill_in "Vouchernumber", with: @authorization.voucherNumber
    fill_in "Voucherorauthorizationdate", with: @authorization.voucherOrAuthorizationDate
    fill_in "Voucherpaymentcomment", with: @authorization.voucherPaymentComment
    fill_in "Voucherpaymentreceived", with: @authorization.voucherPaymentReceived
    fill_in "Voucherpaymentreceiveddate", with: @authorization.voucherPaymentReceivedDate
    click_on "Create Authorization"

    assert_text "Authorization was successfully created"
    click_on "Back"
  end

  test "updating a Authorization" do
    visit authorizations_url
    click_on "Edit", match: :first

    fill_in "Flatratevoucher", with: @authorization.FlatRateVoucher
    fill_in "Vouchercompleted", with: @authorization.VoucherCompleted
    fill_in "Voucherfacounty", with: @authorization.VoucherFACounty
    fill_in "Vouchergoal", with: @authorization.VoucherGoal
    fill_in "Voucherinvoicenumber", with: @authorization.VoucherInvoiceNumber
    fill_in "Voucherrate", with: @authorization.VoucherRate
    fill_in "Voucherreceiveddate", with: @authorization.VoucherReceivedDate
    fill_in "Voucherstatus", with: @authorization.VoucherStatus
    fill_in "Clientid", with: @authorization.clientID
    fill_in "Counselorname", with: @authorization.counselorName
    fill_in "Funderid", with: @authorization.funderID
    fill_in "Hoursauthorized", with: @authorization.hoursAuthorized
    fill_in "Hoursremaining", with: @authorization.hoursRemaining
    fill_in "Servicetype", with: @authorization.serviceType
    fill_in "Totalhourssupported", with: @authorization.totalHoursSupported
    fill_in "Voucheramountinvoiced", with: @authorization.voucherAmountInvoiced
    fill_in "Voucherinvoiceadjustment", with: @authorization.voucherInvoiceAdjustment
    fill_in "Voucherinvoicedate", with: @authorization.voucherInvoiceDate
    fill_in "Vouchernumber", with: @authorization.voucherNumber
    fill_in "Voucherorauthorizationdate", with: @authorization.voucherOrAuthorizationDate
    fill_in "Voucherpaymentcomment", with: @authorization.voucherPaymentComment
    fill_in "Voucherpaymentreceived", with: @authorization.voucherPaymentReceived
    fill_in "Voucherpaymentreceiveddate", with: @authorization.voucherPaymentReceivedDate
    click_on "Update Authorization"

    assert_text "Authorization was successfully updated"
    click_on "Back"
  end

  test "destroying a Authorization" do
    visit authorizations_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Authorization was successfully destroyed"
  end
end
