require "application_system_test_case"

class EestatuscodesTest < ApplicationSystemTestCase
  setup do
    @eestatuscode = eestatuscodes(:one)
  end

  test "visiting the index" do
    visit eestatuscodes_url
    assert_selector "h1", text: "Eestatuscodes"
  end

  test "creating a Eestatuscode" do
    visit eestatuscodes_url
    click_on "New Eestatuscode"

    fill_in "Client", with: @eestatuscode.client
    fill_in "Enddate", with: @eestatuscode.enddate
    fill_in "Startdate", with: @eestatuscode.startdate
    fill_in "Statuscode", with: @eestatuscode.statuscode
    click_on "Create Eestatuscode"

    assert_text "Eestatuscode was successfully created"
    click_on "Back"
  end

  test "updating a Eestatuscode" do
    visit eestatuscodes_url
    click_on "Edit", match: :first

    fill_in "Client", with: @eestatuscode.client
    fill_in "Enddate", with: @eestatuscode.enddate
    fill_in "Startdate", with: @eestatuscode.startdate
    fill_in "Statuscode", with: @eestatuscode.statuscode
    click_on "Update Eestatuscode"

    assert_text "Eestatuscode was successfully updated"
    click_on "Back"
  end

  test "destroying a Eestatuscode" do
    visit eestatuscodes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Eestatuscode was successfully destroyed"
  end
end
