require "application_system_test_case"

class VotltfasTest < ApplicationSystemTestCase
  setup do
    @votltfa = votltfas(:one)
  end

  test "visiting the index" do
    visit votltfas_url
    assert_selector "h1", text: "Votltfas"
  end

  test "creating a Votltfa" do
    visit votltfas_url
    click_on "New Votltfa"

    fill_in "Client", with: @votltfa.client
    fill_in "Date", with: @votltfa.date
    fill_in "Empname", with: @votltfa.empname
    fill_in "Empsig", with: @votltfa.empsig
    fill_in "Provider", with: @votltfa.provider
    fill_in "Providerstaff", with: @votltfa.providerstaff
    fill_in "Rate", with: @votltfa.rate
    fill_in "Reason", with: @votltfa.reason
    click_on "Create Votltfa"

    assert_text "Votltfa was successfully created"
    click_on "Back"
  end

  test "updating a Votltfa" do
    visit votltfas_url
    click_on "Edit", match: :first

    fill_in "Client", with: @votltfa.client
    fill_in "Date", with: @votltfa.date
    fill_in "Empname", with: @votltfa.empname
    fill_in "Empsig", with: @votltfa.empsig
    fill_in "Provider", with: @votltfa.provider
    fill_in "Providerstaff", with: @votltfa.providerstaff
    fill_in "Rate", with: @votltfa.rate
    fill_in "Reason", with: @votltfa.reason
    click_on "Update Votltfa"

    assert_text "Votltfa was successfully updated"
    click_on "Back"
  end

  test "destroying a Votltfa" do
    visit votltfas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Votltfa was successfully destroyed"
  end
end
