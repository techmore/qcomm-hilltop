require "application_system_test_case"

class OtfcompletionsTest < ApplicationSystemTestCase
  setup do
    @otfcompletion = otfcompletions(:one)
  end

  test "visiting the index" do
    visit otfcompletions_url
    assert_selector "h1", text: "Otfcompletions"
  end

  test "creating a Otfcompletion" do
    visit otfcompletions_url
    click_on "New Otfcompletion"

    fill_in "Address1", with: @otfcompletion.Address1
    fill_in "Address2", with: @otfcompletion.Address2
    fill_in "Address3", with: @otfcompletion.Address3
    fill_in "Dates1", with: @otfcompletion.Dates1
    fill_in "Dates2", with: @otfcompletion.Dates2
    fill_in "Dates3", with: @otfcompletion.Dates3
    fill_in "Jobsperformed1", with: @otfcompletion.JobsPerformed1
    fill_in "Jobsperformed2", with: @otfcompletion.JobsPerformed2
    fill_in "Jobsperformed3", with: @otfcompletion.JobsPerformed3
    fill_in "Agency", with: @otfcompletion.agency
    fill_in "Authid", with: @otfcompletion.authid
    fill_in "Avehoursperday1", with: @otfcompletion.aveHoursPerDay1
    fill_in "Avehoursperday2", with: @otfcompletion.aveHoursPerDay2
    fill_in "Avehoursperday3", with: @otfcompletion.aveHoursPerDay3
    fill_in "Comments", with: @otfcompletion.comments
    fill_in "Formtype", with: @otfcompletion.formtype
    fill_in "Numberofdays1", with: @otfcompletion.numberOfDays1
    fill_in "Numberofdays2", with: @otfcompletion.numberOfDays2
    fill_in "Numberofdays3", with: @otfcompletion.numberOfDays3
    fill_in "Q1", with: @otfcompletion.q1
    fill_in "Q2", with: @otfcompletion.q2
    fill_in "Q3", with: @otfcompletion.q3
    fill_in "Q4", with: @otfcompletion.q4
    fill_in "Q5", with: @otfcompletion.q5
    fill_in "Q6", with: @otfcompletion.q6
    fill_in "Signature", with: @otfcompletion.signature
    fill_in "Sitename1", with: @otfcompletion.siteName1
    fill_in "Sitename2", with: @otfcompletion.siteName2
    fill_in "Sitename3", with: @otfcompletion.siteName3
    fill_in "Specialist1", with: @otfcompletion.specialist1
    fill_in "Specialist2", with: @otfcompletion.specialist2
    fill_in "Specialist3", with: @otfcompletion.specialist3
    fill_in "Summary", with: @otfcompletion.summary
    fill_in "Totalsitetime1", with: @otfcompletion.totalSiteTime1
    fill_in "Totalsitetime2", with: @otfcompletion.totalSiteTime2
    fill_in "Totalsitetime3", with: @otfcompletion.totalSiteTime3
    fill_in "Vrc", with: @otfcompletion.vrc
    click_on "Create Otfcompletion"

    assert_text "Otfcompletion was successfully created"
    click_on "Back"
  end

  test "updating a Otfcompletion" do
    visit otfcompletions_url
    click_on "Edit", match: :first

    fill_in "Address1", with: @otfcompletion.Address1
    fill_in "Address2", with: @otfcompletion.Address2
    fill_in "Address3", with: @otfcompletion.Address3
    fill_in "Dates1", with: @otfcompletion.Dates1
    fill_in "Dates2", with: @otfcompletion.Dates2
    fill_in "Dates3", with: @otfcompletion.Dates3
    fill_in "Jobsperformed1", with: @otfcompletion.JobsPerformed1
    fill_in "Jobsperformed2", with: @otfcompletion.JobsPerformed2
    fill_in "Jobsperformed3", with: @otfcompletion.JobsPerformed3
    fill_in "Agency", with: @otfcompletion.agency
    fill_in "Authid", with: @otfcompletion.authid
    fill_in "Avehoursperday1", with: @otfcompletion.aveHoursPerDay1
    fill_in "Avehoursperday2", with: @otfcompletion.aveHoursPerDay2
    fill_in "Avehoursperday3", with: @otfcompletion.aveHoursPerDay3
    fill_in "Comments", with: @otfcompletion.comments
    fill_in "Formtype", with: @otfcompletion.formtype
    fill_in "Numberofdays1", with: @otfcompletion.numberOfDays1
    fill_in "Numberofdays2", with: @otfcompletion.numberOfDays2
    fill_in "Numberofdays3", with: @otfcompletion.numberOfDays3
    fill_in "Q1", with: @otfcompletion.q1
    fill_in "Q2", with: @otfcompletion.q2
    fill_in "Q3", with: @otfcompletion.q3
    fill_in "Q4", with: @otfcompletion.q4
    fill_in "Q5", with: @otfcompletion.q5
    fill_in "Q6", with: @otfcompletion.q6
    fill_in "Signature", with: @otfcompletion.signature
    fill_in "Sitename1", with: @otfcompletion.siteName1
    fill_in "Sitename2", with: @otfcompletion.siteName2
    fill_in "Sitename3", with: @otfcompletion.siteName3
    fill_in "Specialist1", with: @otfcompletion.specialist1
    fill_in "Specialist2", with: @otfcompletion.specialist2
    fill_in "Specialist3", with: @otfcompletion.specialist3
    fill_in "Summary", with: @otfcompletion.summary
    fill_in "Totalsitetime1", with: @otfcompletion.totalSiteTime1
    fill_in "Totalsitetime2", with: @otfcompletion.totalSiteTime2
    fill_in "Totalsitetime3", with: @otfcompletion.totalSiteTime3
    fill_in "Vrc", with: @otfcompletion.vrc
    click_on "Update Otfcompletion"

    assert_text "Otfcompletion was successfully updated"
    click_on "Back"
  end

  test "destroying a Otfcompletion" do
    visit otfcompletions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Otfcompletion was successfully destroyed"
  end
end
