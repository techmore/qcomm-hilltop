require "application_system_test_case"

class OtfsTest < ApplicationSystemTestCase
  setup do
    @otf = otfs(:one)
  end

  test "visiting the index" do
    visit otfs_url
    assert_selector "h1", text: "Otfs"
  end

  test "creating a Otf" do
    visit otfs_url
    click_on "New Otf"

    fill_in "Authid", with: @otf.authid
    fill_in "Date", with: @otf.date
    fill_in "Emp1", with: @otf.emp1
    fill_in "Emp10", with: @otf.emp10
    fill_in "Emp10comment", with: @otf.emp10comment
    fill_in "Emp11", with: @otf.emp11
    fill_in "Emp11comment", with: @otf.emp11comment
    fill_in "Emp12", with: @otf.emp12
    fill_in "Emp12comment", with: @otf.emp12comment
    fill_in "Emp13", with: @otf.emp13
    fill_in "Emp13comment", with: @otf.emp13comment
    fill_in "Emp14", with: @otf.emp14
    fill_in "Emp14comment", with: @otf.emp14comment
    fill_in "Emp15", with: @otf.emp15
    fill_in "Emp15comment", with: @otf.emp15comment
    fill_in "Emp16", with: @otf.emp16
    fill_in "Emp16comment", with: @otf.emp16comment
    fill_in "Emp17", with: @otf.emp17
    fill_in "Emp17comment", with: @otf.emp17comment
    fill_in "Emp18", with: @otf.emp18
    fill_in "Emp18comment", with: @otf.emp18comment
    fill_in "Emp19", with: @otf.emp19
    fill_in "Emp19comment", with: @otf.emp19comment
    fill_in "Emp1comment", with: @otf.emp1comment
    fill_in "Emp2", with: @otf.emp2
    fill_in "Emp20", with: @otf.emp20
    fill_in "Emp20comment", with: @otf.emp20comment
    fill_in "Emp21", with: @otf.emp21
    fill_in "Emp21comment", with: @otf.emp21comment
    fill_in "Emp22", with: @otf.emp22
    fill_in "Emp22comment", with: @otf.emp22comment
    fill_in "Emp23", with: @otf.emp23
    fill_in "Emp23comment", with: @otf.emp23comment
    fill_in "Emp2comment", with: @otf.emp2comment
    fill_in "Emp3", with: @otf.emp3
    fill_in "Emp3comment", with: @otf.emp3comment
    fill_in "Emp4", with: @otf.emp4
    fill_in "Emp4comment", with: @otf.emp4comment
    fill_in "Emp5", with: @otf.emp5
    fill_in "Emp5comment", with: @otf.emp5comment
    fill_in "Emp6", with: @otf.emp6
    fill_in "Emp6comment", with: @otf.emp6comment
    fill_in "Emp7", with: @otf.emp7
    fill_in "Emp7comment", with: @otf.emp7comment
    fill_in "Emp8", with: @otf.emp8
    fill_in "Emp8comment", with: @otf.emp8comment
    fill_in "Emp9", with: @otf.emp9
    fill_in "Emp9comment", with: @otf.emp9comment
    fill_in "Employee", with: @otf.employee
    fill_in "Employer", with: @otf.employer
    fill_in "Endtime", with: @otf.endTime
    fill_in "Env1", with: @otf.env1
    fill_in "Env1comment", with: @otf.env1comment
    fill_in "Env2", with: @otf.env2
    fill_in "Env2comment", with: @otf.env2comment
    fill_in "Env3", with: @otf.env3
    fill_in "Env3comment", with: @otf.env3comment
    fill_in "Env4", with: @otf.env4
    fill_in "Env4comment", with: @otf.env4comment
    fill_in "Env5", with: @otf.env5
    fill_in "Env5comment", with: @otf.env5comment
    fill_in "Phy1", with: @otf.phy1
    fill_in "Phy10", with: @otf.phy10
    fill_in "Phy10comment", with: @otf.phy10comment
    fill_in "Phy1comment", with: @otf.phy1comment
    fill_in "Phy2", with: @otf.phy2
    fill_in "Phy2comment", with: @otf.phy2comment
    fill_in "Phy3", with: @otf.phy3
    fill_in "Phy3comment", with: @otf.phy3comment
    fill_in "Phy4", with: @otf.phy4
    fill_in "Phy4comment", with: @otf.phy4comment
    fill_in "Phy5", with: @otf.phy5
    fill_in "Phy5comment", with: @otf.phy5comment
    fill_in "Phy6", with: @otf.phy6
    fill_in "Phy6comment", with: @otf.phy6comment
    fill_in "Phy7", with: @otf.phy7
    fill_in "Phy7comment", with: @otf.phy7comment
    fill_in "Phy8", with: @otf.phy8
    fill_in "Phy8comment", with: @otf.phy8comment
    fill_in "Phy9", with: @otf.phy9
    fill_in "Phy9comment", with: @otf.phy9comment
    fill_in "Starttime", with: @otf.startTime
    click_on "Create Otf"

    assert_text "Otf was successfully created"
    click_on "Back"
  end

  test "updating a Otf" do
    visit otfs_url
    click_on "Edit", match: :first

    fill_in "Authid", with: @otf.authid
    fill_in "Date", with: @otf.date
    fill_in "Emp1", with: @otf.emp1
    fill_in "Emp10", with: @otf.emp10
    fill_in "Emp10comment", with: @otf.emp10comment
    fill_in "Emp11", with: @otf.emp11
    fill_in "Emp11comment", with: @otf.emp11comment
    fill_in "Emp12", with: @otf.emp12
    fill_in "Emp12comment", with: @otf.emp12comment
    fill_in "Emp13", with: @otf.emp13
    fill_in "Emp13comment", with: @otf.emp13comment
    fill_in "Emp14", with: @otf.emp14
    fill_in "Emp14comment", with: @otf.emp14comment
    fill_in "Emp15", with: @otf.emp15
    fill_in "Emp15comment", with: @otf.emp15comment
    fill_in "Emp16", with: @otf.emp16
    fill_in "Emp16comment", with: @otf.emp16comment
    fill_in "Emp17", with: @otf.emp17
    fill_in "Emp17comment", with: @otf.emp17comment
    fill_in "Emp18", with: @otf.emp18
    fill_in "Emp18comment", with: @otf.emp18comment
    fill_in "Emp19", with: @otf.emp19
    fill_in "Emp19comment", with: @otf.emp19comment
    fill_in "Emp1comment", with: @otf.emp1comment
    fill_in "Emp2", with: @otf.emp2
    fill_in "Emp20", with: @otf.emp20
    fill_in "Emp20comment", with: @otf.emp20comment
    fill_in "Emp21", with: @otf.emp21
    fill_in "Emp21comment", with: @otf.emp21comment
    fill_in "Emp22", with: @otf.emp22
    fill_in "Emp22comment", with: @otf.emp22comment
    fill_in "Emp23", with: @otf.emp23
    fill_in "Emp23comment", with: @otf.emp23comment
    fill_in "Emp2comment", with: @otf.emp2comment
    fill_in "Emp3", with: @otf.emp3
    fill_in "Emp3comment", with: @otf.emp3comment
    fill_in "Emp4", with: @otf.emp4
    fill_in "Emp4comment", with: @otf.emp4comment
    fill_in "Emp5", with: @otf.emp5
    fill_in "Emp5comment", with: @otf.emp5comment
    fill_in "Emp6", with: @otf.emp6
    fill_in "Emp6comment", with: @otf.emp6comment
    fill_in "Emp7", with: @otf.emp7
    fill_in "Emp7comment", with: @otf.emp7comment
    fill_in "Emp8", with: @otf.emp8
    fill_in "Emp8comment", with: @otf.emp8comment
    fill_in "Emp9", with: @otf.emp9
    fill_in "Emp9comment", with: @otf.emp9comment
    fill_in "Employee", with: @otf.employee
    fill_in "Employer", with: @otf.employer
    fill_in "Endtime", with: @otf.endTime
    fill_in "Env1", with: @otf.env1
    fill_in "Env1comment", with: @otf.env1comment
    fill_in "Env2", with: @otf.env2
    fill_in "Env2comment", with: @otf.env2comment
    fill_in "Env3", with: @otf.env3
    fill_in "Env3comment", with: @otf.env3comment
    fill_in "Env4", with: @otf.env4
    fill_in "Env4comment", with: @otf.env4comment
    fill_in "Env5", with: @otf.env5
    fill_in "Env5comment", with: @otf.env5comment
    fill_in "Phy1", with: @otf.phy1
    fill_in "Phy10", with: @otf.phy10
    fill_in "Phy10comment", with: @otf.phy10comment
    fill_in "Phy1comment", with: @otf.phy1comment
    fill_in "Phy2", with: @otf.phy2
    fill_in "Phy2comment", with: @otf.phy2comment
    fill_in "Phy3", with: @otf.phy3
    fill_in "Phy3comment", with: @otf.phy3comment
    fill_in "Phy4", with: @otf.phy4
    fill_in "Phy4comment", with: @otf.phy4comment
    fill_in "Phy5", with: @otf.phy5
    fill_in "Phy5comment", with: @otf.phy5comment
    fill_in "Phy6", with: @otf.phy6
    fill_in "Phy6comment", with: @otf.phy6comment
    fill_in "Phy7", with: @otf.phy7
    fill_in "Phy7comment", with: @otf.phy7comment
    fill_in "Phy8", with: @otf.phy8
    fill_in "Phy8comment", with: @otf.phy8comment
    fill_in "Phy9", with: @otf.phy9
    fill_in "Phy9comment", with: @otf.phy9comment
    fill_in "Starttime", with: @otf.startTime
    click_on "Update Otf"

    assert_text "Otf was successfully updated"
    click_on "Back"
  end

  test "destroying a Otf" do
    visit otfs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Otf was successfully destroyed"
  end
end
