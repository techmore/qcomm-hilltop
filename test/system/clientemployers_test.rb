require "application_system_test_case"

class ClientemployersTest < ApplicationSystemTestCase
  setup do
    @clientemployer = clientemployers(:one)
  end

  test "visiting the index" do
    visit clientemployers_url
    assert_selector "h1", text: "Clientemployers"
  end

  test "creating a Clientemployer" do
    visit clientemployers_url
    click_on "New Clientemployer"

    fill_in "Datemedicalstart", with: @clientemployer.DateMedicalStart
    fill_in "Employersnotes", with: @clientemployer.EmployersNotes
    fill_in "Getmedical", with: @clientemployer.GetMedical
    fill_in "Jobtitle", with: @clientemployer.JobTitle
    fill_in "Medicaltype", with: @clientemployer.MedicalType
    fill_in "Occupationcode", with: @clientemployer.OccupationCode
    fill_in "Semodel", with: @clientemployer.SEModel
    fill_in "Supervisorname", with: @clientemployer.SupervisorName
    fill_in "Supervisorphone", with: @clientemployer.SupervisorPhone
    fill_in "Terminationreason", with: @clientemployer.TerminationReason
    fill_in "Terminationtype", with: @clientemployer.TerminationType
    fill_in "Typeofjob", with: @clientemployer.TypeofJob
    fill_in "Wage", with: @clientemployer.Wage
    fill_in "Wageunit", with: @clientemployer.WageUnit
    fill_in "Workschedule", with: @clientemployer.WorkSchedule
    fill_in "Worktransportation", with: @clientemployer.WorkTransportation
    fill_in "Clientemployerid", with: @clientemployer.clientEmployerID
    fill_in "Clientid", with: @clientemployer.clientID
    fill_in "Enddate", with: @clientemployer.endDate
    fill_in "Hourspermonth", with: @clientemployer.hoursPerMonth
    fill_in "Startdate", with: @clientemployer.startDate
    click_on "Create Clientemployer"

    assert_text "Clientemployer was successfully created"
    click_on "Back"
  end

  test "updating a Clientemployer" do
    visit clientemployers_url
    click_on "Edit", match: :first

    fill_in "Datemedicalstart", with: @clientemployer.DateMedicalStart
    fill_in "Employersnotes", with: @clientemployer.EmployersNotes
    fill_in "Getmedical", with: @clientemployer.GetMedical
    fill_in "Jobtitle", with: @clientemployer.JobTitle
    fill_in "Medicaltype", with: @clientemployer.MedicalType
    fill_in "Occupationcode", with: @clientemployer.OccupationCode
    fill_in "Semodel", with: @clientemployer.SEModel
    fill_in "Supervisorname", with: @clientemployer.SupervisorName
    fill_in "Supervisorphone", with: @clientemployer.SupervisorPhone
    fill_in "Terminationreason", with: @clientemployer.TerminationReason
    fill_in "Terminationtype", with: @clientemployer.TerminationType
    fill_in "Typeofjob", with: @clientemployer.TypeofJob
    fill_in "Wage", with: @clientemployer.Wage
    fill_in "Wageunit", with: @clientemployer.WageUnit
    fill_in "Workschedule", with: @clientemployer.WorkSchedule
    fill_in "Worktransportation", with: @clientemployer.WorkTransportation
    fill_in "Clientemployerid", with: @clientemployer.clientEmployerID
    fill_in "Clientid", with: @clientemployer.clientID
    fill_in "Enddate", with: @clientemployer.endDate
    fill_in "Hourspermonth", with: @clientemployer.hoursPerMonth
    fill_in "Startdate", with: @clientemployer.startDate
    click_on "Update Clientemployer"

    assert_text "Clientemployer was successfully updated"
    click_on "Back"
  end

  test "destroying a Clientemployer" do
    visit clientemployers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Clientemployer was successfully destroyed"
  end
end
