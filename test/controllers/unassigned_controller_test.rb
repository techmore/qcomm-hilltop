require 'test_helper'

class UnassignedControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get unassigned_index_url
    assert_response :success
  end

end
