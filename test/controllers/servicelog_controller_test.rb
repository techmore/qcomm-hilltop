require 'test_helper'

class ServicelogControllerTest < ActionDispatch::IntegrationTest
  test "should get cbwe" do
    get servicelog_cbwe_url
    assert_response :success
  end

  test "should get twe" do
    get servicelog_twe_url
    assert_response :success
  end

  test "should get ee" do
    get servicelog_ee_url
    assert_response :success
  end

  test "should get ltfa" do
    get servicelog_ltfa_url
    assert_response :success
  end

  test "should get ijc" do
    get servicelog_ijc_url
    assert_response :success
  end

  test "should get pp" do
    get servicelog_pp_url
    assert_response :success
  end

  test "should get ddd" do
    get servicelog_ddd_url
    assert_response :success
  end

  test "should get dddpp" do
    get servicelog_dddpp_url
    assert_response :success
  end

  test "should get preets" do
    get servicelog_preets_url
    assert_response :success
  end

  test "should get schooleval" do
    get servicelog_schooleval_url
    assert_response :success
  end

end
