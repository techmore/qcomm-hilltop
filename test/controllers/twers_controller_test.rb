require 'test_helper'

class TwersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @twer = twers(:one)
  end

  test "should get index" do
    get twers_url
    assert_response :success
  end

  test "should get new" do
    get new_twer_url
    assert_response :success
  end

  test "should create twer" do
    assert_difference('Twer.count') do
      post twers_url, params: { twer: { c1: @twer.c1, c2: @twer.c2, c3: @twer.c3, c4: @twer.c4, client: @twer.client, coachsig: @twer.coachsig, days: @twer.days, duties: @twer.duties, employer: @twer.employer, final: @twer.final, hours: @twer.hours, q1: @twer.q1, q2: @twer.q2, q3: @twer.q3, q4: @twer.q4, sidgdate: @twer.sidgdate, ss1: @twer.ss1, ss2: @twer.ss2, ss3: @twer.ss3, ss4: @twer.ss4, ss5: @twer.ss5, ss6: @twer.ss6, ss7: @twer.ss7, ss8: @twer.ss8, week1: @twer.week1, wp1: @twer.wp1, wp2: @twer.wp2, wp3: @twer.wp3, wp4: @twer.wp4, wp5: @twer.wp5, wp6: @twer.wp6, wp7: @twer.wp7 } }
    end

    assert_redirected_to twer_url(Twer.last)
  end

  test "should show twer" do
    get twer_url(@twer)
    assert_response :success
  end

  test "should get edit" do
    get edit_twer_url(@twer)
    assert_response :success
  end

  test "should update twer" do
    patch twer_url(@twer), params: { twer: { c1: @twer.c1, c2: @twer.c2, c3: @twer.c3, c4: @twer.c4, client: @twer.client, coachsig: @twer.coachsig, days: @twer.days, duties: @twer.duties, employer: @twer.employer, final: @twer.final, hours: @twer.hours, q1: @twer.q1, q2: @twer.q2, q3: @twer.q3, q4: @twer.q4, sidgdate: @twer.sidgdate, ss1: @twer.ss1, ss2: @twer.ss2, ss3: @twer.ss3, ss4: @twer.ss4, ss5: @twer.ss5, ss6: @twer.ss6, ss7: @twer.ss7, ss8: @twer.ss8, week1: @twer.week1, wp1: @twer.wp1, wp2: @twer.wp2, wp3: @twer.wp3, wp4: @twer.wp4, wp5: @twer.wp5, wp6: @twer.wp6, wp7: @twer.wp7 } }
    assert_redirected_to twer_url(@twer)
  end

  test "should destroy twer" do
    assert_difference('Twer.count', -1) do
      delete twer_url(@twer)
    end

    assert_redirected_to twers_url
  end
end
