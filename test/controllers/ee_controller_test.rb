require 'test_helper'

class EeControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get ee_index_url
    assert_response :success
  end

  test "should get report" do
    get ee_report_url
    assert_response :success
  end

end
