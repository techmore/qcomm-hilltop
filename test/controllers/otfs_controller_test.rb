require 'test_helper'

class OtfsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @otf = otfs(:one)
  end

  test "should get index" do
    get otfs_url
    assert_response :success
  end

  test "should get new" do
    get new_otf_url
    assert_response :success
  end

  test "should create otf" do
    assert_difference('Otf.count') do
      post otfs_url, params: { otf: { authid: @otf.authid, date: @otf.date, emp1: @otf.emp1, emp10: @otf.emp10, emp10comment: @otf.emp10comment, emp11: @otf.emp11, emp11comment: @otf.emp11comment, emp12: @otf.emp12, emp12comment: @otf.emp12comment, emp13: @otf.emp13, emp13comment: @otf.emp13comment, emp14: @otf.emp14, emp14comment: @otf.emp14comment, emp15: @otf.emp15, emp15comment: @otf.emp15comment, emp16: @otf.emp16, emp16comment: @otf.emp16comment, emp17: @otf.emp17, emp17comment: @otf.emp17comment, emp18: @otf.emp18, emp18comment: @otf.emp18comment, emp19: @otf.emp19, emp19comment: @otf.emp19comment, emp1comment: @otf.emp1comment, emp2: @otf.emp2, emp20: @otf.emp20, emp20comment: @otf.emp20comment, emp21: @otf.emp21, emp21comment: @otf.emp21comment, emp22: @otf.emp22, emp22comment: @otf.emp22comment, emp23: @otf.emp23, emp23comment: @otf.emp23comment, emp2comment: @otf.emp2comment, emp3: @otf.emp3, emp3comment: @otf.emp3comment, emp4: @otf.emp4, emp4comment: @otf.emp4comment, emp5: @otf.emp5, emp5comment: @otf.emp5comment, emp6: @otf.emp6, emp6comment: @otf.emp6comment, emp7: @otf.emp7, emp7comment: @otf.emp7comment, emp8: @otf.emp8, emp8comment: @otf.emp8comment, emp9: @otf.emp9, emp9comment: @otf.emp9comment, employee: @otf.employee, employer: @otf.employer, endTime: @otf.endTime, env1: @otf.env1, env1comment: @otf.env1comment, env2: @otf.env2, env2comment: @otf.env2comment, env3: @otf.env3, env3comment: @otf.env3comment, env4: @otf.env4, env4comment: @otf.env4comment, env5: @otf.env5, env5comment: @otf.env5comment, phy1: @otf.phy1, phy10: @otf.phy10, phy10comment: @otf.phy10comment, phy1comment: @otf.phy1comment, phy2: @otf.phy2, phy2comment: @otf.phy2comment, phy3: @otf.phy3, phy3comment: @otf.phy3comment, phy4: @otf.phy4, phy4comment: @otf.phy4comment, phy5: @otf.phy5, phy5comment: @otf.phy5comment, phy6: @otf.phy6, phy6comment: @otf.phy6comment, phy7: @otf.phy7, phy7comment: @otf.phy7comment, phy8: @otf.phy8, phy8comment: @otf.phy8comment, phy9: @otf.phy9, phy9comment: @otf.phy9comment, startTime: @otf.startTime } }
    end

    assert_redirected_to otf_url(Otf.last)
  end

  test "should show otf" do
    get otf_url(@otf)
    assert_response :success
  end

  test "should get edit" do
    get edit_otf_url(@otf)
    assert_response :success
  end

  test "should update otf" do
    patch otf_url(@otf), params: { otf: { authid: @otf.authid, date: @otf.date, emp1: @otf.emp1, emp10: @otf.emp10, emp10comment: @otf.emp10comment, emp11: @otf.emp11, emp11comment: @otf.emp11comment, emp12: @otf.emp12, emp12comment: @otf.emp12comment, emp13: @otf.emp13, emp13comment: @otf.emp13comment, emp14: @otf.emp14, emp14comment: @otf.emp14comment, emp15: @otf.emp15, emp15comment: @otf.emp15comment, emp16: @otf.emp16, emp16comment: @otf.emp16comment, emp17: @otf.emp17, emp17comment: @otf.emp17comment, emp18: @otf.emp18, emp18comment: @otf.emp18comment, emp19: @otf.emp19, emp19comment: @otf.emp19comment, emp1comment: @otf.emp1comment, emp2: @otf.emp2, emp20: @otf.emp20, emp20comment: @otf.emp20comment, emp21: @otf.emp21, emp21comment: @otf.emp21comment, emp22: @otf.emp22, emp22comment: @otf.emp22comment, emp23: @otf.emp23, emp23comment: @otf.emp23comment, emp2comment: @otf.emp2comment, emp3: @otf.emp3, emp3comment: @otf.emp3comment, emp4: @otf.emp4, emp4comment: @otf.emp4comment, emp5: @otf.emp5, emp5comment: @otf.emp5comment, emp6: @otf.emp6, emp6comment: @otf.emp6comment, emp7: @otf.emp7, emp7comment: @otf.emp7comment, emp8: @otf.emp8, emp8comment: @otf.emp8comment, emp9: @otf.emp9, emp9comment: @otf.emp9comment, employee: @otf.employee, employer: @otf.employer, endTime: @otf.endTime, env1: @otf.env1, env1comment: @otf.env1comment, env2: @otf.env2, env2comment: @otf.env2comment, env3: @otf.env3, env3comment: @otf.env3comment, env4: @otf.env4, env4comment: @otf.env4comment, env5: @otf.env5, env5comment: @otf.env5comment, phy1: @otf.phy1, phy10: @otf.phy10, phy10comment: @otf.phy10comment, phy1comment: @otf.phy1comment, phy2: @otf.phy2, phy2comment: @otf.phy2comment, phy3: @otf.phy3, phy3comment: @otf.phy3comment, phy4: @otf.phy4, phy4comment: @otf.phy4comment, phy5: @otf.phy5, phy5comment: @otf.phy5comment, phy6: @otf.phy6, phy6comment: @otf.phy6comment, phy7: @otf.phy7, phy7comment: @otf.phy7comment, phy8: @otf.phy8, phy8comment: @otf.phy8comment, phy9: @otf.phy9, phy9comment: @otf.phy9comment, startTime: @otf.startTime } }
    assert_redirected_to otf_url(@otf)
  end

  test "should destroy otf" do
    assert_difference('Otf.count', -1) do
      delete otf_url(@otf)
    end

    assert_redirected_to otfs_url
  end
end
