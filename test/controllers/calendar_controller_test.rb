require 'test_helper'

class CalendarControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get calendar_index_url
    assert_response :success
  end

  test "should get jan" do
    get calendar_jan_url
    assert_response :success
  end

  test "should get feb" do
    get calendar_feb_url
    assert_response :success
  end

  test "should get mar" do
    get calendar_mar_url
    assert_response :success
  end

  test "should get apr" do
    get calendar_apr_url
    assert_response :success
  end

  test "should get may" do
    get calendar_may_url
    assert_response :success
  end

  test "should get jun" do
    get calendar_jun_url
    assert_response :success
  end

  test "should get jul" do
    get calendar_jul_url
    assert_response :success
  end

  test "should get aug" do
    get calendar_aug_url
    assert_response :success
  end

  test "should get sep" do
    get calendar_sep_url
    assert_response :success
  end

  test "should get oct" do
    get calendar_oct_url
    assert_response :success
  end

  test "should get nov" do
    get calendar_nov_url
    assert_response :success
  end

  test "should get dec" do
    get calendar_dec_url
    assert_response :success
  end

end
