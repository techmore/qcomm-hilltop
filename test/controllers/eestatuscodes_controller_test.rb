require 'test_helper'

class EestatuscodesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @eestatuscode = eestatuscodes(:one)
  end

  test "should get index" do
    get eestatuscodes_url
    assert_response :success
  end

  test "should get new" do
    get new_eestatuscode_url
    assert_response :success
  end

  test "should create eestatuscode" do
    assert_difference('Eestatuscode.count') do
      post eestatuscodes_url, params: { eestatuscode: { client: @eestatuscode.client, enddate: @eestatuscode.enddate, startdate: @eestatuscode.startdate, statuscode: @eestatuscode.statuscode } }
    end

    assert_redirected_to eestatuscode_url(Eestatuscode.last)
  end

  test "should show eestatuscode" do
    get eestatuscode_url(@eestatuscode)
    assert_response :success
  end

  test "should get edit" do
    get edit_eestatuscode_url(@eestatuscode)
    assert_response :success
  end

  test "should update eestatuscode" do
    patch eestatuscode_url(@eestatuscode), params: { eestatuscode: { client: @eestatuscode.client, enddate: @eestatuscode.enddate, startdate: @eestatuscode.startdate, statuscode: @eestatuscode.statuscode } }
    assert_redirected_to eestatuscode_url(@eestatuscode)
  end

  test "should destroy eestatuscode" do
    assert_difference('Eestatuscode.count', -1) do
      delete eestatuscode_url(@eestatuscode)
    end

    assert_redirected_to eestatuscodes_url
  end
end
