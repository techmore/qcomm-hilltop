require 'test_helper'

class ClientemployersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @clientemployer = clientemployers(:one)
  end

  test "should get index" do
    get clientemployers_url
    assert_response :success
  end

  test "should get new" do
    get new_clientemployer_url
    assert_response :success
  end

  test "should create clientemployer" do
    assert_difference('Clientemployer.count') do
      post clientemployers_url, params: { clientemployer: { DateMedicalStart: @clientemployer.DateMedicalStart, EmployersNotes: @clientemployer.EmployersNotes, GetMedical: @clientemployer.GetMedical, JobTitle: @clientemployer.JobTitle, MedicalType: @clientemployer.MedicalType, OccupationCode: @clientemployer.OccupationCode, SEModel: @clientemployer.SEModel, SupervisorName: @clientemployer.SupervisorName, SupervisorPhone: @clientemployer.SupervisorPhone, TerminationReason: @clientemployer.TerminationReason, TerminationType: @clientemployer.TerminationType, TypeofJob: @clientemployer.TypeofJob, Wage: @clientemployer.Wage, WageUnit: @clientemployer.WageUnit, WorkSchedule: @clientemployer.WorkSchedule, WorkTransportation: @clientemployer.WorkTransportation, clientEmployerID: @clientemployer.clientEmployerID, clientID: @clientemployer.clientID, endDate: @clientemployer.endDate, hoursPerMonth: @clientemployer.hoursPerMonth, startDate: @clientemployer.startDate } }
    end

    assert_redirected_to clientemployer_url(Clientemployer.last)
  end

  test "should show clientemployer" do
    get clientemployer_url(@clientemployer)
    assert_response :success
  end

  test "should get edit" do
    get edit_clientemployer_url(@clientemployer)
    assert_response :success
  end

  test "should update clientemployer" do
    patch clientemployer_url(@clientemployer), params: { clientemployer: { DateMedicalStart: @clientemployer.DateMedicalStart, EmployersNotes: @clientemployer.EmployersNotes, GetMedical: @clientemployer.GetMedical, JobTitle: @clientemployer.JobTitle, MedicalType: @clientemployer.MedicalType, OccupationCode: @clientemployer.OccupationCode, SEModel: @clientemployer.SEModel, SupervisorName: @clientemployer.SupervisorName, SupervisorPhone: @clientemployer.SupervisorPhone, TerminationReason: @clientemployer.TerminationReason, TerminationType: @clientemployer.TerminationType, TypeofJob: @clientemployer.TypeofJob, Wage: @clientemployer.Wage, WageUnit: @clientemployer.WageUnit, WorkSchedule: @clientemployer.WorkSchedule, WorkTransportation: @clientemployer.WorkTransportation, clientEmployerID: @clientemployer.clientEmployerID, clientID: @clientemployer.clientID, endDate: @clientemployer.endDate, hoursPerMonth: @clientemployer.hoursPerMonth, startDate: @clientemployer.startDate } }
    assert_redirected_to clientemployer_url(@clientemployer)
  end

  test "should destroy clientemployer" do
    assert_difference('Clientemployer.count', -1) do
      delete clientemployer_url(@clientemployer)
    end

    assert_redirected_to clientemployers_url
  end
end
