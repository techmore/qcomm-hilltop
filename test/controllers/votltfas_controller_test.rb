require 'test_helper'

class VotltfasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @votltfa = votltfas(:one)
  end

  test "should get index" do
    get votltfas_url
    assert_response :success
  end

  test "should get new" do
    get new_votltfa_url
    assert_response :success
  end

  test "should create votltfa" do
    assert_difference('Votltfa.count') do
      post votltfas_url, params: { votltfa: { client: @votltfa.client, date: @votltfa.date, empname: @votltfa.empname, empsig: @votltfa.empsig, provider: @votltfa.provider, providerstaff: @votltfa.providerstaff, rate: @votltfa.rate, reason: @votltfa.reason } }
    end

    assert_redirected_to votltfa_url(Votltfa.last)
  end

  test "should show votltfa" do
    get votltfa_url(@votltfa)
    assert_response :success
  end

  test "should get edit" do
    get edit_votltfa_url(@votltfa)
    assert_response :success
  end

  test "should update votltfa" do
    patch votltfa_url(@votltfa), params: { votltfa: { client: @votltfa.client, date: @votltfa.date, empname: @votltfa.empname, empsig: @votltfa.empsig, provider: @votltfa.provider, providerstaff: @votltfa.providerstaff, rate: @votltfa.rate, reason: @votltfa.reason } }
    assert_redirected_to votltfa_url(@votltfa)
  end

  test "should destroy votltfa" do
    assert_difference('Votltfa.count', -1) do
      delete votltfa_url(@votltfa)
    end

    assert_redirected_to votltfas_url
  end
end
