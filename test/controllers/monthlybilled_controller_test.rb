require 'test_helper'

class MonthlybilledControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get monthlybilled_index_url
    assert_response :success
  end

  test "should get jan" do
    get monthlybilled_jan_url
    assert_response :success
  end

  test "should get feb" do
    get monthlybilled_feb_url
    assert_response :success
  end

  test "should get mar" do
    get monthlybilled_mar_url
    assert_response :success
  end

  test "should get apr" do
    get monthlybilled_apr_url
    assert_response :success
  end

  test "should get may" do
    get monthlybilled_may_url
    assert_response :success
  end

  test "should get jun" do
    get monthlybilled_jun_url
    assert_response :success
  end

  test "should get jul" do
    get monthlybilled_jul_url
    assert_response :success
  end

  test "should get aug" do
    get monthlybilled_aug_url
    assert_response :success
  end

  test "should get sep" do
    get monthlybilled_sep_url
    assert_response :success
  end

  test "should get oct" do
    get monthlybilled_oct_url
    assert_response :success
  end

  test "should get nov" do
    get monthlybilled_nov_url
    assert_response :success
  end

  test "should get dec" do
    get monthlybilled_dec_url
    assert_response :success
  end

end
