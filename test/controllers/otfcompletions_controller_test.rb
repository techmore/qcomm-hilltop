require 'test_helper'

class OtfcompletionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @otfcompletion = otfcompletions(:one)
  end

  test "should get index" do
    get otfcompletions_url
    assert_response :success
  end

  test "should get new" do
    get new_otfcompletion_url
    assert_response :success
  end

  test "should create otfcompletion" do
    assert_difference('Otfcompletion.count') do
      post otfcompletions_url, params: { otfcompletion: { Address1: @otfcompletion.Address1, Address2: @otfcompletion.Address2, Address3: @otfcompletion.Address3, Dates1: @otfcompletion.Dates1, Dates2: @otfcompletion.Dates2, Dates3: @otfcompletion.Dates3, JobsPerformed1: @otfcompletion.JobsPerformed1, JobsPerformed2: @otfcompletion.JobsPerformed2, JobsPerformed3: @otfcompletion.JobsPerformed3, agency: @otfcompletion.agency, authid: @otfcompletion.authid, aveHoursPerDay1: @otfcompletion.aveHoursPerDay1, aveHoursPerDay2: @otfcompletion.aveHoursPerDay2, aveHoursPerDay3: @otfcompletion.aveHoursPerDay3, comments: @otfcompletion.comments, formtype: @otfcompletion.formtype, numberOfDays1: @otfcompletion.numberOfDays1, numberOfDays2: @otfcompletion.numberOfDays2, numberOfDays3: @otfcompletion.numberOfDays3, q1: @otfcompletion.q1, q2: @otfcompletion.q2, q3: @otfcompletion.q3, q4: @otfcompletion.q4, q5: @otfcompletion.q5, q6: @otfcompletion.q6, signature: @otfcompletion.signature, siteName1: @otfcompletion.siteName1, siteName2: @otfcompletion.siteName2, siteName3: @otfcompletion.siteName3, specialist1: @otfcompletion.specialist1, specialist2: @otfcompletion.specialist2, specialist3: @otfcompletion.specialist3, summary: @otfcompletion.summary, totalSiteTime1: @otfcompletion.totalSiteTime1, totalSiteTime2: @otfcompletion.totalSiteTime2, totalSiteTime3: @otfcompletion.totalSiteTime3, vrc: @otfcompletion.vrc } }
    end

    assert_redirected_to otfcompletion_url(Otfcompletion.last)
  end

  test "should show otfcompletion" do
    get otfcompletion_url(@otfcompletion)
    assert_response :success
  end

  test "should get edit" do
    get edit_otfcompletion_url(@otfcompletion)
    assert_response :success
  end

  test "should update otfcompletion" do
    patch otfcompletion_url(@otfcompletion), params: { otfcompletion: { Address1: @otfcompletion.Address1, Address2: @otfcompletion.Address2, Address3: @otfcompletion.Address3, Dates1: @otfcompletion.Dates1, Dates2: @otfcompletion.Dates2, Dates3: @otfcompletion.Dates3, JobsPerformed1: @otfcompletion.JobsPerformed1, JobsPerformed2: @otfcompletion.JobsPerformed2, JobsPerformed3: @otfcompletion.JobsPerformed3, agency: @otfcompletion.agency, authid: @otfcompletion.authid, aveHoursPerDay1: @otfcompletion.aveHoursPerDay1, aveHoursPerDay2: @otfcompletion.aveHoursPerDay2, aveHoursPerDay3: @otfcompletion.aveHoursPerDay3, comments: @otfcompletion.comments, formtype: @otfcompletion.formtype, numberOfDays1: @otfcompletion.numberOfDays1, numberOfDays2: @otfcompletion.numberOfDays2, numberOfDays3: @otfcompletion.numberOfDays3, q1: @otfcompletion.q1, q2: @otfcompletion.q2, q3: @otfcompletion.q3, q4: @otfcompletion.q4, q5: @otfcompletion.q5, q6: @otfcompletion.q6, signature: @otfcompletion.signature, siteName1: @otfcompletion.siteName1, siteName2: @otfcompletion.siteName2, siteName3: @otfcompletion.siteName3, specialist1: @otfcompletion.specialist1, specialist2: @otfcompletion.specialist2, specialist3: @otfcompletion.specialist3, summary: @otfcompletion.summary, totalSiteTime1: @otfcompletion.totalSiteTime1, totalSiteTime2: @otfcompletion.totalSiteTime2, totalSiteTime3: @otfcompletion.totalSiteTime3, vrc: @otfcompletion.vrc } }
    assert_redirected_to otfcompletion_url(@otfcompletion)
  end

  test "should destroy otfcompletion" do
    assert_difference('Otfcompletion.count', -1) do
      delete otfcompletion_url(@otfcompletion)
    end

    assert_redirected_to otfcompletions_url
  end
end
