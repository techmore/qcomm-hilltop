require 'test_helper'

class TimesheetsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get timesheets_index_url
    assert_response :success
  end

  test "should get jan" do
    get timesheets_jan_url
    assert_response :success
  end

  test "should get feb" do
    get timesheets_feb_url
    assert_response :success
  end

  test "should get mar" do
    get timesheets_mar_url
    assert_response :success
  end

  test "should get apr" do
    get timesheets_apr_url
    assert_response :success
  end

  test "should get may" do
    get timesheets_may_url
    assert_response :success
  end

  test "should get jun" do
    get timesheets_jun_url
    assert_response :success
  end

  test "should get jul" do
    get timesheets_jul_url
    assert_response :success
  end

  test "should get aug" do
    get timesheets_aug_url
    assert_response :success
  end

  test "should get sep" do
    get timesheets_sep_url
    assert_response :success
  end

  test "should get oct" do
    get timesheets_oct_url
    assert_response :success
  end

  test "should get nov" do
    get timesheets_nov_url
    assert_response :success
  end

  test "should get dec" do
    get timesheets_dec_url
    assert_response :success
  end

end
