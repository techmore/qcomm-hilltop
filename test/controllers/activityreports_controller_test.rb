require 'test_helper'

class ActivityreportsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @activityreport = activityreports(:one)
  end

  test "should get index" do
    get activityreports_url
    assert_response :success
  end

  test "should get new" do
    get new_activityreport_url
    assert_response :success
  end

  test "should create activityreport" do
    assert_difference('Activityreport.count') do
      post activityreports_url, params: { activityreport: { activities: @activityreport.activities, areas: @activityreport.areas, authid: @activityreport.authid, clientid: @activityreport.clientid, comments: @activityreport.comments, completed_by: @activityreport.completed_by, date: @activityreport.date, date_medical_start: @activityreport.date_medical_start, dvrs_office: @activityreport.dvrs_office, emp_address: @activityreport.emp_address, emp_name: @activityreport.emp_name, emp_start: @activityreport.emp_start, emp_sup: @activityreport.emp_sup, hours_provided: @activityreport.hours_provided, hours_wanted: @activityreport.hours_wanted, invoice: @activityreport.invoice, job_goal: @activityreport.job_goal, job_title: @activityreport.job_title, med_provided: @activityreport.med_provided, medical: @activityreport.medical, name: @activityreport.name, option1: @activityreport.option1, option2: @activityreport.option2, option3: @activityreport.option3, pp: @activityreport.pp, reporting_period: @activityreport.reporting_period, requested_hours: @activityreport.requested_hours, service: @activityreport.service, telephone: @activityreport.telephone, voucher_num: @activityreport.voucher_num, wages: @activityreport.wages, work_schedule: @activityreport.work_schedule } }
    end

    assert_redirected_to activityreport_url(Activityreport.last)
  end

  test "should show activityreport" do
    get activityreport_url(@activityreport)
    assert_response :success
  end

  test "should get edit" do
    get edit_activityreport_url(@activityreport)
    assert_response :success
  end

  test "should update activityreport" do
    patch activityreport_url(@activityreport), params: { activityreport: { activities: @activityreport.activities, areas: @activityreport.areas, authid: @activityreport.authid, clientid: @activityreport.clientid, comments: @activityreport.comments, completed_by: @activityreport.completed_by, date: @activityreport.date, date_medical_start: @activityreport.date_medical_start, dvrs_office: @activityreport.dvrs_office, emp_address: @activityreport.emp_address, emp_name: @activityreport.emp_name, emp_start: @activityreport.emp_start, emp_sup: @activityreport.emp_sup, hours_provided: @activityreport.hours_provided, hours_wanted: @activityreport.hours_wanted, invoice: @activityreport.invoice, job_goal: @activityreport.job_goal, job_title: @activityreport.job_title, med_provided: @activityreport.med_provided, medical: @activityreport.medical, name: @activityreport.name, option1: @activityreport.option1, option2: @activityreport.option2, option3: @activityreport.option3, pp: @activityreport.pp, reporting_period: @activityreport.reporting_period, requested_hours: @activityreport.requested_hours, service: @activityreport.service, telephone: @activityreport.telephone, voucher_num: @activityreport.voucher_num, wages: @activityreport.wages, work_schedule: @activityreport.work_schedule } }
    assert_redirected_to activityreport_url(@activityreport)
  end

  test "should destroy activityreport" do
    assert_difference('Activityreport.count', -1) do
      delete activityreport_url(@activityreport)
    end

    assert_redirected_to activityreports_url
  end
end
