require 'test_helper'

class ClientsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @client = clients(:one)
  end

  test "should get index" do
    get clients_url
    assert_response :success
  end

  test "should get new" do
    get new_client_url
    assert_response :success
  end

  test "should create client" do
    assert_difference('Client.count') do
      post clients_url, params: { client: { ClientASL: @client.ClientASL, ClientAllergies: @client.ClientAllergies, ClientCommMethod: @client.ClientCommMethod, ClientEducationLevel: @client.ClientEducationLevel, ClientFAHours: @client.ClientFAHours, ClientFAStartDate: @client.ClientFAStartDate, ClientGender: @client.ClientGender, ClientLivingArrangment: @client.ClientLivingArrangment, ClientMedicalInformation: @client.ClientMedicalInformation, ClientNotes: @client.ClientNotes, ClientPrimaryImpairment: @client.ClientPrimaryImpairment, ClientPrimaryMHDisability: @client.ClientPrimaryMHDisability, ClientPrimaryNonMHDisability: @client.ClientPrimaryNonMHDisability, ClientPrimarySource: @client.ClientPrimarySource, ClientProgram: @client.ClientProgram, ClientRace: @client.ClientRace, ClientReferralDate: @client.ClientReferralDate, ClientReferredBy: @client.ClientReferredBy, ClientResume: @client.ClientResume, ClientSecondaryImpairment: @client.ClientSecondaryImpairment, ClientSecondaryMHDisability: @client.ClientSecondaryMHDisability, ClientSecondaryNonMHDisability: @client.ClientSecondaryNonMHDisability, ClientSecondarySource: @client.ClientSecondarySource, ClientServiceStopDate: @client.ClientServiceStopDate, ClientStartDate: @client.ClientStartDate, ClientTypeOfTermination: @client.ClientTypeOfTermination, CriminalRecord: @client.CriminalRecord, CriminalRecordDetail: @client.CriminalRecordDetail, DDDSerialNbr: @client.DDDSerialNbr, DVRSCaseIDNbr: @client.DVRSCaseIDNbr, EmergencyContact1Cell: @client.EmergencyContact1Cell, EmergencyContact1Home: @client.EmergencyContact1Home, EmergencyContact1Name: @client.EmergencyContact1Name, EmergencyContact1Relationship: @client.EmergencyContact1Relationship, EmergencyContact1Work: @client.EmergencyContact1Work, EmergencyContact1WorkExt: @client.EmergencyContact1WorkExt, EmergencyContact2Cell: @client.EmergencyContact2Cell, EmergencyContact2Home: @client.EmergencyContact2Home, EmergencyContact2Name: @client.EmergencyContact2Name, EmergencyContact2Relationship: @client.EmergencyContact2Relationship, EmergencyContact2Work: @client.EmergencyContact2Work, EmergencyContact2WorkExt: @client.EmergencyContact2WorkExt, Googledrive: @client.Googledrive, GuardianAddress1: @client.GuardianAddress1, GuardianAddress2: @client.GuardianAddress2, GuardianCell: @client.GuardianCell, GuardianCity: @client.GuardianCity, GuardianHome: @client.GuardianHome, GuardianName: @client.GuardianName, GuardianState: @client.GuardianState, GuardianWork: @client.GuardianWork, GuardianWorkExt: @client.GuardianWorkExt, GuardianZip: @client.GuardianZip, GuardinaRelationship: @client.GuardinaRelationship, ReferralPhoneExt: @client.ReferralPhoneExt, ReferralSourceNumber: @client.ReferralSourceNumber, Resume: @client.Resume, clientAddress1: @client.clientAddress1, clientAddress2: @client.clientAddress2, clientCity: @client.clientCity, clientCounty: @client.clientCounty, clientDOB: @client.clientDOB, clientEmail: @client.clientEmail, clientFirstName: @client.clientFirstName, clientHomePhone: @client.clientHomePhone, clientID: @client.clientID, clientLastName: @client.clientLastName, clientMiddleName: @client.clientMiddleName, clientMobilePhone: @client.clientMobilePhone, clientSSN: @client.clientSSN, clientState: @client.clientState, clientWorkPhone: @client.clientWorkPhone, clientWorkPhoneExt: @client.clientWorkPhoneExt, clientZip: @client.clientZip } }
    end

    assert_redirected_to client_url(Client.last)
  end

  test "should show client" do
    get client_url(@client)
    assert_response :success
  end

  test "should get edit" do
    get edit_client_url(@client)
    assert_response :success
  end

  test "should update client" do
    patch client_url(@client), params: { client: { ClientASL: @client.ClientASL, ClientAllergies: @client.ClientAllergies, ClientCommMethod: @client.ClientCommMethod, ClientEducationLevel: @client.ClientEducationLevel, ClientFAHours: @client.ClientFAHours, ClientFAStartDate: @client.ClientFAStartDate, ClientGender: @client.ClientGender, ClientLivingArrangment: @client.ClientLivingArrangment, ClientMedicalInformation: @client.ClientMedicalInformation, ClientNotes: @client.ClientNotes, ClientPrimaryImpairment: @client.ClientPrimaryImpairment, ClientPrimaryMHDisability: @client.ClientPrimaryMHDisability, ClientPrimaryNonMHDisability: @client.ClientPrimaryNonMHDisability, ClientPrimarySource: @client.ClientPrimarySource, ClientProgram: @client.ClientProgram, ClientRace: @client.ClientRace, ClientReferralDate: @client.ClientReferralDate, ClientReferredBy: @client.ClientReferredBy, ClientResume: @client.ClientResume, ClientSecondaryImpairment: @client.ClientSecondaryImpairment, ClientSecondaryMHDisability: @client.ClientSecondaryMHDisability, ClientSecondaryNonMHDisability: @client.ClientSecondaryNonMHDisability, ClientSecondarySource: @client.ClientSecondarySource, ClientServiceStopDate: @client.ClientServiceStopDate, ClientStartDate: @client.ClientStartDate, ClientTypeOfTermination: @client.ClientTypeOfTermination, CriminalRecord: @client.CriminalRecord, CriminalRecordDetail: @client.CriminalRecordDetail, DDDSerialNbr: @client.DDDSerialNbr, DVRSCaseIDNbr: @client.DVRSCaseIDNbr, EmergencyContact1Cell: @client.EmergencyContact1Cell, EmergencyContact1Home: @client.EmergencyContact1Home, EmergencyContact1Name: @client.EmergencyContact1Name, EmergencyContact1Relationship: @client.EmergencyContact1Relationship, EmergencyContact1Work: @client.EmergencyContact1Work, EmergencyContact1WorkExt: @client.EmergencyContact1WorkExt, EmergencyContact2Cell: @client.EmergencyContact2Cell, EmergencyContact2Home: @client.EmergencyContact2Home, EmergencyContact2Name: @client.EmergencyContact2Name, EmergencyContact2Relationship: @client.EmergencyContact2Relationship, EmergencyContact2Work: @client.EmergencyContact2Work, EmergencyContact2WorkExt: @client.EmergencyContact2WorkExt, Googledrive: @client.Googledrive, GuardianAddress1: @client.GuardianAddress1, GuardianAddress2: @client.GuardianAddress2, GuardianCell: @client.GuardianCell, GuardianCity: @client.GuardianCity, GuardianHome: @client.GuardianHome, GuardianName: @client.GuardianName, GuardianState: @client.GuardianState, GuardianWork: @client.GuardianWork, GuardianWorkExt: @client.GuardianWorkExt, GuardianZip: @client.GuardianZip, GuardinaRelationship: @client.GuardinaRelationship, ReferralPhoneExt: @client.ReferralPhoneExt, ReferralSourceNumber: @client.ReferralSourceNumber, Resume: @client.Resume, clientAddress1: @client.clientAddress1, clientAddress2: @client.clientAddress2, clientCity: @client.clientCity, clientCounty: @client.clientCounty, clientDOB: @client.clientDOB, clientEmail: @client.clientEmail, clientFirstName: @client.clientFirstName, clientHomePhone: @client.clientHomePhone, clientID: @client.clientID, clientLastName: @client.clientLastName, clientMiddleName: @client.clientMiddleName, clientMobilePhone: @client.clientMobilePhone, clientSSN: @client.clientSSN, clientState: @client.clientState, clientWorkPhone: @client.clientWorkPhone, clientWorkPhoneExt: @client.clientWorkPhoneExt, clientZip: @client.clientZip } }
    assert_redirected_to client_url(@client)
  end

  test "should destroy client" do
    assert_difference('Client.count', -1) do
      delete client_url(@client)
    end

    assert_redirected_to clients_url
  end
end
