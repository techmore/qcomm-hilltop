require 'test_helper'

class HourlyratesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @hourlyrate = hourlyrates(:one)
  end

  test "should get index" do
    get hourlyrates_url
    assert_response :success
  end

  test "should get new" do
    get new_hourlyrate_url
    assert_response :success
  end

  test "should create hourlyrate" do
    assert_difference('Hourlyrate.count') do
      post hourlyrates_url, params: { hourlyrate: { client: @hourlyrate.client, enddate: @hourlyrate.enddate, hourlyrate: @hourlyrate.hourlyrate, startdate: @hourlyrate.startdate } }
    end

    assert_redirected_to hourlyrate_url(Hourlyrate.last)
  end

  test "should show hourlyrate" do
    get hourlyrate_url(@hourlyrate)
    assert_response :success
  end

  test "should get edit" do
    get edit_hourlyrate_url(@hourlyrate)
    assert_response :success
  end

  test "should update hourlyrate" do
    patch hourlyrate_url(@hourlyrate), params: { hourlyrate: { client: @hourlyrate.client, enddate: @hourlyrate.enddate, hourlyrate: @hourlyrate.hourlyrate, startdate: @hourlyrate.startdate } }
    assert_redirected_to hourlyrate_url(@hourlyrate)
  end

  test "should destroy hourlyrate" do
    assert_difference('Hourlyrate.count', -1) do
      delete hourlyrate_url(@hourlyrate)
    end

    assert_redirected_to hourlyrates_url
  end
end
