require 'test_helper'

class LtfaControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get ltfa_index_url
    assert_response :success
  end

  test "should get jan" do
    get ltfa_jan_url
    assert_response :success
  end

  test "should get feb" do
    get ltfa_feb_url
    assert_response :success
  end

  test "should get mar" do
    get ltfa_mar_url
    assert_response :success
  end

  test "should get apr" do
    get ltfa_apr_url
    assert_response :success
  end

  test "should get may" do
    get ltfa_may_url
    assert_response :success
  end

  test "should get jun" do
    get ltfa_jun_url
    assert_response :success
  end

  test "should get jul" do
    get ltfa_jul_url
    assert_response :success
  end

  test "should get aug" do
    get ltfa_aug_url
    assert_response :success
  end

  test "should get sep" do
    get ltfa_sep_url
    assert_response :success
  end

  test "should get oct" do
    get ltfa_oct_url
    assert_response :success
  end

  test "should get nov" do
    get ltfa_nov_url
    assert_response :success
  end

  test "should get dec" do
    get ltfa_dec_url
    assert_response :success
  end

end
