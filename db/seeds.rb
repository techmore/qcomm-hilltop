# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create( email: 'dbadmin@cea-nj.org', password: 'dbadmin')
Employee.create( employee: "DBadmin", userType: "DBadmin", email: "dbadmin@cea-nj.org")
User.create( email: 'manager@cea-nj.org', password: 'manager')
Employee.create( employee: "Manager", userType: "Manager", email: "manager@cea-nj.org")
User.create( email: 'finance@cea-nj.org', password: 'finance')
Employee.create( employee: "Finance", userType: "Finance", email: "finance@cea-nj.org")
User.create( email: 'dataentry@cea-nj.org', password: 'dataentry')
Employee.create( employee: "Dataentry", userType: "dataEntry", email: "dataentry@cea-nj.org" )



Employer.create( employerName: "Shoprite-Flemington", employerAddress1: "272 Highway 202/31 North", employerCity: "Flemington", employerState: "NJ", employerZip: "08822", employerWorkPhone: "(908) 782-2553"  )

emp_count = 5
client_count = 1
auth_count = 1
year = 2020

 User.create( email: 'jc@cea-nj.org', password: 'jobcoach')
  Employee.create( employee: "Job Coach", userType: "JobCoach", email: "jc@cea-nj.org")
    Assignment.create( ClientID: client_count, Employee: emp_count, DateAssigned: Date.today)
    Client.create( clientFirstName: Faker::Name.first_name, clientLastName: Faker::Name.last_name, clientAddress1: Faker::Address.street_address, clientCity: Faker::Address.city, clientCounty: "USA", clientState: "NJ", clientZip: Faker::Address.zip, clientMobilePhone: Faker::PhoneNumber.cell_phone, clientEmail: Faker::Internet.email, clientSSN: Faker::Number.number(digits: 9), clientDOB: Faker::Date.birthday(min_age: 18, max_age: 65), ClientStartDate: (Date.today - 365.days), ClientReferredBy: Faker::Name.unique.name, EmergencyContact1Name: Faker::Name.unique.name ,EmergencyContact1Cell: Faker::PhoneNumber.cell_phone, EmergencyContact2Name: Faker::Name.unique.name ,EmergencyContact2Cell: Faker::PhoneNumber.cell_phone, participantID: Faker::Number.number(digits: 6), ClientPrimaryImpairment: Faker::Number.between(from: 1, to: 19), ClientPrimarySource: Faker::Number.between(from: 1, to: 37) , GuardianName: Faker::Name.unique.name, GuardinaRelationship: "Parent", GuardianCell: Faker::PhoneNumber.cell_phone )
    Clientemployer.create(clientID: client_count, clientEmployerID: "1", startDate: (Date.today - 365.days), hoursPerMonth: "40", JobTitle: "Cashier", SupervisorName: Faker::Name.unique.name, SupervisorPhone: Faker::PhoneNumber.cell_phone, WorkSchedule: "M-F, 8-4", Wage: "15",  )
    Authorization.create(
      clientID: client_count, 
      voucherNumber: "LTFA-2020", 
      voucherOrAuthorizationDate: "07-01-2020", 
      serviceType: "3", 
      hoursAuthorized: "3", 
      VoucherRate: "53.00", 
      VoucherGoal: "Maintain Employment",
      #invoiceDateJan: DateTime.new(year,2,10,10,0,0),
      #invoiceAmountJan: "159.00",
      #invoiceNumberJan: Faker::Number.number(digits: 6),
      #invoiceDateFeb: DateTime.new(year,3,10,10,0,0),
      #invoiceAmountFeb: "159.00",
      #invoiceNumberFeb: Faker::Number.number(digits: 6),
      #invoiceDateMar: DateTime.new(year,4,10,10,0,0),
      #invoiceAmountMar: "159.00",
      #invoiceNumberMar: Faker::Number.number(digits: 6),
      #invoiceDateApr: DateTime.new(year,5,10,10,0,0),
      #invoiceAmountApr: "159.00",
      #invoiceNumberApr: Faker::Number.number(digits: 6),
      #invoiceDateMay: DateTime.new(year,6,10,10,0,0),
      #invoiceAmountMay: "159.00",
      #invoiceNumberMay: Faker::Number.number(digits: 6),
      invoiceDateJun: DateTime.new(year,7,10,10,0,0),
      invoiceAmountJun: "159.00",
      invoiceNumberJun: Faker::Number.number(digits: 6),
      invoiceDateJul: DateTime.new(year,8,10,10,0,0),
      invoiceAmountJul: "159.00",
      invoiceNumberJul: Faker::Number.number(digits: 6),
      invoiceDateAug: DateTime.new(year,9,10,10,0,0),
      invoiceAmountAug: "159.00",
      invoiceNumberAug: Faker::Number.number(digits: 6),
      invoiceDateSep: DateTime.new(year,10,10,10,0,0),
      invoiceAmountSep: "159.00",
      invoiceNumberSep: Faker::Number.number(digits: 6),
      invoiceDateOct: DateTime.new(year,11,10,10,0,0),
      invoiceAmountOct: "159.00",
      invoiceNumberOct: Faker::Number.number(digits: 6),
      invoiceDateNov: DateTime.new(year,12,10,10,0,0),
      invoiceAmountNov: "159.00",
      invoiceNumberNov: Faker::Number.number(digits: 6),
=begin
      invoiceDateNov: DateTime.new(year,12,10,10,0,0),
      invoiceAmountNov: "159.00",
      invoiceNumberNov: Faker::Number.number(digits: 6),
      invoiceDateDec: DateTime.new((year.to_i + 1),1,10,10,0,0),
      invoiceAmountDec: "159.00",
      invoiceNumberDec: Faker::Number.number(digits: 6),
=end
          #paymentReceivedDateJan: Faker::Date.between(from: DateTime.new(year,2,20,10,0,0), to: DateTime.new(year,2,28,10,0,0)),
          #paymentReceivedDateFeb: Faker::Date.between(from: DateTime.new(year,3,20,10,0,0), to: DateTime.new(year,3,30,10,0,0)),
          #paymentReceivedDateMar: Faker::Date.between(from: DateTime.new(year,4,20,10,0,0), to: DateTime.new(year,4,30,10,0,0)),
          #paymentReceivedDateApr: Faker::Date.between(from: DateTime.new(year,5,20,10,0,0), to: DateTime.new(year,5,30,10,0,0)),
          #paymentReceivedDateMay: Faker::Date.between(from: DateTime.new(year,6,20,10,0,0), to: DateTime.new(year,6,30,10,0,0)),
          paymentReceivedDateJun: Faker::Date.between(from: DateTime.new(year,7,20,10,0,0), to: DateTime.new(year,7,30,10,0,0)),
          paymentReceivedDateJul: Faker::Date.between(from: DateTime.new(year,8,20,10,0,0), to: DateTime.new(year,8,30,10,0,0)),
          paymentReceivedDateAug: Faker::Date.between(from: DateTime.new(year,9,20,10,0,0), to: DateTime.new(year,9,30,10,0,0)),
          paymentReceivedDateSep: Faker::Date.between(from: DateTime.new(year,10,20,10,0,0), to: DateTime.new(year,10,30,10,0,0)),
          paymentReceivedDateOct: Faker::Date.between(from: DateTime.new(year,11,20,10,0,0), to: DateTime.new(year,11,30,10,0,0)),
          paymentReceivedDateNov: Faker::Date.between(from: DateTime.new(year,12,20,10,0,0), to: DateTime.new(year,12,30,10,0,0)),
=begin
          paymentReceivedDateNov: Faker::Date.between(from: DateTime.new(year,12,20,10,0,0), to: DateTime.new(year,12,30,10,0,0)),
          paymentReceivedDateDec: Faker::Date.between(from: DateTime.new((year.to_i + 1),1,20,10,0,0), to: DateTime.new((year.to_i + 1),1,30,10,0,0)),
=end
    )

    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: (Date.today - 2.days), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph, jobRequirements: Faker::Lorem.paragraph, skillsStatus: Faker::Lorem.paragraph, interventionDescription: Faker::Lorem.paragraph)
    #Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-01-01', to: '2020-01-31'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph, clientProgress: Faker::Lorem.paragraph, jobRequirements: Faker::Lorem.paragraph, skillsStatus: Faker::Lorem.paragraph, interventionDescription: Faker::Lorem.paragraph)
    #Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-02-01', to: '2020-02-28'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph, clientProgress: Faker::Lorem.paragraph, jobRequirements: Faker::Lorem.paragraph, skillsStatus: Faker::Lorem.paragraph, interventionDescription: Faker::Lorem.paragraph)
    #Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-03-01', to: '2020-03-31'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph, clientProgress: Faker::Lorem.paragraph, jobRequirements: Faker::Lorem.paragraph, skillsStatus: Faker::Lorem.paragraph, interventionDescription: Faker::Lorem.paragraph)
    #Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-04-01', to: '2020-04-30'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph, clientProgress: Faker::Lorem.paragraph, jobRequirements: Faker::Lorem.paragraph, skillsStatus: Faker::Lorem.paragraph, interventionDescription: Faker::Lorem.paragraph)
    #Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-05-01', to: '2020-05-31'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph, clientProgress: Faker::Lorem.paragraph, jobRequirements: Faker::Lorem.paragraph, skillsStatus: Faker::Lorem.paragraph, interventionDescription: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-06-01', to: '2020-06-30'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph, jobRequirements: Faker::Lorem.paragraph, skillsStatus: Faker::Lorem.paragraph, interventionDescription: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-07-01', to: '2020-07-31'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph, jobRequirements: Faker::Lorem.paragraph, skillsStatus: Faker::Lorem.paragraph, interventionDescription: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-08-01', to: '2020-08-31'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph, jobRequirements: Faker::Lorem.paragraph, skillsStatus: Faker::Lorem.paragraph, interventionDescription: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-09-01', to: '2020-09-30'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph, jobRequirements: Faker::Lorem.paragraph, skillsStatus: Faker::Lorem.paragraph, interventionDescription: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-10-01', to: '2020-10-31'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph, jobRequirements: Faker::Lorem.paragraph, skillsStatus: Faker::Lorem.paragraph, interventionDescription: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-11-01', to: '2020-11-30'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph, jobRequirements: Faker::Lorem.paragraph, skillsStatus: Faker::Lorem.paragraph, interventionDescription: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-12-01', to: '2020-12-31'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph, jobRequirements: Faker::Lorem.paragraph, skillsStatus: Faker::Lorem.paragraph, interventionDescription: Faker::Lorem.paragraph)

    Punch.create(date: (Date.today - 2.days), employee: emp_count, intime: "18:00", outtime: "18:15", service: "Notes", description: "3 client notes for vists today")
    Punch.create(date: (Date.today + 2), employee: emp_count, intime: "09:00", outtime: "11:00", service: "Scheduled Intervention", description: "Visting client at this time")
    Punch.create(date: ( Date.today + 6 ), employee: emp_count, intime: "08:00", outtime: "12:00", service: "Meeting", description: "Department review and update.")
    Punch.create(date: ( Date.today + 6 ), employee: emp_count, intime: "07:00", outtime: "07:30", service: "Travel", description: "To Meeting from office, from meeting to office.")
    Punch.create(date: ( Date.today + 14), employee: emp_count, intime: "12:00", outtime: "18:00", service: "Training", description: "Remote training.")

  #Authorization.create(
     # clientID: client_count,
     # voucherNumber: Faker::Number.number(digits: 6),
     # voucherOrAuthorizationDate: Faker::Date.between(from: '2020-08-01', to: '2020-08-10'),
     # serviceType: "1",
     # hoursAuthorized: "20",
     # VoucherRate: "53.00",
     # VoucherGoal: "Maintain Employment",
   # )

   # Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-08-11', to: '2020-08-31'),
       #                 startTime: "08:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)
    #Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-09-01', to: '2020-09-30'),
      #                  startTime: "08:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)
   # Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-10-01', to: '2020-10-31'),
     #                   startTime: "08:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)
   # Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-11-01', to: '2020-11-30'),
    #                    startTime: "08:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)



Dir[Rails.root.join('db/seeds/*.rb')].sort.each do |file|
puts "Processing #{file.split('/').last}"
require file
end


