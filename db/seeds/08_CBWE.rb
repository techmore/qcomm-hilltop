
emp_count = 5
client_count = 210
auth_count = 628

3.times do
  emp_count = emp_count + 1

=begin  Employee.create( 
          employee: Faker::Name.unique.name, 
          userType: "JobCoach",  
          email: Faker::Internet.email, 
          start_date: Date.today
=end  )

  3.times do 
    client_count = client_count + 1
    auth_count = auth_count + 1
    Client.create( 
          clientFirstName: Faker::Name.first_name, 
          clientLastName: Faker::Name.last_name, 
          clientAddress1: Faker::Address.street_address, 
          clientCity: Faker::Address.city, 
          clientCounty: "USA", 
          clientState: "NJ", 
          clientZip: Faker::Address.zip, 
          clientMobilePhone: Faker::PhoneNumber.cell_phone, 
          clientEmail: Faker::Internet.email, 
          clientSSN: Faker::Number.number(digits: 9), 
          clientDOB: Faker::Date.birthday(min_age: 18, max_age: 65), 
          EmergencyContact1Name: Faker::Name.unique.name ,
          EmergencyContact1Cell: Faker::PhoneNumber.cell_phone, 
          EmergencyContact2Name: Faker::Name.unique.name ,
          EmergencyContact2Cell: Faker::PhoneNumber.cell_phone,
          participantID: Faker::Number.number(digits: 6)
         )
    Assignment.create( 
          ClientID: client_count, 
          Employee: emp_count, 
          DateAssigned: Date.today
          )
    Clientemployer.create(
          clientID: client_count, 
          clientEmployerID: "2", 
          startDate: (Date.today - 365.days), 
          hoursPerMonth: "40", 
          JobTitle: Faker::Job.title, 
          SupervisorName: Faker::Name.unique.name, 
          SupervisorPhone: Faker::PhoneNumber.cell_phone, 
          WorkSchedule: "M-F, 8-4", 
          Wage: "15"
        )
  Authorization.create(
      clientID: client_count,
      voucherNumber: Faker::Number.number(digits: 6),
      voucherOrAuthorizationDate: Faker::Date.between(from: '2020-01-01', to: '2020-01-31'),
       serviceType: "22",
      intake: "2",
      hoursAuthorized: "6"
      
    )

    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-02-01', to: '2020-02-28'),
                        startTime: "08:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-03-01', to: '2020-03-31'),
                        startTime: "08:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-04-01', to: '2020-04-30'),
                        startTime: "08:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-05-01', to: '2020-05-31'),
                        startTime: "08:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)

    Otf.create(
      date: Faker::Date.between(from: '2020-12-01', to: '2020-12-28' ),
      authid: auth_count,
      emp1: "Site1: " + Faker::Number.between(from: 1, to: 4).to_s,
      emp2: Faker::Number.between(from: 1, to: 4),
      emp3: Faker::Number.between(from: 1, to: 4),
      emp4: Faker::Number.between(from: 1, to: 4),
      emp5: Faker::Number.between(from: 1, to: 4),
      emp6: Faker::Number.between(from: 1, to: 4),
      emp7: Faker::Number.between(from: 1, to: 4),
      emp8: Faker::Number.between(from: 1, to: 4),
      emp9: Faker::Number.between(from: 1, to: 4),
      emp10: Faker::Number.between(from: 1, to: 4),
      emp11: Faker::Number.between(from: 1, to: 4),
      emp12: Faker::Number.between(from: 1, to: 4),
      emp13: Faker::Number.between(from: 1, to: 4),
      emp14: Faker::Number.between(from: 1, to: 4),
      emp15: Faker::Number.between(from: 1, to: 4),
      emp16: Faker::Number.between(from: 1, to: 4),
      emp17: Faker::Number.between(from: 1, to: 4),
      emp18: Faker::Number.between(from: 1, to: 4),
      emp19: Faker::Number.between(from: 1, to: 4),
      emp20: Faker::Number.between(from: 1, to: 4),
      emp21: Faker::Number.between(from: 1, to: 4),
      emp22: Faker::Number.between(from: 1, to: 4),
      emp23: Faker::Number.between(from: 1, to: 4),
      phy1: Faker::Number.between(from: 1, to: 4),
      phy2: Faker::Number.between(from: 1, to: 4),
      phy3: Faker::Number.between(from: 1, to: 4),
      phy4: Faker::Number.between(from: 1, to: 4),
      phy5: Faker::Number.between(from: 1, to: 4),
      phy6: Faker::Number.between(from: 1, to: 4),
      phy7: Faker::Number.between(from: 1, to: 4),
      phy8: Faker::Number.between(from: 1, to: 4),
      phy9: Faker::Number.between(from: 1, to: 4),
      phy10: Faker::Number.between(from: 1, to: 4),
      env1: "na",
      env2: "na",
      env3: "na",
      env4: "na",
      env5: "na",
      emp1comment: Faker::Lorem.paragraph,
      emp2comment: Faker::Lorem.paragraph,
      emp3comment: Faker::Lorem.paragraph,
      emp4comment: Faker::Lorem.paragraph,
      emp5comment: Faker::Lorem.paragraph,
      emp6comment: Faker::Lorem.paragraph,
      emp7comment: Faker::Lorem.paragraph,
      emp8comment: Faker::Lorem.paragraph,
      emp9comment: Faker::Lorem.paragraph,
      emp10comment: Faker::Lorem.paragraph,
      emp11comment: Faker::Lorem.paragraph,
      emp12comment: Faker::Lorem.paragraph,
      emp13comment: Faker::Lorem.paragraph,
      emp14comment: Faker::Lorem.paragraph,
      emp15comment: Faker::Lorem.paragraph,
      emp16comment: Faker::Lorem.paragraph,
      emp17comment: Faker::Lorem.paragraph,
      emp18comment: Faker::Lorem.paragraph,
      emp19comment: Faker::Lorem.paragraph,
      emp20comment: Faker::Lorem.paragraph,
      emp21comment: Faker::Lorem.paragraph,
      emp22comment: Faker::Lorem.paragraph,
      emp23comment: Faker::Lorem.paragraph,
      phy1comment: Faker::Lorem.paragraph,
      phy2comment: Faker::Lorem.paragraph,
      phy3comment: Faker::Lorem.paragraph,
      phy4comment: Faker::Lorem.paragraph,
      phy5comment: Faker::Lorem.paragraph,
      phy6comment: Faker::Lorem.paragraph,
      phy7comment: Faker::Lorem.paragraph,
      phy8comment: Faker::Lorem.paragraph,
      phy9comment: Faker::Lorem.paragraph,
      phy10comment: Faker::Lorem.paragraph,
      env1comment: Faker::Lorem.paragraph,
      env2comment: Faker::Lorem.paragraph,
      env3comment: Faker::Lorem.paragraph,
      env4comment: Faker::Lorem.paragraph,
      env5comment: Faker::Lorem.paragraph)

Otfcompletion.create(
      formtype: "CBWE",
      agency: "Center for Educational Advancement",
      vrc: "",
      siteName1: "1",
      Address1: "",
      Dates1: "",
      specialist1: emp_count,
      totalSiteTime1: "",
      numberOfDays1: "",
      aveHoursPerDay1: "",
      JobsPerformed1: "",
      Dates2: "",
      specialist2: emp_count,
      totalSiteTime2: "",
      numberOfDays2: "",
      aveHoursPerDay2: "",
      JobsPerformed2: "",
      Dates3: "",
      specialist3: emp_count,
      totalSiteTime3: "",
      numberOfDays3: "",
      aveHoursPerDay3: "",
      JobsPerformed3: "",
      summary: Faker::Lorem.paragraph,
      comments: Faker::Lorem.paragraph,
      q1: Faker::Lorem.paragraph,
      q2: Faker::Lorem.paragraph,
      q3: Faker::Lorem.paragraph,
      q4: Faker::Lorem.paragraph,
      q5: Faker::Lorem.paragraph,
      q6: Faker::Lorem.paragraph)


    year = 2015
    
    2.times do
       Authorization.create(
          clientID: client_count, 
          voucherNumber: Faker::Number.number(digits: 6), 
          voucherOrAuthorizationDate: DateTime.new(year,1,1,0,0,1),
          serviceType: "13", 
          hoursAuthorized: "20",
          VoucherRate: "55.60",
          VoucherGoal: "Maintain Employment",
          voucherInvoiceDate: Faker::Date.between(from: DateTime.new(year,10,1,10,0,0), to: DateTime.new(year,10,25,10,0,0)),
          voucherAmountInvoiced: "1060",
          VoucherInvoiceNumber: Faker::Number.number(digits: 6),
          voucherPaymentReceivedDate: Faker::Date.between(from: DateTime.new(year,12,1,10,0,0), to: DateTime.new(year,12,25,10,0,0))
           )
        month = 2
        auth_count = auth_count + 1
        5.times do
               Intervention.create(
                  employeeID: emp_count, 
                  workAuthorizationID: auth_count, 
                  dateOfIntervention: Faker::Date.between(from: DateTime.new(year,month,1,10,0,0), to: DateTime.new(year,month,15,10,0,0)), 
                  startTime: "08:00", 
                  endTime: "12:00", 
                  generalComments: Faker::Lorem.paragraph
                  )
               Intervention.create(
                  employeeID: emp_count,
                  workAuthorizationID: auth_count,
                  dateOfIntervention: Faker::Date.between(from: DateTime.new(year,month,16,10,0,0), to: DateTime.new(year,month,28,10,0,0)),
                  startTime: "08:00",
                  endTime: "12:00",
                  generalComments: Faker::Lorem.paragraph
                  )


              Punch.create(date: Faker::Date.between(from: DateTime.new(year,month,25,10,0,0), to: DateTime.new(year,month,28,10,0,0)), 
                     employee: emp_count, intime: "18:00", outtime: "18:15", service: "Notes", description: "LTFA notes for client")
              month = month + 1
        end      
        year = year + 1
     end
  end
end
