emp_count = 4
client_count = 1
auth_count = 1
ltfa = "LTFA-"

5.times do
  emp_count = emp_count + 1

  Employee.create( 
          employee: Faker::Name.unique.name, 
          userType: "JobCoach",  
          email: Faker::Internet.email, 
          start_date: Date.today
  )
  10.times do 
    client_count = client_count + 1
    Client.create( 
          clientFirstName: Faker::Name.first_name, 
          clientLastName: Faker::Name.last_name, 
          clientAddress1: Faker::Address.street_address, 
          clientCity: Faker::Address.city, 
          clientCounty: "USA", 
          clientState: "NJ", 
          clientZip: Faker::Address.zip, 
          clientMobilePhone: Faker::PhoneNumber.cell_phone, 
          clientEmail: Faker::Internet.email, 
          clientSSN: Faker::Number.number(digits: 9), 
          clientDOB: Faker::Date.birthday(min_age: 18, max_age: 65), 
          EmergencyContact1Name: Faker::Name.unique.name ,
          EmergencyContact1Cell: Faker::PhoneNumber.cell_phone, 
          EmergencyContact2Name: Faker::Name.unique.name ,
          EmergencyContact2Cell: Faker::PhoneNumber.cell_phone,
          participantID: Faker::Number.number(digits: 6)
         )
    Assignment.create( 
          ClientID: client_count, 
          Employee: emp_count, 
          DateAssigned: Date.today
          )
    Clientemployer.create(
          clientID: client_count, 
          clientEmployerID: "1", 
          startDate: (Date.today - 365.days), 
          hoursPerMonth: "40", 
          JobTitle: Faker::Job.title, 
          SupervisorName: Faker::Name.unique.name, 
          SupervisorPhone: Faker::PhoneNumber.cell_phone, 
          WorkSchedule: "M-F, 8-4", 
          Wage: "15"
        )
    year = 2020
    auth_count = auth_count + 1
  Authorization.create(
      clientID: client_count,
      voucherNumber: "LTFA-2020",
      voucherOrAuthorizationDate: "01-01-2020",
      serviceType: "3",
      hoursAuthorized: "3",
      VoucherRate: "53.00",
      VoucherGoal: "Maintain Employment",
    #  invoiceDateJan: DateTime.new(year,2,10,10,0,0),
    #  invoiceAmountJan: "159.00",
    #  invoiceNumberJan: Faker::Number.number(digits: 6),
    #  invoiceDateFeb: DateTime.new(year,3,10,10,0,0),
    #  invoiceAmountFeb: "159.00",
    #  invoiceNumberFeb: Faker::Number.number(digits: 6),
    #  invoiceDateMar: DateTime.new(year,4,10,10,0,0),
    #  invoiceAmountMar: "159.00",
    #  invoiceNumberMar: Faker::Number.number(digits: 6),
    #  invoiceDateApr: DateTime.new(year,5,10,10,0,0),
    #  invoiceAmountApr: "159.00",
    #  invoiceNumberApr: Faker::Number.number(digits: 6),
    #  invoiceDateMay: DateTime.new(year,6,10,10,0,0),
    #  invoiceAmountMay: "159.00",
    #  invoiceNumberMay: Faker::Number.number(digits: 6),
    #  invoiceDateJun: DateTime.new(year,7,10,10,0,0),
    #  invoiceAmountJun: "159.00",
    #  invoiceNumberJun: Faker::Number.number(digits: 6),
      invoiceDateJul: DateTime.new(year,8,10,10,0,0),
      invoiceAmountJul: "159.00",
      invoiceNumberJul: Faker::Number.number(digits: 6),
      invoiceDateAug: DateTime.new(year,9,10,10,0,0),
      invoiceAmountAug: "159.00",
      invoiceNumberAug: Faker::Number.number(digits: 6),
      invoiceDateSep: DateTime.new(year,10,10,10,0,0),
      invoiceAmountSep: "159.00",
      invoiceNumberSep: Faker::Number.number(digits: 6),
      invoiceDateOct: DateTime.new(year,11,10,10,0,0),
      invoiceAmountOct: "159.00",
      invoiceNumberOct: Faker::Number.number(digits: 6),
      invoiceDateNov: DateTime.new(year,12,10,10,0,0),
      invoiceAmountNov: "159.00",
      invoiceNumberNov: Faker::Number.number(digits: 6),
      #invoiceDateDec: DateTime.new(year,1,10,10,0,0),
      #invoiceAmountDec: "159.00",
      #invoiceNumberDec: Faker::Number.number(digits: 6),
=begin
      invoiceDateNov: DateTime.new(year,12,10,10,0,0),
      invoiceAmountNov: "159.00",
      invoiceNumberNov: Faker::Number.number(digits: 6),
      invoiceDateDec: DateTime.new((year.to_i + 1),1,10,10,0,0),
      invoiceAmountDec: "159.00",
      invoiceNumberDec: Faker::Number.number(digits: 6),
=end
      #    paymentReceivedDateJan: Faker::Date.between(from: DateTime.new(year,2,20,10,0,0), to: DateTime.new(year,2,28,10,0,0)),
      #    paymentReceivedDateFeb: Faker::Date.between(from: DateTime.new(year,3,20,10,0,0), to: DateTime.new(year,3,30,10,0,0)),
      #    paymentReceivedDateMar: Faker::Date.between(from: DateTime.new(year,4,20,10,0,0), to: DateTime.new(year,4,30,10,0,0)),
      #    paymentReceivedDateApr: Faker::Date.between(from: DateTime.new(year,5,20,10,0,0), to: DateTime.new(year,5,30,10,0,0)),
      #    paymentReceivedDateMay: Faker::Date.between(from: DateTime.new(year,6,20,10,0,0), to: DateTime.new(year,6,30,10,0,0)),
      #    paymentReceivedDateJun: Faker::Date.between(from: DateTime.new(year,7,20,10,0,0), to: DateTime.new(year,7,30,10,0,0)),
          paymentReceivedDateJul: Faker::Date.between(from: DateTime.new(year,8,20,10,0,0), to: DateTime.new(year,8,30,10,0,0)),
          paymentReceivedDateAug: Faker::Date.between(from: DateTime.new(year,9,20,10,0,0), to: DateTime.new(year,9,30,10,0,0)),
          paymentReceivedDateSep: Faker::Date.between(from: DateTime.new(year,10,20,10,0,0), to: DateTime.new(year,10,30,10,0,0)),
          paymentReceivedDateOct: Faker::Date.between(from: DateTime.new(year,11,20,10,0,0), to: DateTime.new(year,11,30,10,0,0)),
          paymentReceivedDateNov: Faker::Date.between(from: DateTime.new(year,12,20,10,0,0), to: DateTime.new(year,12,30,10,0,0)),
          paymentReceivedDateDec: Faker::Date.between(from: DateTime.new(year,1,20,10,0,0), to: DateTime.new(year,1,30,10,0,0))
=begin
          paymentReceivedDateNov: Faker::Date.between(from: DateTime.new(year,12,20,10,0,0), to: DateTime.new(year,12,30,10,0,0)),
          paymentReceivedDateDec: Faker::Date.between(from: DateTime.new((year.to_i + 1),1,20,10,0,0), to: DateTime.new((year.to_i + 1),1,30,10,0,0)),
=end
    )

    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-11-01', to: Date.today), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)

    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-01-01', to: '2020-01-31'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-02-01', to: '2020-02-28'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-03-01', to: '2020-03-31'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-04-01', to: '2020-04-30'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-05-01', to: '2020-05-31'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-06-01', to: '2020-06-30'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-07-01', to: '2020-07-31'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-08-01', to: '2020-08-31'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-09-01', to: '2020-09-30'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-10-01', to: '2020-10-31'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-11-01', to: '2020-11-30'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-12-01', to: '2020-12-31'), startTime: "09:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)


    year = 2018
    
    2.times do
       auth_count = auth_count + 1
       Authorization.create(
          clientID: client_count, 
          voucherNumber: (ltfa.to_s + year.to_s), 
          voucherOrAuthorizationDate: DateTime.new(year,1,1,0,0,0),
          serviceType: "3", 
          hoursAuthorized: "3",
          VoucherRate: "53.00",
          VoucherGoal: "Maintain Employment",
          invoiceDateJan: DateTime.new(year,2,10,10,0,0),
          invoiceAmountJan: "159.00",
          invoiceNumberJan: Faker::Number.number(digits: 6),
          invoiceDateFeb: DateTime.new(year,3,10,10,0,0),
          invoiceAmountFeb: "159.00",
          invoiceNumberFeb: Faker::Number.number(digits: 6),
          invoiceDateMar: DateTime.new(year,4,10,10,0,0),
          invoiceAmountMar: "159.00",
          invoiceNumberMar: Faker::Number.number(digits: 6),
          invoiceDateApr: DateTime.new(year,5,10,10,0,0),
          invoiceAmountApr: "159.00",
          invoiceNumberApr: Faker::Number.number(digits: 6),
          invoiceDateMay: DateTime.new(year,6,10,10,0,0),
          invoiceAmountMay: "159.00",
          invoiceNumberMay: Faker::Number.number(digits: 6),
          invoiceDateJun: DateTime.new(year,7,10,10,0,0),
          invoiceAmountJun: "159.00",
          invoiceNumberJun: Faker::Number.number(digits: 6),
          invoiceDateJul: DateTime.new(year,8,10,10,0,0),
          invoiceAmountJul: "159.00",
          invoiceNumberJul: Faker::Number.number(digits: 6),
          invoiceDateAug: DateTime.new(year,9,10,10,0,0),
          invoiceAmountAug: "159.00",
          invoiceNumberAug: Faker::Number.number(digits: 6),
          invoiceDateSep: DateTime.new(year,10,10,10,0,0),
          invoiceAmountSep: "159.00",
          invoiceNumberSep: Faker::Number.number(digits: 6),
          invoiceDateOct: DateTime.new(year,11,10,10,0,0),
          invoiceAmountOct: "159.00",
          invoiceNumberOct: Faker::Number.number(digits: 6),
          invoiceDateNov: DateTime.new(year,12,10,10,0,0),
          invoiceAmountNov: "159.00",
          invoiceNumberNov: Faker::Number.number(digits: 6),
          invoiceDateDec: DateTime.new((year.to_i + 1),1,10,10,0,0),
          invoiceAmountDec: "159.00",
          invoiceNumberDec: Faker::Number.number(digits: 6),
          paymentReceivedDateJan: Faker::Date.between(from: DateTime.new(year,2,20,10,0,0), to: DateTime.new(year,2,28,10,0,0)),
          paymentReceivedDateFeb: Faker::Date.between(from: DateTime.new(year,3,20,10,0,0), to: DateTime.new(year,3,30,10,0,0)),
          paymentReceivedDateMar: Faker::Date.between(from: DateTime.new(year,4,20,10,0,0), to: DateTime.new(year,4,30,10,0,0)),
          paymentReceivedDateApr: Faker::Date.between(from: DateTime.new(year,5,20,10,0,0), to: DateTime.new(year,5,30,10,0,0)),
          paymentReceivedDateMay: Faker::Date.between(from: DateTime.new(year,6,20,10,0,0), to: DateTime.new(year,6,30,10,0,0)),
          paymentReceivedDateJun: Faker::Date.between(from: DateTime.new(year,7,20,10,0,0), to: DateTime.new(year,7,30,10,0,0)),
          paymentReceivedDateJul: Faker::Date.between(from: DateTime.new(year,8,20,10,0,0), to: DateTime.new(year,8,30,10,0,0)),
          paymentReceivedDateAug: Faker::Date.between(from: DateTime.new(year,9,20,10,0,0), to: DateTime.new(year,9,30,10,0,0)),
          paymentReceivedDateSep: Faker::Date.between(from: DateTime.new(year,10,20,10,0,0), to: DateTime.new(year,10,30,10,0,0)),
          paymentReceivedDateOct: Faker::Date.between(from: DateTime.new(year,11,20,10,0,0), to: DateTime.new(year,11,30,10,0,0)), 
          paymentReceivedDateNov: Faker::Date.between(from: DateTime.new(year,12,20,10,0,0), to: DateTime.new(year,12,30,10,0,0)),
          paymentReceivedDateDec: Faker::Date.between(from: DateTime.new((year.to_i + 1),1,20,10,0,0), to: DateTime.new((year.to_i + 1),1,30,10,0,0))
  )
    month = 1
    12.times do
        Intervention.create(
                  employeeID: emp_count, 
                  workAuthorizationID: auth_count, 
                  dateOfIntervention: Faker::Date.between(from: DateTime.new(year,month,1,10,0,0), to: DateTime.new(year,month,25,10,0,0)), 
                  startTime: "09:00", 
                  endTime: "12:00", 
                  generalComments: Faker::Lorem.paragraph
                  )
        Punch.create(date: Faker::Date.between(from: DateTime.new(year,month,25,10,0,0), to: DateTime.new(year,month,28,10,0,0)), 
                     employee: emp_count, intime: "18:00", outtime: "18:15", service: "Notes", description: "LTFA notes for client")
        month = month + 1
      end      
        year = year + 1
     end
  end
end
