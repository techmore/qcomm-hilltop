50.times do
   Employer.create( employerName: Faker::Company.name, employerAddress1: Faker::Address.street_address, employerCity: "Flemington", employerState: "NJ", employerZip: "08822", employerWorkPhone: Faker::PhoneNumber.cell_phone  )
end
