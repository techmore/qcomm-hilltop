emp_count = 4
client_count = 201
auth_count = 601

3.times do
  emp_count = emp_count + 1

=begin  Employee.create( 
          employee: Faker::Name.unique.name, 
          userType: "JobCoach",  
          email: Faker::Internet.email, 
          start_date: Date.today
=end  )

  3.times do 
    client_count = client_count + 1
    auth_count = auth_count + 1
    Client.create( 
          clientFirstName: Faker::Name.first_name, 
          clientLastName: Faker::Name.last_name, 
          clientAddress1: Faker::Address.street_address, 
          clientCity: Faker::Address.city, 
          clientCounty: "USA", 
          clientState: "NJ", 
          clientZip: Faker::Address.zip, 
          clientMobilePhone: Faker::PhoneNumber.cell_phone, 
          clientEmail: Faker::Internet.email, 
          clientSSN: Faker::Number.number(digits: 9), 
          clientDOB: Faker::Date.birthday(min_age: 18, max_age: 65), 
          EmergencyContact1Name: Faker::Name.unique.name ,
          EmergencyContact1Cell: Faker::PhoneNumber.cell_phone, 
          EmergencyContact2Name: Faker::Name.unique.name ,
          EmergencyContact2Cell: Faker::PhoneNumber.cell_phone,
          participantID: Faker::Number.number(digits: 6)
         )
    Assignment.create( 
          ClientID: client_count, 
          Employee: emp_count, 
          DateAssigned: Date.today
          )
    Clientemployer.create(
          clientID: client_count, 
          clientEmployerID: "2", 
          startDate: (Date.today - 365.days), 
          hoursPerMonth: "40", 
          JobTitle: Faker::Job.title, 
          SupervisorName: Faker::Name.unique.name, 
          SupervisorPhone: Faker::PhoneNumber.cell_phone, 
          WorkSchedule: "M-F, 8-4", 
          Wage: "15"
        )
  Authorization.create(
      clientID: client_count,
      voucherNumber: Faker::Number.number(digits: 6),
      voucherOrAuthorizationDate: Faker::Date.between(from: '2020-01-01', to: '2020-01-31'),
      serviceType: "12",
      hoursAuthorized: "20",
      VoucherRate: "55.60",
      VoucherGoal: "Maintain Employment",
      
    )

    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-02-01', to: '2020-02-28'),
                        startTime: "08:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-03-01', to: '2020-03-31'),
                        startTime: "08:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-04-01', to: '2020-04-30'),
                        startTime: "08:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)
    Intervention.create(employeeID: emp_count, workAuthorizationID: auth_count, dateOfIntervention: Faker::Date.between(from: '2020-05-01', to: '2020-05-31'),
                        startTime: "08:00", endTime: "12:00", generalComments: Faker::Lorem.paragraph)

    year = 2018
    
    2.times do
       Authorization.create(
          clientID: client_count, 
          voucherNumber: Faker::Number.number(digits: 6), 
          voucherOrAuthorizationDate: DateTime.new(year,1,1,0,0,1),
          serviceType: "12", 
          hoursAuthorized: "20",
          VoucherRate: "55.60",
          VoucherGoal: "Maintain Employment",
          voucherInvoiceDate: Faker::Date.between(from: DateTime.new(year,10,1,10,0,0), to: DateTime.new(year,10,25,10,0,0)),
          voucherAmountInvoiced: "1060",
          VoucherInvoiceNumber: Faker::Number.number(digits: 6),
          voucherPaymentReceivedDate: Faker::Date.between(from: DateTime.new(year,12,1,10,0,0), to: DateTime.new(year,12,25,10,0,0))
           )
        month = 2
        auth_count = auth_count + 1
        5.times do
               Intervention.create(
                  employeeID: emp_count, 
                  workAuthorizationID: auth_count, 
                  dateOfIntervention: Faker::Date.between(from: DateTime.new(year,month,1,10,0,0), to: DateTime.new(year,month,15,10,0,0)), 
                  startTime: "08:00", 
                  endTime: "12:00", 
                  generalComments: Faker::Lorem.paragraph
                  )
               Intervention.create(
                  employeeID: emp_count,
                  workAuthorizationID: auth_count,
                  dateOfIntervention: Faker::Date.between(from: DateTime.new(year,month,16,10,0,0), to: DateTime.new(year,month,28,10,0,0)),
                  startTime: "08:00",
                  endTime: "12:00",
                  generalComments: Faker::Lorem.paragraph
                  )


              Punch.create(date: Faker::Date.between(from: DateTime.new(year,month,25,10,0,0), to: DateTime.new(year,month,28,10,0,0)), 
                     employee: emp_count, intime: "18:00", outtime: "18:15", service: "Notes", description: "LTFA notes for client")
              month = month + 1
        end      
        year = year + 1
     end
  end
end
