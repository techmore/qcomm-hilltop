# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_01_03_041044) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "activityreports", force: :cascade do |t|
    t.string "clientid"
    t.string "authid"
    t.string "voucher_num"
    t.string "name"
    t.date "date"
    t.string "reporting_period"
    t.string "invoice"
    t.string "hours_provided"
    t.string "dvrs_office"
    t.string "job_goal"
    t.string "completed_by"
    t.string "areas"
    t.string "activities"
    t.string "pp"
    t.string "comments"
    t.string "service"
    t.string "requested_hours"
    t.string "hours_wanted"
    t.string "emp_name"
    t.string "job_title"
    t.string "emp_address"
    t.string "telephone"
    t.string "emp_start"
    t.string "work_schedule"
    t.string "emp_sup"
    t.string "wages"
    t.string "medical"
    t.string "date_medical_start"
    t.string "med_provided"
    t.string "option1"
    t.string "option2"
    t.string "option3"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "assignments", force: :cascade do |t|
    t.string "ClientID"
    t.string "Employee"
    t.date "DateAssigned"
    t.date "DateUnassigned"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_assignments_on_deleted_at"
  end

  create_table "authorizations", force: :cascade do |t|
    t.string "funderID"
    t.string "clientID"
    t.string "voucherNumber"
    t.date "voucherOrAuthorizationDate"
    t.string "counselorName"
    t.string "serviceType"
    t.string "hoursAuthorized"
    t.string "hoursRemaining"
    t.string "totalHoursSupported"
    t.date "voucherInvoiceDate"
    t.string "VoucherInvoiceNumber"
    t.date "voucherPaymentReceivedDate"
    t.string "voucherPaymentReceived"
    t.string "voucherInvoiceAdjustment"
    t.string "voucherPaymentComment"
    t.string "voucherAmountInvoiced"
    t.string "VoucherCompleted"
    t.string "VoucherRate"
    t.string "VoucherGoal"
    t.date "VoucherReceivedDate"
    t.string "VoucherFACounty"
    t.string "VoucherStatus"
    t.string "FlatRateVoucher"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.date "invoiceDateJan"
    t.date "invoiceDateFeb"
    t.date "invoiceDateMar"
    t.date "invoiceDateApr"
    t.date "invoiceDateMay"
    t.date "invoiceDateJun"
    t.date "invoiceDateJul"
    t.date "invoiceDateAug"
    t.date "invoiceDateSep"
    t.date "invoiceDateOct"
    t.date "invoiceDateNov"
    t.date "invoiceDateDec"
    t.decimal "invoiceAmountJan", precision: 6, scale: 2
    t.decimal "invoiceAmountFeb", precision: 6, scale: 2
    t.decimal "invoiceAmountMar", precision: 6, scale: 2
    t.decimal "invoiceAmountApr", precision: 6, scale: 2
    t.decimal "invoiceAmountMay", precision: 6, scale: 2
    t.decimal "invoiceAmountJun", precision: 6, scale: 2
    t.decimal "invoiceAmountJul", precision: 6, scale: 2
    t.decimal "invoiceAmountAug", precision: 6, scale: 2
    t.decimal "invoiceAmountSep", precision: 6, scale: 2
    t.decimal "invoiceAmountOct", precision: 6, scale: 2
    t.decimal "invoiceAmountNov", precision: 6, scale: 2
    t.decimal "invoiceAmountDec", precision: 6, scale: 2
    t.string "invoiceNumberJan"
    t.string "invoiceNumberFeb"
    t.string "invoiceNumberMar"
    t.string "invoiceNumberApr"
    t.string "invoiceNumberMay"
    t.string "invoiceNumberJun"
    t.string "invoiceNumberJul"
    t.string "invoiceNumberAug"
    t.string "invoiceNumberSep"
    t.string "invoiceNumberOct"
    t.string "invoiceNumberNov"
    t.string "invoiceNumberDec"
    t.date "paymentReceivedDateJan"
    t.date "paymentReceivedDateFeb"
    t.date "paymentReceivedDateMar"
    t.date "paymentReceivedDateApr"
    t.date "paymentReceivedDateMay"
    t.date "paymentReceivedDateJun"
    t.date "paymentReceivedDateJul"
    t.date "paymentReceivedDateAug"
    t.date "paymentReceivedDateSep"
    t.date "paymentReceivedDateOct"
    t.date "paymentReceivedDateNov"
    t.date "paymentReceivedDateDec"
    t.decimal "invoiceAdjustmentJan", precision: 6, scale: 2
    t.decimal "invoiceAdjustmentFeb", precision: 6, scale: 2
    t.decimal "invoiceAdjustmentMar", precision: 6, scale: 2
    t.decimal "invoiceAdjustmentApr", precision: 6, scale: 2
    t.decimal "invoiceAdjustmentMay", precision: 6, scale: 2
    t.decimal "invoiceAdjustmentJun", precision: 6, scale: 2
    t.decimal "invoiceAdjustmentJul", precision: 6, scale: 2
    t.decimal "invoiceAdjustmentAug", precision: 6, scale: 2
    t.decimal "invoiceAdjustmentSep", precision: 6, scale: 2
    t.decimal "invoiceAdjustmentOct", precision: 6, scale: 2
    t.decimal "invoiceAdjustmentNov", precision: 6, scale: 2
    t.decimal "invoiceAdjustmentDec", precision: 6, scale: 2
    t.string "paymentCommentJan"
    t.string "paymentCommentFeb"
    t.string "paymentCommentMar"
    t.string "paymentCommentApr"
    t.string "paymentCommentMay"
    t.string "paymentCommentJun"
    t.string "paymentCommentJul"
    t.string "paymentCommentAug"
    t.string "paymentCommentSep"
    t.string "paymentCommentOct"
    t.string "paymentCommentNov"
    t.string "paymentCommentDec"
    t.integer "intake"
    t.index ["deleted_at"], name: "index_authorizations_on_deleted_at"
  end

  create_table "clientemployers", force: :cascade do |t|
    t.string "clientID"
    t.string "clientEmployerID"
    t.date "startDate"
    t.date "endDate"
    t.decimal "hoursPerMonth"
    t.string "JobTitle"
    t.string "SupervisorName"
    t.string "SupervisorPhone"
    t.string "WorkSchedule"
    t.string "WorkTransportation"
    t.decimal "Wage"
    t.string "WageUnit"
    t.string "GetMedical"
    t.string "MedicalType"
    t.date "DateMedicalStart"
    t.string "TerminationType"
    t.string "TerminationReason"
    t.string "EmployersNotes"
    t.string "OccupationCode"
    t.string "SEModel"
    t.string "TypeofJob"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_clientemployers_on_deleted_at"
  end

  create_table "clients", force: :cascade do |t|
    t.string "clientID"
    t.string "clientFirstName"
    t.string "clientLastName"
    t.string "clientMiddleName"
    t.string "ClientRace"
    t.string "ClientGender"
    t.string "clientAddress1"
    t.string "clientAddress2"
    t.string "clientCity"
    t.string "clientCounty"
    t.string "clientState"
    t.string "clientZip"
    t.string "clientHomePhone"
    t.string "clientMobilePhone"
    t.string "ClientProgram"
    t.string "clientEmail"
    t.string "clientSSN"
    t.date "clientDOB"
    t.date "ClientStartDate"
    t.date "ClientFAStartDate"
    t.date "ClientServiceStopDate"
    t.string "ClientTypeOfTermination"
    t.date "ClientReferralDate"
    t.string "ClientReferredBy"
    t.string "ReferralSourceNumber"
    t.string "ClientPrimaryImpairment"
    t.string "ClientPrimarySource"
    t.string "ClientSecondaryImpairment"
    t.string "ClientSecondarySource"
    t.string "ClientNotes"
    t.string "participantID"
    t.string "DDDSerialNbr"
    t.string "EmergencyContact1Name"
    t.string "EmergencyContact1Relationship"
    t.string "EmergencyContact1Home"
    t.string "EmergencyContact1Work"
    t.string "EmergencyContact1WorkExt"
    t.string "EmergencyContact1Cell"
    t.string "EmergencyContact2Name"
    t.string "EmergencyContact2Relationship"
    t.string "EmergencyContact2Home"
    t.string "EmergencyContact2Work"
    t.string "EmergencyContact2WorkExt"
    t.string "EmergencyContact2Cell"
    t.string "GuardianName"
    t.string "GuardinaRelationship"
    t.string "GuardianHome"
    t.string "GuardianWork"
    t.string "GuardianWorkExt"
    t.string "GuardianCell"
    t.string "GuardianAddress1"
    t.string "GuardianAddress2"
    t.string "GuardianCity"
    t.string "GuardianState"
    t.string "GuardianZip"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_clients_on_deleted_at"
  end

  create_table "controllers", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "cea_people"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_controllers_on_email", unique: true
    t.index ["reset_password_token"], name: "index_controllers_on_reset_password_token", unique: true
  end

  create_table "eestatuscodes", force: :cascade do |t|
    t.string "client"
    t.string "statuscode"
    t.date "startdate"
    t.date "enddate"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "employees", force: :cascade do |t|
    t.string "employee"
    t.string "email"
    t.string "userType"
    t.date "start_date"
    t.date "end_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.string "ee"
    t.string "signature"
    t.index ["deleted_at"], name: "index_employees_on_deleted_at"
  end

  create_table "employers", force: :cascade do |t|
    t.string "employerID"
    t.string "employerName"
    t.string "employerCompanyMainContact"
    t.string "employerAddress1"
    t.string "employerAddress2"
    t.string "employerCity"
    t.string "employerCounty"
    t.string "employerState"
    t.string "employerZip"
    t.string "employerWorkPhone"
    t.string "employerFaxNumber"
    t.string "employerEmail"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_employers_on_deleted_at"
  end

  create_table "features", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "status"
    t.string "votes"
    t.string "internal"
    t.string "public"
    t.string "priority"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "hourlyrates", force: :cascade do |t|
    t.string "client"
    t.string "hourlyrate"
    t.date "startdate"
    t.date "enddate"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "impressions", force: :cascade do |t|
    t.string "impressionable_type"
    t.integer "impressionable_id"
    t.integer "user_id"
    t.string "controller_name"
    t.string "action_name"
    t.string "view_name"
    t.string "request_hash"
    t.string "ip_address"
    t.string "session_hash"
    t.text "message"
    t.text "referrer"
    t.text "params"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["controller_name", "action_name", "ip_address"], name: "controlleraction_ip_index"
    t.index ["controller_name", "action_name", "request_hash"], name: "controlleraction_request_index"
    t.index ["controller_name", "action_name", "session_hash"], name: "controlleraction_session_index"
    t.index ["impressionable_type", "impressionable_id", "ip_address"], name: "poly_ip_index"
    t.index ["impressionable_type", "impressionable_id", "params"], name: "poly_params_request_index"
    t.index ["impressionable_type", "impressionable_id", "request_hash"], name: "poly_request_index"
    t.index ["impressionable_type", "impressionable_id", "session_hash"], name: "poly_session_index"
    t.index ["impressionable_type", "message", "impressionable_id"], name: "impressionable_type_message_index"
    t.index ["user_id"], name: "index_impressions_on_user_id"
  end

  create_table "interventions", force: :cascade do |t|
    t.string "interventionID"
    t.string "employeeID"
    t.string "EmploymentHistoryID"
    t.string "employerID"
    t.string "workAuthorizationID"
    t.date "dateOfIntervention"
    t.time "startTime"
    t.time "endTime"
    t.string "jobRequirements"
    t.string "skillsStatus"
    t.string "interventionDescription"
    t.string "clientProgress"
    t.string "generalComments"
    t.string "prePlacementActivityID"
    t.string "ActivityConductedID"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.string "service"
    t.index ["deleted_at"], name: "index_interventions_on_deleted_at"
  end

  create_table "otfcompletions", force: :cascade do |t|
    t.string "formtype"
    t.string "authid"
    t.string "agency"
    t.string "vrc"
    t.string "siteName1"
    t.string "Address1"
    t.string "Dates1"
    t.string "specialist1"
    t.string "totalSiteTime1"
    t.string "numberOfDays1"
    t.string "aveHoursPerDay1"
    t.string "JobsPerformed1"
    t.string "siteName2"
    t.string "Address2"
    t.string "Dates2"
    t.string "specialist2"
    t.string "totalSiteTime2"
    t.string "numberOfDays2"
    t.string "aveHoursPerDay2"
    t.string "JobsPerformed2"
    t.string "siteName3"
    t.string "Address3"
    t.string "Dates3"
    t.string "specialist3"
    t.string "totalSiteTime3"
    t.string "numberOfDays3"
    t.string "aveHoursPerDay3"
    t.string "JobsPerformed3"
    t.text "summary"
    t.text "comments"
    t.text "q1"
    t.text "q2"
    t.text "q3"
    t.text "q4"
    t.text "q5"
    t.text "q6"
    t.text "signature"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "otfs", force: :cascade do |t|
    t.date "date"
    t.time "startTime"
    t.time "endTime"
    t.string "authid"
    t.string "employee"
    t.string "employer"
    t.string "emp1"
    t.string "emp2"
    t.string "emp3"
    t.string "emp4"
    t.string "emp5"
    t.string "emp6"
    t.string "emp7"
    t.string "emp8"
    t.string "emp9"
    t.string "emp10"
    t.string "emp11"
    t.string "emp12"
    t.string "emp13"
    t.string "emp14"
    t.string "emp15"
    t.string "emp16"
    t.string "emp17"
    t.string "emp18"
    t.string "emp19"
    t.string "emp20"
    t.string "emp21"
    t.string "emp22"
    t.string "emp23"
    t.string "phy1"
    t.string "phy2"
    t.string "phy3"
    t.string "phy4"
    t.string "phy5"
    t.string "phy6"
    t.string "phy7"
    t.string "phy8"
    t.string "phy9"
    t.string "phy10"
    t.string "env1"
    t.string "env2"
    t.string "env3"
    t.string "env4"
    t.string "env5"
    t.text "emp1comment"
    t.text "emp2comment"
    t.text "emp3comment"
    t.text "emp4comment"
    t.text "emp5comment"
    t.text "emp6comment"
    t.text "emp7comment"
    t.text "emp8comment"
    t.text "emp9comment"
    t.text "emp10comment"
    t.text "emp11comment"
    t.text "emp12comment"
    t.text "emp13comment"
    t.text "emp14comment"
    t.text "emp15comment"
    t.text "emp16comment"
    t.text "emp17comment"
    t.text "emp18comment"
    t.text "emp19comment"
    t.text "emp20comment"
    t.text "emp21comment"
    t.text "emp22comment"
    t.text "emp23comment"
    t.text "phy1comment"
    t.text "phy2comment"
    t.text "phy3comment"
    t.text "phy4comment"
    t.text "phy5comment"
    t.text "phy6comment"
    t.text "phy7comment"
    t.text "phy8comment"
    t.text "phy9comment"
    t.text "phy10comment"
    t.text "env1comment"
    t.text "env2comment"
    t.text "env3comment"
    t.text "env4comment"
    t.text "env5comment"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "punches", force: :cascade do |t|
    t.date "date"
    t.string "employee"
    t.time "intime"
    t.time "outtime"
    t.string "service"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.string "client"
    t.index ["deleted_at"], name: "index_punches_on_deleted_at"
  end

  create_table "twers", force: :cascade do |t|
    t.date "date"
    t.string "client"
    t.string "employer"
    t.string "days"
    t.string "hours"
    t.string "week1"
    t.string "final"
    t.string "duties"
    t.string "q1"
    t.string "q2"
    t.string "q3"
    t.string "q4"
    t.string "wp1"
    t.string "wp2"
    t.string "wp3"
    t.string "wp4"
    t.string "wp5"
    t.string "wp6"
    t.string "wp7"
    t.string "q1c"
    t.string "q2c"
    t.string "q3c"
    t.string "q4c"
    t.string "wp1c"
    t.string "wp2c"
    t.string "wp3c"
    t.string "wp4c"
    t.string "wp5c"
    t.string "wp6c"
    t.string "wp7c"
    t.string "ss1"
    t.string "ss2"
    t.string "ss3"
    t.string "ss4"
    t.string "ss5"
    t.string "ss6"
    t.string "ss7"
    t.string "ss8"
    t.string "c1"
    t.string "c2"
    t.string "c3"
    t.string "c4"
    t.string "coachsig"
    t.date "sidgdate"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "votltfas", force: :cascade do |t|
    t.date "date"
    t.string "provider"
    t.string "client"
    t.string "rate"
    t.string "reason"
    t.string "empsig"
    t.string "providerstaff"
    t.string "empname"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
end
