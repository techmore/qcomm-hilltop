class CreatePunches < ActiveRecord::Migration[6.0]
  def change
    create_table :punches do |t|
      t.date :date
      t.string :employee
      t.time :intime
      t.time :outtime
      t.string :service
      t.string :description

      t.timestamps
    end
  end
end
