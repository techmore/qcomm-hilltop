class AddYearlyToAuthorization < ActiveRecord::Migration[6.0]
  def change
    add_column :authorizations, :invoiceDateJan, :date
    add_column :authorizations, :invoiceDateFeb, :date
    add_column :authorizations, :invoiceDateMar, :date
    add_column :authorizations, :invoiceDateApr, :date
    add_column :authorizations, :invoiceDateMay, :date
    add_column :authorizations, :invoiceDateJun, :date
    add_column :authorizations, :invoiceDateJul, :date
    add_column :authorizations, :invoiceDateAug, :date
    add_column :authorizations, :invoiceDateSep, :date
    add_column :authorizations, :invoiceDateOct, :date
    add_column :authorizations, :invoiceDateNov, :date
    add_column :authorizations, :invoiceDateDec, :date

    add_column :authorizations, :invoiceAmountJan, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAmountFeb, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAmountMar, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAmountApr, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAmountMay, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAmountJun, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAmountJul, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAmountAug, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAmountSep, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAmountOct, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAmountNov, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAmountDec, :decimal, :precision => 6, :scale => 2

    add_column :authorizations, :invoiceNumberJan, :string
    add_column :authorizations, :invoiceNumberFeb, :string
    add_column :authorizations, :invoiceNumberMar, :string
    add_column :authorizations, :invoiceNumberApr, :string
    add_column :authorizations, :invoiceNumberMay, :string
    add_column :authorizations, :invoiceNumberJun, :string
    add_column :authorizations, :invoiceNumberJul, :string
    add_column :authorizations, :invoiceNumberAug, :string
    add_column :authorizations, :invoiceNumberSep, :string
    add_column :authorizations, :invoiceNumberOct, :string
    add_column :authorizations, :invoiceNumberNov, :string
    add_column :authorizations, :invoiceNumberDec, :string

    add_column :authorizations, :paymentReceivedDateJan, :date
    add_column :authorizations, :paymentReceivedDateFeb, :date
    add_column :authorizations, :paymentReceivedDateMar, :date
    add_column :authorizations, :paymentReceivedDateApr, :date
    add_column :authorizations, :paymentReceivedDateMay, :date
    add_column :authorizations, :paymentReceivedDateJun, :date
    add_column :authorizations, :paymentReceivedDateJul, :date
    add_column :authorizations, :paymentReceivedDateAug, :date
    add_column :authorizations, :paymentReceivedDateSep, :date
    add_column :authorizations, :paymentReceivedDateOct, :date
    add_column :authorizations, :paymentReceivedDateNov, :date
    add_column :authorizations, :paymentReceivedDateDec, :date

    add_column :authorizations, :invoiceAdjustmentJan, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAdjustmentFeb, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAdjustmentMar, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAdjustmentApr, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAdjustmentMay, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAdjustmentJun, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAdjustmentJul, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAdjustmentAug, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAdjustmentSep, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAdjustmentOct, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAdjustmentNov, :decimal, :precision => 6, :scale => 2
    add_column :authorizations, :invoiceAdjustmentDec, :decimal, :precision => 6, :scale => 2

    add_column :authorizations, :paymentCommentJan, :string
    add_column :authorizations, :paymentCommentFeb, :string
    add_column :authorizations, :paymentCommentMar, :string
    add_column :authorizations, :paymentCommentApr, :string
    add_column :authorizations, :paymentCommentMay, :string
    add_column :authorizations, :paymentCommentJun, :string
    add_column :authorizations, :paymentCommentJul, :string
    add_column :authorizations, :paymentCommentAug, :string
    add_column :authorizations, :paymentCommentSep, :string
    add_column :authorizations, :paymentCommentOct, :string
    add_column :authorizations, :paymentCommentNov, :string
    add_column :authorizations, :paymentCommentDec, :string

   end
end
