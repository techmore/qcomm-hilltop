class CreateClients < ActiveRecord::Migration[6.0]
  def change
    create_table :clients do |t|
      t.string :clientID
      t.string :clientFirstName
      t.string :clientLastName
      t.string :clientMiddleName
      t.string :ClientRace
      t.string :ClientGender
      t.string :clientAddress1
      t.string :clientAddress2
      t.string :clientCity
      t.string :clientCounty
      t.string :clientState
      t.string :clientZip
      t.string :clientHomePhone
      t.string :clientMobilePhone
      t.string :ClientProgram
      t.string :clientEmail
      t.string :clientSSN
      t.date :clientDOB
      t.date :ClientStartDate
      t.date :ClientFAStartDate
      t.date :ClientServiceStopDate
      t.string :ClientTypeOfTermination
      t.date :ClientReferralDate
      t.string :ClientReferredBy
      t.string :ReferralSourceNumber
      t.string :ClientPrimaryImpairment
      t.string :ClientPrimarySource
      t.string :ClientSecondaryImpairment
      t.string :ClientSecondarySource
      t.string :ClientNotes
      t.string :participantID
      t.string :DDDSerialNbr
      t.string :EmergencyContact1Name
      t.string :EmergencyContact1Relationship
      t.string :EmergencyContact1Home
      t.string :EmergencyContact1Work
      t.string :EmergencyContact1WorkExt
      t.string :EmergencyContact1Cell
      t.string :EmergencyContact2Name
      t.string :EmergencyContact2Relationship
      t.string :EmergencyContact2Home
      t.string :EmergencyContact2Work
      t.string :EmergencyContact2WorkExt
      t.string :EmergencyContact2Cell
      t.string :GuardianName
      t.string :GuardinaRelationship
      t.string :GuardianHome
      t.string :GuardianWork
      t.string :GuardianWorkExt
      t.string :GuardianCell
      t.string :GuardianAddress1
      t.string :GuardianAddress2
      t.string :GuardianCity
      t.string :GuardianState
      t.string :GuardianZip
=begin
      #t.string :Googledrive
      # t.string :clientWorkPhone
      # t.string :clientWorkPhoneExt
      # t.string :ClientLivingArrangment
      # t.string :ClientCommMethod
    # t.decimal :ClientFAHours
      #t.string :ClientASL
      #t.string :ReferralPhoneExt
      #t.string :ClientPrimaryMHDisability
      #t.string :ClientSecondaryMHDisability
      #t.string :ClientAllergies
      #t.string :ClientPrimaryNonMHDisability
      #t.string :ClientSecondaryNonMHDisability
      #t.string :ClientMedicalInformation
      #t.string :ClientEducationLevel
      #t.string :CriminalRecord
      #t.string :CriminalRecordDetail
      #t.string :DVRSCaseIDNbr
=end
      t.timestamps
    end
  end
end
