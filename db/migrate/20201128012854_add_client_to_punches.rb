class AddClientToPunches < ActiveRecord::Migration[6.0]
  def change
     add_column :punches, :client, :string
  end
end
