class CreateInterventions < ActiveRecord::Migration[6.0]
  def change
    create_table :interventions do |t|
      t.string :interventionID
      t.string :employeeID
      t.string :EmploymentHistoryID
      t.string :employerID
      t.string :workAuthorizationID
      t.date :dateOfIntervention
      t.time :startTime
      t.time :endTime
      t.string :jobRequirements
      t.string :skillsStatus
      t.string :interventionDescription
      t.string :clientProgress
      t.string :generalComments
      t.string :prePlacementActivityID
      t.string :ActivityConductedID

      t.timestamps
    end
  end
end
