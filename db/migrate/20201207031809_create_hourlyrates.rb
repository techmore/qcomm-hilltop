class CreateHourlyrates < ActiveRecord::Migration[6.0]
  def change
    create_table :hourlyrates do |t|
      t.string :client
      t.string :hourlyrate
      t.date :startdate
      t.date :enddate

      t.timestamps
    end
  end
end
