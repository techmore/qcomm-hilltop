class AddDeletedAtToClientemployers < ActiveRecord::Migration[6.0]
  def change
    add_column :clientemployers, :deleted_at, :datetime
    add_index :clientemployers, :deleted_at
  end
end
