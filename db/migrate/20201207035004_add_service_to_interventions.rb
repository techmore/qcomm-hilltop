class AddServiceToInterventions < ActiveRecord::Migration[6.0]
  def change
    add_column :interventions, :service, :string
  end
end
