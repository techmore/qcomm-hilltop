class CreateAssignments < ActiveRecord::Migration[6.0]
  def change
    create_table :assignments do |t|
      t.string :ClientID
      t.string :Employee
      t.date :DateAssigned
      t.date :DateUnassigned

      t.timestamps
    end
  end
end
