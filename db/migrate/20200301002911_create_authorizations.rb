class CreateAuthorizations < ActiveRecord::Migration[6.0]
  def change
    create_table :authorizations do |t|
      t.string :funderID
      t.string :clientID
      t.string :voucherNumber
      t.date :voucherOrAuthorizationDate
      t.string :counselorName
      t.string :serviceType
      t.string :hoursAuthorized
      t.string :hoursRemaining
      t.string :totalHoursSupported
      t.date :voucherInvoiceDate

=begin
      t.date :InvoiceDateJan
      t.date :InvoiceDateFeb
      t.date :InvoiceDateMar
      t.date :InvoiceDateApr
      t.date :InvoiceDateMay
      t.date :InvoiceDateJun
      t.date :InvoiceDateJul
      t.date :InvoiceDateAug
      t.date :InvoiceDateSep
      t.date :InvoiceDateOct
      t.date :InvoiceDateNov
      t.date :InvoiceDateDec
 
      t.date :InvoiceAmountJan
      t.date :InvoiceAmountFeb
      t.date :InvoiceAmountMar
      t.date :InvoiceAmountApr
      t.date :InvoiceAmountMay
      t.date :InvoiceAmountJun
      t.date :InvoiceAmountJul
      t.date :InvoiceAmountAug
      t.date :InvoiceAmountSep
      t.date :InvoiceAmountOct
      t.date :InvoiceAmountNov
      t.date :InvoiceAmountDec

      t.string :InvoicedNumberJan
      t.string :InvoicedNumberFeb
      t.string :InvoicedNumberMar
      t.string :InvoicedNumberApr
      t.string :InvoicedNumberMay
      t.string :InvoicedNumberJun
      t.string :InvoicedNumberJul
      t.string :InvoicedNumberAug
      t.string :InvoicedNumberSep
      t.string :InvoicedNumberOct
      t.string :InvoicedNumberNov
      t.string :InvoicedNumberDec
=end
      t.string :VoucherInvoiceNumber
      t.date :voucherPaymentReceivedDate
      t.string :voucherPaymentReceived
      t.string :voucherInvoiceAdjustment
      t.string :voucherPaymentComment
      t.string :voucherAmountInvoiced
      t.string :VoucherCompleted
      t.string :VoucherRate
      t.string :VoucherGoal
      t.date :VoucherReceivedDate
      t.string :VoucherFACounty
      t.string :VoucherStatus
      t.string :FlatRateVoucher

      t.timestamps
    end
  end
end
