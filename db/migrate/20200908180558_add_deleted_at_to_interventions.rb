class AddDeletedAtToInterventions < ActiveRecord::Migration[6.0]
  def change
    add_column :interventions, :deleted_at, :datetime
    add_index :interventions, :deleted_at
  end
end
