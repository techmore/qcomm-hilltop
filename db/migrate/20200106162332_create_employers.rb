class CreateEmployers < ActiveRecord::Migration[6.0]
  def change
    create_table :employers do |t|
      t.string :employerID
      t.string :employerName
      t.string :employerCompanyMainContact
      t.string :employerAddress1
      t.string :employerAddress2
      t.string :employerCity
      t.string :employerCounty
      t.string :employerState
      t.string :employerZip
      t.string :employerWorkPhone
      t.string :employerFaxNumber
      t.string :employerEmail

      t.timestamps
    end
  end
end
