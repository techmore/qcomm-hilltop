class CreateTwers < ActiveRecord::Migration[6.0]
  def change
    create_table :twers do |t|
      t.date :date
      t.string :client
      t.string :employer
      t.string :days
      t.string :hours
      t.string :week1
      t.string :final
      t.string :duties
      t.string :q1
      t.string :q2
      t.string :q3
      t.string :q4
      t.string :wp1
      t.string :wp2
      t.string :wp3
      t.string :wp4
      t.string :wp5
      t.string :wp6
      t.string :wp7
      t.string :q1c
      t.string :q2c
      t.string :q3c
      t.string :q4c
      t.string :wp1c
      t.string :wp2c
      t.string :wp3c
      t.string :wp4c
      t.string :wp5c
      t.string :wp6c
      t.string :wp7c
      t.string :ss1
      t.string :ss2
      t.string :ss3
      t.string :ss4
      t.string :ss5
      t.string :ss6
      t.string :ss7
      t.string :ss8
      t.string :c1
      t.string :c2
      t.string :c3
      t.string :c4
      t.string :coachsig
      t.date :sidgdate
      t.timestamps
    end
  end
end
