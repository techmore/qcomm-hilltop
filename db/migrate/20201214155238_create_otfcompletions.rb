class CreateOtfcompletions < ActiveRecord::Migration[6.0]
  def change
    create_table :otfcompletions do |t|
      t.string :formtype
      t.string :authid
      t.string :agency
      t.string :vrc
      t.string :siteName1
      t.string :Address1
      t.string :Dates1
      t.string :specialist1
      t.string :totalSiteTime1
      t.string :numberOfDays1
      t.string :aveHoursPerDay1
      t.string :JobsPerformed1
      t.string :siteName2
      t.string :Address2
      t.string :Dates2
      t.string :specialist2
      t.string :totalSiteTime2
      t.string :numberOfDays2
      t.string :aveHoursPerDay2
      t.string :JobsPerformed2
      t.string :siteName3
      t.string :Address3
      t.string :Dates3
      t.string :specialist3
      t.string :totalSiteTime3
      t.string :numberOfDays3
      t.string :aveHoursPerDay3
      t.string :JobsPerformed3
      t.text :summary
      t.text :comments
      t.text :q1
      t.text :q2
      t.text :q3
      t.text :q4
      t.text :q5
      t.text :q6
      t.text :signature

      t.timestamps
    end
  end
end
