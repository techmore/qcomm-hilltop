class CreateVotltfas < ActiveRecord::Migration[6.0]
  def change
    create_table :votltfas do |t|
      t.date :date
      t.string :provider
      t.string :client
      t.string :rate
      t.string :reason
      t.string :empsig
      t.string :providerstaff
      t.string :empname

      t.timestamps
    end
  end
end
