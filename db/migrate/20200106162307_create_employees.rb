class CreateEmployees < ActiveRecord::Migration[6.0]
  def change
    create_table :employees do |t|
      t.string :employee
      t.string :email
      t.string :userType
      t.date :start_date
      t.date :end_date

      t.timestamps
    end
  end
end
