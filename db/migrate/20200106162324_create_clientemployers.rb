class CreateClientemployers < ActiveRecord::Migration[6.0]
  def change
    create_table :clientemployers do |t|
      t.string :clientID
      t.string :clientEmployerID
      t.date :startDate
      t.date :endDate
      t.decimal :hoursPerMonth
      t.string :JobTitle
      t.string :SupervisorName
      t.string :SupervisorPhone
      t.string :WorkSchedule
      t.string :WorkTransportation
      t.decimal :Wage
      t.string :WageUnit
      t.string :GetMedical
      t.string :MedicalType
      t.date :DateMedicalStart
      t.string :TerminationType
      t.string :TerminationReason
      t.string :EmployersNotes
      t.string :OccupationCode
      t.string :SEModel
      t.string :TypeofJob

      t.timestamps
    end
  end
end
