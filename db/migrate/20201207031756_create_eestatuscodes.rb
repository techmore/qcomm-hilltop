class CreateEestatuscodes < ActiveRecord::Migration[6.0]
  def change
    create_table :eestatuscodes do |t|
      t.string :client
      t.string :statuscode
      t.date :startdate
      t.date :enddate

      t.timestamps
    end
  end
end
