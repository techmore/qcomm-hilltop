class CreateActivityreports < ActiveRecord::Migration[6.0]
  def change
    create_table :activityreports do |t|
      t.string :clientid
      t.string :authid
      t.string :voucher_num
      t.string :name
      t.date :date
      t.string :reporting_period
      t.string :invoice
      t.string :hours_provided
      t.string :dvrs_office
      t.string :job_goal
      t.string :completed_by
      t.string :areas
      t.string :activities
      t.string :pp
      t.string :comments
      t.string :service
      t.string :requested_hours
      t.string :hours_wanted
      t.string :emp_name
      t.string :job_title
      t.string :emp_address
      t.string :telephone
      t.string :emp_start
      t.string :work_schedule
      t.string :emp_sup
      t.string :wages
      t.string :medical
      t.string :date_medical_start
      t.string :med_provided
      t.string :option1
      t.string :option2
      t.string :option3

      t.timestamps
    end
  end
end
