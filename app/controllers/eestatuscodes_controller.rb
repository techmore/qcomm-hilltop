class EestatuscodesController < ApplicationController
  before_action :set_eestatuscode, only: [:show, :edit, :update, :destroy]

  # GET /eestatuscodes
  # GET /eestatuscodes.json
  def index
    @eestatuscodes = Eestatuscode.all
  end

  # GET /eestatuscodes/1
  # GET /eestatuscodes/1.json
  def show
    @clients = Client.all
  end

  # GET /eestatuscodes/new
  def new
    @eestatuscode = Eestatuscode.new
    @clients = Client.all
    @authorizations = Authorization.all
    @clientid = params[:clientid]

  end

  # GET /eestatuscodes/1/edit
  def edit
    @clientid = params[:clientid]
    @clients = Client.all
  end

  # POST /eestatuscodes
  # POST /eestatuscodes.json
  def create
    @eestatuscode = Eestatuscode.new(eestatuscode_params)

    respond_to do |format|
      if @eestatuscode.save
        format.html { redirect_to @eestatuscode, notice: 'Eestatuscode was successfully created.' }
        format.json { render :show, status: :created, location: @eestatuscode }
      else
        format.html { render :new }
        format.json { render json: @eestatuscode.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /eestatuscodes/1
  # PATCH/PUT /eestatuscodes/1.json
  def update
    respond_to do |format|
      if @eestatuscode.update(eestatuscode_params)
        format.html { redirect_to @eestatuscode, notice: 'Eestatuscode was successfully updated.' }
        format.json { render :show, status: :ok, location: @eestatuscode }
      else
        format.html { render :edit }
        format.json { render json: @eestatuscode.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /eestatuscodes/1
  # DELETE /eestatuscodes/1.json
  def destroy
    @eestatuscode.destroy
    respond_to do |format|
      format.html { redirect_to eestatuscodes_url, notice: 'Eestatuscode was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_eestatuscode
      @eestatuscode = Eestatuscode.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def eestatuscode_params
      params.require(:eestatuscode).permit(:client, :statuscode, :startdate, :enddate)
    end
end
