class OpenvoucherController < ApplicationController
  def index
	   @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "pdf-printed",
          orientation: "landscape",
          :footer => { :right => '[page] of [topage]' }
       end
    end
    
  end
end
