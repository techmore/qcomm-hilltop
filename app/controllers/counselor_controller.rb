class CounselorController < ApplicationController
  def index
     @authorizations = Authorization.all
     @clients = Client.all
     @interventions = Intervention.all
     @employees = Employee.all
  end
end
