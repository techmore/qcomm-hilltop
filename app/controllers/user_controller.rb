class UserController < ApplicationController
  def index
	  @users = User.all
    respond_to do |format|
       format.html
       format.csv { send_data @users.to_csv , filename: "users-#{Date.today}.csv"}
    end
  end

  def show
  end

  def delete
  end

  def edit
  end
end
