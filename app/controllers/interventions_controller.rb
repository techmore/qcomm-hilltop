class InterventionsController < ApplicationController
  before_action :set_intervention, only: [:show, :edit, :update, :destroy]
  # GET /interventions
  # GET /interventions.json
  def index
    @interventions = Intervention.all

    respond_to do |format|
       format.html
       format.csv { send_data @interventions.to_csv , filename: "interventions-#{Date.today}.csv"}
    end
  end

  def deleted
    @interventions = Intervention.only_deleted
  end

  # GET /interventions/1
  # GET /interventions/1.json
  def show
    @employees = Employee.all
  end

  # GET /interventions/new
  def new
    @intervention = Intervention.new
    @interventions = Intervention.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @authid = params[:authid]
    @jobRequirements_old = params[:jobRequirements_old]
    @skillsStatus_old = params[:skillsStatus_old]
    @interventionDescription_old = params[:interventionDescription]
    @clientProgress_old = params[:clientProgress]
    @generalComments_old = params[:generalComments]
    @serviceType = params[:serviceType]
  end

  # GET /interventions/1/edit
  def edit
    @employees = Employee.all
    @authid = params[:authid]
    @interventions = Intervention.all
    @authorizations = Authorization.all

  end

  # POST /interventions
  # POST /interventions.json
  def create
    @intervention = Intervention.new(intervention_params)
    @authid = params[:authid]
    @employees = Employee.all

    respond_to do |format|
      if @intervention.save
         format.html { redirect_to @intervention, notice: 'Intervention was successfully created.' }
	# <td>  <button type="button" class="btn btn-light"><%= link_to "Calendar", controller: "calendar", action: "oct", employee: "#{employee.id}" %></button> </td>
        #format.html { redirect_to "authorizations", action: @authid  }
       format.json { render :show, status: :created, location: @intervention }
	open('log.txt', 'a') do |f|; f <<  Time.now.to_s + " " + current_user.email + " " + request.remote_ip + " CREATED intervention " + @intervention.id.to_s + "\n"; end

      else
        format.html { render :new }
        format.json { render json: @intervention.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /interventions/1
  # PATCH/PUT /interventions/1.json
  def update
	       @employees = Employee.new

    respond_to do |format|
      if @intervention.update(intervention_params)
        format.html { redirect_to @intervention, notice: 'Intervention was successfully updated.' }
        format.json { render :show, status: :ok, location: @intervention }
      else
        format.html { render :edit }
        format.json { render json: @intervention.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /interventions/1
  # DELETE /interventions/1.json
  def destroy
     @authid = params[:authid]
    @intervention.destroy
    respond_to do |format|
      #format.html { redirect_to interventions_url, notice: 'Intervention was successfully destroyed.' }
      format.html { redirect_to :controller => "authorizations", :action => @authid, notice: '@authid'  }
      # format.html { redirect_to "/authorizations/"@authid }
 
  
       #format.html { redirect_to @authorizations, notice: 'Intervention was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def import
   Intervention.import(params[:file])
   redirect_to interventions_path, notice: "Added Successfully"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_intervention
      @intervention = Intervention.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def intervention_params
      params.require(:intervention).permit(:interventionID, :employeeID, :EmploymentHistoryID, :employerID, :workAuthorizationID, :dateOfIntervention, :startTime, :endTime, :jobRequirements, :skillsStatus, :interventionDescription, :clientProgress, :generalComments, :prePlacementActivityID, :ActivityConductedID, :service)
    end
end
