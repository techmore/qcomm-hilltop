class ActivityreportsController < ApplicationController
  before_action :set_activityreport, only: [:show, :edit, :update, :destroy]

  # GET /activityreports
  # GET /activityreports.json
  def index
    @activityreports = Activityreport.all
    
    respond_to do |format|
       format.html
       format.csv { send_data @activityreports.to_csv , filename: "activityreports-#{Date.today}.csv"}
    end
  end

  # GET /activityreports/1
  # GET /activityreports/1.json
  def show
    respond_to do |format|
     format.html
     format.pdf do
       render pdf: "pdf-printed"
     end
   end
  end

  # GET /activityreports/new
  def new
    @activityreport = Activityreport.new
    @clientid = params[:clientid]
    @authid = params[:authid]
    @job_goal = params[:job_goal]
    @voucherid = params[:voucherid]
    @inter_dates = params[:inter_dates]

    @clientemployer = Clientemployer.all
    @employer = Employer.all
    @interventions = Intervention.all
    @clients = Client.all
  end

  # GET /activityreports/1/edit
  def edit
  end

  # POST /activityreports
  # POST /activityreports.json
  def create
    @activityreport = Activityreport.new(activityreport_params)

    respond_to do |format|
      if @activityreport.save
        format.html { redirect_to @activityreport, notice: 'Activityreport was successfully created.' }
        format.json { render :show, status: :created, location: @activityreport }
      else
        format.html { render :new }
        format.json { render json: @activityreport.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /activityreports/1
  # PATCH/PUT /activityreports/1.json
  def update
    respond_to do |format|
      if @activityreport.update(activityreport_params)
        format.html { redirect_to @activityreport, notice: 'Activityreport was successfully updated.' }
        format.json { render :show, status: :ok, location: @activityreport }
      else
        format.html { render :edit }
        format.json { render json: @activityreport.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /activityreports/1
  # DELETE /activityreports/1.json
  def destroy
    @activityreport.destroy
    respond_to do |format|
      format.html { redirect_to activityreports_url, notice: 'Activityreport was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def import
   Activityreport.import(params[:file])
   redirect_to activityreports_path, notice: "Activity Reportss Added Successfully"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_activityreport
      @activityreport = Activityreport.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def activityreport_params
      params.require(:activityreport).permit(:clientid, :authid, :voucher_num, :name, :date, :reporting_period, :invoice, :hours_provided, :dvrs_office, :job_goal, :completed_by, :areas, :activities, :pp, :comments, :service, :requested_hours, :hours_wanted, :emp_name, :job_title, :emp_address, :telephone, :emp_start, :work_schedule, :emp_sup, :wages, :medical, :date_medical_start, :med_provided, :option1, :option2, :option3)
    end
end
