class UsersController < ApplicationController
  def index
    @users = User.all
    respond_to do |format|
       format.html
       format.csv { send_data @users.to_csv , filename: "user_accounts-#{Date.today}.csv"}
    end

  end
  def show
     @user = User.find_by_id(params[:id])
  end

  def import
     User.import(params[:file])
     redirect_to users_path, notice: "Added Successfully"
  end

  def destroy
    @user = User.find(params[:id])

    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_index_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

end
