class PpController < ApplicationController
  def index
    @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
  end
end
