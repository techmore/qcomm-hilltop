class OtfcompletionsController < ApplicationController
  before_action :set_otfcompletion, only: [:show, :edit, :update, :destroy]

  # GET /otfcompletions
  # GET /otfcompletions.json
  def index
    @otfcompletions = Otfcompletion.all
  end

  # GET /otfcompletions/1
  # GET /otfcompletions/1.json
  def show
    @otfs = Otf.all
    @authorizations = Authorization.all
    @clients = Client.all
    @employees = Employee.all
    respond_to do |format|
     format.html
     format.pdf do
       render pdf: "pdf-printed",
          
           footer: {
             font_size: 6,
             left: 'Form: DVRS 2021' ,
             center: 'QComm' ,
             right: '[page] of [topage]' }
     end
   end

  end

  # GET /otfcompletions/new
  def new
    @otfcompletion = Otfcompletion.new
    @authid = params[:authid]
    @clientid = params[:clientid]
    @serviceType = params[:serviceType]
    @employers = Employer.all
    @employees = Employee.all
    @clients = Client.all
  end

  # GET /otfcompletions/1/edit
  def edit
  end

  # POST /otfcompletions
  # POST /otfcompletions.json
  def create
    @otfcompletion = Otfcompletion.new(otfcompletion_params)

    respond_to do |format|
      if @otfcompletion.save
        format.html { redirect_to @otfcompletion, notice: 'Otfcompletion was successfully created.' }
        format.json { render :show, status: :created, location: @otfcompletion }
      else
        format.html { render :new }
        format.json { render json: @otfcompletion.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /otfcompletions/1
  # PATCH/PUT /otfcompletions/1.json
  def update
    respond_to do |format|
      if @otfcompletion.update(otfcompletion_params)
        format.html { redirect_to @otfcompletion, notice: 'Otfcompletion was successfully updated.' }
        format.json { render :show, status: :ok, location: @otfcompletion }
      else
        format.html { render :edit }
        format.json { render json: @otfcompletion.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /otfcompletions/1
  # DELETE /otfcompletions/1.json
  def destroy
    @otfcompletion.destroy
    respond_to do |format|
      format.html { redirect_to otfcompletions_url, notice: 'Otfcompletion was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_otfcompletion
      @otfcompletion = Otfcompletion.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def otfcompletion_params
      params.require(:otfcompletion).permit(:formtype, :client, :authid, :agency, :vrc, :siteName1, :Address1, :Dates1, :specialist1, :totalSiteTime1, :numberOfDays1, :aveHoursPerDay1, :JobsPerformed1, :siteName2, :Address2, :Dates2, :specialist2, :totalSiteTime2, :numberOfDays2, :aveHoursPerDay2, :JobsPerformed2, :siteName3, :Address3, :Dates3, :specialist3, :totalSiteTime3, :numberOfDays3, :aveHoursPerDay3, :JobsPerformed3, :summary, :comments, :q1, :q2, :q3, :q4, :q5, :q6, :signature)
    end
end
