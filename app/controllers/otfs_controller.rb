class OtfsController < ApplicationController
  before_action :set_otf, only: [:show, :edit, :update, :destroy]

  # GET /otfs
  # GET /otfs.json
  def index
    @otfs = Otf.all
     @authid = params[:authid]
    @serviceType = params[:serviceType]
    @employees = Employee.all
  end

  # GET /otfs/1
  # GET /otfs/1.json
  def show
     @authid = params[:authid]
    @serviceType = params[:serviceType]
    @employees = Employee.all
  end

  # GET /otfs/new
  def new
    @otf = Otf.new
    @authid = params[:authid]
    @serviceType = params[:serviceType]
    @employees = Employee.all
  end

  # GET /otfs/1/edit
  def edit
    @otfs = Otf.all
     @authid = params[:authid]
    @serviceType = params[:serviceType]
    @employees = Employee.all
  end

  # POST /otfs
  # POST /otfs.json
  def create
    @otf = Otf.new(otf_params)
     @authid = params[:authid]
    @serviceType = params[:serviceType]
    @employees = Employee.all
    respond_to do |format|
      if @otf.save
        format.html { redirect_to @otf, notice: 'Otf was successfully created.' }
        format.json { render :show, status: :created, location: @otf }
      else
        format.html { render :new }
        format.json { render json: @otf.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /otfs/1
  # PATCH/PUT /otfs/1.json
  def update
    respond_to do |format|
      if @otf.update(otf_params)
        format.html { redirect_to @otf, notice: 'Otf was successfully updated.' }
        format.json { render :show, status: :ok, location: @otf }
      else
        format.html { render :edit }
        format.json { render json: @otf.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /otfs/1
  # DELETE /otfs/1.json
  def destroy
    @otf.destroy
    respond_to do |format|
      format.html { redirect_to otfs_url, notice: 'Otf was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_otf
      @otf = Otf.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def otf_params
      params.require(:otf).permit(:date, :startTime, :endTime, :authid, :employee, :employer, :emp1, :emp2, :emp3, :emp4, :emp5, :emp6, :emp7, :emp8, :emp9, :emp10, :emp11, :emp12, :emp13, :emp14, :emp15, :emp16, :emp17, :emp18, :emp19, :emp20, :emp21, :emp22, :emp23, :phy1, :phy2, :phy3, :phy4, :phy5, :phy6, :phy7, :phy8, :phy9, :phy10, :env1, :env2, :env3, :env4, :env5, :emp1comment, :emp2comment, :emp3comment, :emp4comment, :emp5comment, :emp6comment, :emp7comment, :emp8comment, :emp9comment, :emp10comment, :emp11comment, :emp12comment, :emp13comment, :emp14comment, :emp15comment, :emp16comment, :emp17comment, :emp18comment, :emp19comment, :emp20comment, :emp21comment, :emp22comment, :emp23comment, :phy1comment, :phy2comment, :phy3comment, :phy4comment, :phy5comment, :phy6comment, :phy7comment, :phy8comment, :phy9comment, :phy10comment, :env1comment, :env2comment, :env3comment, :env4comment, :env5comment, :env6comment)
    end
end
