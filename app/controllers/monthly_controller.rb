class MonthlyController < ApplicationController
  def index
     @clients = Client.all
     @authorizations = Authorization.all
     @interventions = Intervention.all
  end
  def myaction
    year = Date.today.year
    age = params[:my_age]
    @birth_year = year - age
end
end
