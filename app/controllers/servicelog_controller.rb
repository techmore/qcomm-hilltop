class ServicelogController < ApplicationController
  def cbwe
  end

  def twe
    @eestatuscodes = Eestatuscode.all
      @clients = Client.all
      @employees = Employee.all
      @interventions = Intervention.all
      @clientemployers = Clientemployer.all
      @employers = Employer.all
      @clientid = params[:clientid]
      @otf = Otf.all
      @otfcompletion = Otfcompletion.all
      @hourlyrate = Hourlyrate.all
      @authid = params[:authid]
      @goal = params[:goal]
      @counselor = params[:counselor]
      @invoice = params[:invoice]

      respond_to do |format|
              format.html
        format.pdf do
           render pdf: "authorization"
       #       :disposition => 'attachment'
        end
      end
  end

  def ee
  end

  def ltfa
    @eestatuscodes = Eestatuscode.all
      @clients = Client.all
      @employees = Employee.all
      @interventions = Intervention.all
      @clientemployers = Clientemployer.all
      @employers = Employer.all
      @clientid = params[:clientid]
      @otf = Otf.all
      @otfcompletion = Otfcompletion.all
      @hourlyrate = Hourlyrate.all
      @authid = params[:authid]
      @goal = params[:goal]
      @counselor = params[:counselor]
      @invoice = params[:invoice]

      respond_to do |format|
              format.html
        format.pdf do
           render pdf: "authorization"
       #       :disposition => 'attachment'
        end
      end
  end

  def ijc
    @eestatuscodes = Eestatuscode.all
      @clients = Client.all
      @employees = Employee.all
      @interventions = Intervention.all
      @clientemployers = Clientemployer.all
      @employers = Employer.all
      @clientid = params[:clientid]
      @otf = Otf.all
      @otfcompletion = Otfcompletion.all
      @hourlyrate = Hourlyrate.all
      @authid = params[:authid]
      @goal = params[:goal]
      @counselor = params[:counselor]
      @invoice = params[:invoice]

      respond_to do |format|
              format.html
        format.pdf do
           render pdf: "authorization"
       #       :disposition => 'attachment'
        end
      end
  end

  def pp
    @eestatuscodes = Eestatuscode.all
      @clients = Client.all
      @employees = Employee.all
      @interventions = Intervention.all
      @clientemployers = Clientemployer.all
      @employers = Employer.all
      @clientid = params[:clientid]
      @otf = Otf.all
      @otfcompletion = Otfcompletion.all
      @hourlyrate = Hourlyrate.all
      @authid = params[:authid]
      @goal = params[:goal]
      @counselor = params[:counselor]
      @invoice = params[:invoice]

      respond_to do |format|
              format.html
        format.pdf do
           render pdf: "authorization"
       #       :disposition => 'attachment'
        end
      end
  end

  def ddd
  end

  def dddpp
  end

  def preets
  end

  def schooleval
  end
end
