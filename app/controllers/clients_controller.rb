class ClientsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_client, only: [:show, :edit, :update, :destroy]


  # GET /clients
  # GET /clients.json
  #
  def deleted 
    @clients = Client.only_deleted
  end


  def index
    @clients = Client.all
    respond_to do |format|
       format.html
       format.csv { send_data @clients.to_csv , filename: "clients-#{Date.today}.csv"}
    end

  end

  # GET /clients/1
  # GET /clients/1.json
  def show
        @employees = Employee.all
      @authorizations = Authorization.all
      @clientemployers = Clientemployer.all
      @employers = Employer.all
  end

  # GET /clients/new
  def new
    @client = Client.new
  end

  # GET /clients/1/edit
  def edit
  end

  # POST /clients
  # POST /clients.json
  def create
    @client = Client.new(client_params)

    respond_to do |format|
      if @client.save
        format.html { redirect_to @client, notice: 'Client was successfully created.' }
        format.json { render :show, status: :created, location: @client }
      else
        format.html { render :new }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /clients/1
  # PATCH/PUT /clients/1.json
  def update
    respond_to do |format|
      if @client.update(client_params)
        format.html { redirect_to @client, notice: 'Client was successfully updated.' }
        format.json { render :show, status: :ok, location: @client }
      else
        format.html { render :edit }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /clients/1
  # DELETE /clients/1.json
  def destroy
    @client.destroy
    respond_to do |format|
      format.html { redirect_to clients_url, notice: 'Client was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def import
   Client.import(params[:file])
   redirect_to clients_path, notice: "Added Successfully"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client
      @client = Client.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_params
      params.require(:client).permit(:clientID, :clientFirstName, :clientLastName, :clientMiddleName, :ClientRace, :ClientGender, :clientAddress1, :clientAddress2, :clientCity, :clientCounty, :clientState, :clientZip, :clientHomePhone, :clientMobilePhone, :clientWorkPhone, :clientWorkPhoneExt, :ClientLivingArrangment, :ClientCommMethod, :ClientProgram, :clientEmail, :clientSSN, :clientDOB, :ClientStartDate, :ClientFAHours, :ClientFAStartDate, :ClientASL, :ClientServiceStopDate, :ClientTypeOfTermination, :ClientReferralDate, :ClientReferredBy, :ReferralSourceNumber, :ReferralPhoneExt, :ClientPrimaryImpairment, :ClientPrimarySource, :ClientSecondaryImpairment, :ClientSecondarySource, :ClientPrimaryMHDisability, :ClientSecondaryMHDisability, :ClientAllergies, :ClientPrimaryNonMHDisability, :ClientSecondaryNonMHDisability, :ClientMedicalInformation, :ClientEducationLevel, :ClientResume, :CriminalRecord, :CriminalRecordDetail, :ClientNotes, :DVRSCaseIDNbr, :DDDSerialNbr, :EmergencyContact1Name, :EmergencyContact1Relationship, :EmergencyContact1Home, :EmergencyContact1Work, :EmergencyContact1WorkExt, :EmergencyContact1Cell, :EmergencyContact2Name, :EmergencyContact2Relationship, :EmergencyContact2Home, :EmergencyContact2Work, :EmergencyContact2WorkExt, :EmergencyContact2Cell, :GuardianName, :GuardinaRelationship, :GuardianHome, :GuardianWork, :GuardianWorkExt, :GuardianCell, :GuardianAddress1, :GuardianAddress2, :GuardianCity, :GuardianState, :GuardianZip, :Resume, :Googledrive, :participantID )
    end
end
