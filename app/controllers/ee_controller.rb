class EeController < ApplicationController
  def index
    @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
  end

  def report
    @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
    @eestatuscodes = Eestatuscode.all
    @hourlyrates = Hourlyrate.all
    @interventions = Intervention.all
    respond_to do |format|
              format.html
        format.pdf do
           render pdf: "report",
           orientation:                    'Landscape'
       #       :disposition => 'attachment'
        end
      end

  end
end
