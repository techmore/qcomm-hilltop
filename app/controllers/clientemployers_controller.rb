class ClientemployersController < ApplicationController
  before_action :set_clientemployer, only: [:show, :edit, :update, :destroy]

  # GET /clientemployers
  # GET /clientemployers.json
  def index
    @clientemployers = Clientemployer.all
  
    respond_to do |format|
       format.html
       format.csv { send_data @clientemployers.to_csv , filename: "clientemployers-#{Date.today}.csv"}
    end
  end

  def deleted
    @clientemployers = Clientemployer.only_deleted
  end

  # GET /clientemployers/1
  # GET /clientemployers/1.json
  def show
  end

  # GET /clientemployers/new
  def new
    @clientid = params[:clientid]
    @clients = Client.all
        @employers = Employer.all
    @clientemployer = Clientemployer.new
  end

  # GET /clientemployers/1/edit
  def edit
	  @clientid = params[:clientid]
      @clients = Client.all
  end

  # POST /clientemployers
  # POST /clientemployers.json
  def create
    @clientemployer = Clientemployer.new(clientemployer_params)

    respond_to do |format|
      if @clientemployer.save
        format.html { redirect_to @clientemployer, notice: 'Clientemployer was successfully created.' }
        format.json { render :show, status: :created, location: @clientemployer }
      else
        format.html { render :new }
        format.json { render json: @clientemployer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /clientemployers/1
  # PATCH/PUT /clientemployers/1.json
  def update
    respond_to do |format|
      if @clientemployer.update(clientemployer_params)
        format.html { redirect_to @clientemployer, notice: 'Clientemployer was successfully updated.' }
        format.json { render :show, status: :ok, location: @clientemployer }
      else
        format.html { render :edit }
        format.json { render json: @clientemployer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /clientemployers/1
  # DELETE /clientemployers/1.json
  def destroy
    @clientemployer.destroy
    respond_to do |format|
      format.html { redirect_to clientemployers_url, notice: 'Clientemployer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def import
   Clientemployer.import(params[:file])
   redirect_to clientemployers_path, notice: "Added Successfully"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_clientemployer
      @clientemployer = Clientemployer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def clientemployer_params
      params.require(:clientemployer).permit(:clientID, :clientEmployerID, :startDate, :endDate, :hoursPerMonth, :JobTitle, :SupervisorName, :SupervisorPhone, :WorkSchedule, :WorkTransportation, :Wage, :WageUnit, :GetMedical, :MedicalType, :DateMedicalStart, :TerminationType, :TerminationReason, :EmployersNotes, :OccupationCode, :SEModel, :TypeofJob)
    end
end
