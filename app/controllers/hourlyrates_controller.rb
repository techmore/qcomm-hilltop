class HourlyratesController < ApplicationController
  before_action :set_hourlyrate, only: [:show, :edit, :update, :destroy]

  # GET /hourlyrates
  # GET /hourlyrates.json
  def index
    @hourlyrates = Hourlyrate.all
  end

  # GET /hourlyrates/1
  # GET /hourlyrates/1.json
  def show
  end

  # GET /hourlyrates/new
  def new
    @hourlyrate = Hourlyrate.new
        @clientid = params[:clientid]
    @clients = Client.all
p
  end

  # GET /hourlyrates/1/edit
  def edit
  end

  # POST /hourlyrates
  # POST /hourlyrates.json
  def create
    @hourlyrate = Hourlyrate.new(hourlyrate_params)

    respond_to do |format|
      if @hourlyrate.save
        format.html { redirect_to @hourlyrate, notice: 'Hourlyrate was successfully created.' }
        format.json { render :show, status: :created, location: @hourlyrate }
      else
        format.html { render :new }
        format.json { render json: @hourlyrate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hourlyrates/1
  # PATCH/PUT /hourlyrates/1.json
  def update
    respond_to do |format|
      if @hourlyrate.update(hourlyrate_params)
        format.html { redirect_to @hourlyrate, notice: 'Hourlyrate was successfully updated.' }
        format.json { render :show, status: :ok, location: @hourlyrate }
      else
        format.html { render :edit }
        format.json { render json: @hourlyrate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hourlyrates/1
  # DELETE /hourlyrates/1.json
  def destroy
    @hourlyrate.destroy
    respond_to do |format|
      format.html { redirect_to hourlyrates_url, notice: 'Hourlyrate was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hourlyrate
      @hourlyrate = Hourlyrate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def hourlyrate_params
      params.require(:hourlyrate).permit(:client, :hourlyrate, :startdate, :enddate)
    end
end
