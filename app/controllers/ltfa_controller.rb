class LtfaController < ApplicationController
  def index
    @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
  end

  def show

	  @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all


    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "pdf-printed",
        	orientation: "landscape",
        	:footer => { :right => '[page] of [topage]' }
       end
    end
  end

  def jan
    @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all


    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "pdf-printed",
        	orientation: "landscape",
        	:footer => { :right => '[page] of [topage]' }
       end
    end
  end



  def feb
    @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "pdf-printed",
          orientation: "landscape",
          :footer => { :right => '[page] of [topage]' }
       end
    end
  end

  def mar
    @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "pdf-printed",
          orientation: "landscape",
          :footer => { :right => '[page] of [topage]' }
       end
    end
  end

  def apr
    @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "pdf-printed",
          orientation: "landscape",
          :footer => { :right => '[page] of [topage]' }
       end
    end
  end

  def may
    @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "pdf-printed",
          orientation: "landscape",
          :footer => { :right => '[page] of [topage]' }
       end
    end
  end

  def jun
    @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "pdf-printed",
          orientation: "landscape",
          :footer => { :right => '[page] of [topage]' }
       end
    end
  end

  def jul
    @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "pdf-printed",
          orientation: "landscape",
          :footer => { :right => '[page] of [topage]' }
       end
    end
  end

  def aug
    @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "pdf-printed",
          orientation: "landscape",
          :footer => { :right => '[page] of [topage]' }
       end
    end
  end

  def sep
    @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "pdf-printed",
          orientation: "landscape",
          :footer => { :right => '[page] of [topage]' }
       end
    end
  end

  def oct
    @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "pdf-printed",
          orientation: "landscape",
          :footer => { :right => '[page] of [topage]' }
       end
    end
  end

  def nov
    @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "pdf-printed",
          orientation: "landscape",
          :footer => { :right => '[page] of [topage]' }
       end
    end
  end

  def dec
    @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "pdf-printed",
          orientation: "landscape",
          :footer => { :right => '[page] of [topage]' }
       end
    end
  end


  def ltfatwentytwenty
    @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
  end

  def julynineteen
	      @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
  end


  def junnineteen
              @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
  end


  def augnineteen
              @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
  end


  def sepnineteen
              @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
  end


  def octnineteen
              @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
  end


  def novnineteen
              @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
  end



  def decnineteen
              @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
    @clientemployers = Clientemployer.all
    @employers = Employer.all
  end



end
