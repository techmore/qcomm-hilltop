class VotltfasController < ApplicationController
  before_action :set_votltfa, only: [:show, :edit, :update, :destroy]

  # GET /votltfas
  # GET /votltfas.json
  def index
    @votltfas = Votltfa.all
  end

  # GET /votltfas/1
  # GET /votltfas/1.json
  def show
  end

  # GET /votltfas/new
  def new
    @votltfa = Votltfa.new
    @clientid = params[:clientid]
    @clients = Client.all
    @employees = Employee.all
  end

  # GET /votltfas/1/edit
  def edit
  end

  # POST /votltfas
  # POST /votltfas.json
  def create
    @votltfa = Votltfa.new(votltfa_params)

    respond_to do |format|
      if @votltfa.save
        format.html { redirect_to @votltfa, notice: 'Votltfa was successfully created.' }
        format.json { render :show, status: :created, location: @votltfa }
      else
        format.html { render :new }
        format.json { render json: @votltfa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /votltfas/1
  # PATCH/PUT /votltfas/1.json
  def update
    respond_to do |format|
      if @votltfa.update(votltfa_params)
        format.html { redirect_to @votltfa, notice: 'Votltfa was successfully updated.' }
        format.json { render :show, status: :ok, location: @votltfa }
      else
        format.html { render :edit }
        format.json { render json: @votltfa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /votltfas/1
  # DELETE /votltfas/1.json
  def destroy
    @votltfa.destroy
    respond_to do |format|
      format.html { redirect_to votltfas_url, notice: 'Votltfa was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_votltfa
      @votltfa = Votltfa.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def votltfa_params
      params.require(:votltfa).permit(:date, :provider, :client, :rate, :reason, :empsig, :providerstaff, :empname)
    end
end
