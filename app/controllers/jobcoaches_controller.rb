class JobcoachesController < ApplicationController
	  before_action :authenticate_user!

  def index
	    @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all

  end

end
