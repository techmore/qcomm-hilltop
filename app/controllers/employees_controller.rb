class EmployeesController < ApplicationController
  before_action :authenticate_user!

  before_action :set_employee, only: [:show, :edit, :update, :destroy]


  def deleted
    @employees = Employee.only_deleted
  end


  # GET /employees
  # GET /employees.json
  def index
    @employees = Employee.all

    respond_to do |format|
       format.html
       format.csv { send_data @employees.to_csv , filename: "employees-#{Date.today}.csv"}
    end
  end

  # GET /employees/1
  # GET /employees/1.json
  def show
    @clients = Client.all
    @authorizations = Authorization.all
    @employees = Employee.all
    @assignments = Assignment.all
    @interventions = Intervention.all
  end

  def fullname
          "#{clientFirstName}, #{clientLastName.clientFirstName}"
  end

  # GET /employees/new
  def new
    @employee = Employee.new
  end

  # GET /employees/1/edit
  def edit
  end

  # POST /employees
  # POST /employees.json
  def create
    @employee = Employee.new(employee_params)

    respond_to do |format|
      if @employee.save
        format.html { redirect_to @employee, notice: 'Employee was successfully created.' }
        format.json { render :show, status: :created, location: @employee }
      else
        format.html { render :new }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /employees/1
  # PATCH/PUT /employees/1.json
  def update
    respond_to do |format|
      if @employee.update(employee_params)
        format.html { redirect_to @employee, notice: 'Employee was successfully updated.' }
        format.json { render :show, status: :ok, location: @employee }
      else
        format.html { render :edit }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employees/1
  # DELETE /employees/1.json
  def destroy
    @employee.destroy
    respond_to do |format|
      format.html { redirect_to employees_url, notice: 'Employee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def import
   Employee.import(params[:file])
   redirect_to employees_path, notice: "Added Successfully"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee
      @employee = Employee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_params
      params.require(:employee).permit(:employee, :start_date, :end_date, :userType, :email, :ee, :signature)
    end
end
