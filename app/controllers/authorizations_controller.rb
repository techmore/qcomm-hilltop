class AuthorizationsController < ApplicationController
  before_action :set_authorization, only: [:show, :edit, :update, :destroy]

  # GET /authorizations
  # GET /authorizations.json
  def index
    @authorizations = Authorization.all

     respond_to do |format|
       format.html
       format.csv { send_data @authorizations.to_csv , filename: "authorizations-#{Date.today}.csv"}
    end
  end

  def deleted
    @authorizations = Authorization.only_deleted
  end


  # GET /authorizations/1
  # GET /authorizations/1.json
  def show
      @eestatuscodes = Eestatuscode.all
      @clients = Client.all
      @authorizations = Authorization.all
      @employees = Employee.all
      @assignments = Assignment.all
      @interventions = Intervention.all
      @clientemployers = Clientemployer.all
      @employers = Employer.all
      @activityreports = Activityreport.all
      @month = params[:month]
      @otf = Otf.all
      @otfcompletion = Otfcompletion.all
      @hourlyrate = Hourlyrate.all

      respond_to do |format|
	      format.html
        format.pdf do
	   render pdf: "authorization"
       #       :disposition => 'attachment'
	end
      end
  end

  def report_LTFA_TwentyTwenty
     @clients = Client.all
      @authorizations = Authorization.all
      @employees = Employee.all
      @assignments = Assignment.all
      @interventions = Intervention.all
      @clientemployers = Clientemployer.all
      @employers = Employer.all
      @activityreports = Activityreport.all
      @month = params[:month]

      respond_to do |format|
              format.html
        format.pdf do
           render pdf: "authorization"
       #       :disposition => 'attachment'
        end
      end
  end



  # GET /authorizations/new
  def new
    @authorization = Authorization.new
    @clients = Client.all
    @clientid = params[:clientid]
    @type = params[:type]
    @voucher = params[:voucher]
    @service = params[:service]
    @hours = params[:hours]
    @rate = params[:rate]


  end

  # GET /authorizations/1/edit
  def edit
    @clients = Client.all
    @voucherInvoiceDate_exist = params[:voucherInvoiceDate_exist]
    @serviceType = params[:serviceType]
    @voucher = params[:voucherNumber]
  end

  # POST /authorizations
  # POST /authorizations.json
  def create
    @authorization = Authorization.new(authorization_params)

    respond_to do |format|
      if @authorization.save
        format.html { redirect_to @authorization, notice: 'Authorization was successfully created.' }
        format.json { render :show, status: :created, location: @authorization }
      else
        format.html { render :new }
        format.json { render json: @authorization.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /authorizations/1
  # PATCH/PUT /authorizations/1.json
  def update
    respond_to do |format|
      if @authorization.update(authorization_params)
        format.html { redirect_to @authorization, notice: 'Authorization was successfully updated.' }
        format.json { render :show, status: :ok, location: @authorization }
      else
        format.html { render :edit }
        format.json { render json: @authorization.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /authorizations/1
  # DELETE /authorizations/1.json
  def destroy
    @authorization.destroy
    respond_to do |format|
      format.html { redirect_to authorizations_url, notice: 'Authorization was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
def import
   Authorization.import(params[:file])
   redirect_to authorizations_path, notice: "Added Successfully"
end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_authorization
      @authorization = Authorization.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def authorization_params
<<<<<<< HEAD
      params.require(:authorization).permit(:funderID, :clientID, :voucherNumber, :voucherOrAuthorizationDate, :counselorName, :serviceType, :hoursAuthorized, :hoursRemaining, :totalHoursSupported, :voucherInvoiceDate, :VoucherInvoiceNumber, :voucherPaymentReceivedDate, :voucherPaymentReceived, :voucherInvoiceAdjustment, :voucherPaymentComment, :voucherAmountInvoiced, :VoucherCompleted, :VoucherRate, :VoucherGoal, :VoucherReceivedDate, :VoucherFACounty, :VoucherStatus, :FlatRateVoucher, :invoiceDateJan, :invoiceDateFeb, :invoiceDateMar, :invoiceDateApr, :invoiceDateMay, :invoiceDateJun, :invoiceDateJul, :invoiceDateAug, :invoiceDateSep, :intake, :invoiceDateOct,
=======
      params.require(:authorization).permit(:funderID, :clientID, :voucherNumber, :voucherOrAuthorizationDate, :counselorName, :serviceType, :hoursAuthorized, :hoursRemaining, :totalHoursSupported, :voucherInvoiceDate, :VoucherInvoiceNumber, :voucherPaymentReceivedDate, :voucherPaymentReceived, :voucherInvoiceAdjustment, :voucherPaymentComment, :voucherAmountInvoiced, :VoucherCompleted, :VoucherRate, :VoucherGoal, :VoucherReceivedDate, :VoucherFACounty, :VoucherStatus, :FlatRateVoucher,, :invoiceDateJan, :invoiceDateFeb, :invoiceDateMar, :invoiceDateApr, :invoiceDateMay, :invoiceDateJun, :invoiceDateJul, :invoiceDateAug, :invoiceDateSep,
:invoiceDateOct,
>>>>>>> 3ca44d04a7333640d5d5ced73b3cafc4b971b92c
:invoiceDateNov,
:invoiceDateDec,

:invoiceAmountJan,
:invoiceAmountFeb,
:invoiceAmountMar,
:invoiceAmountApr,
:invoiceAmountMay,
:invoiceAmountJun,
:invoiceAmountJul,
:invoiceAmountAug,
:invoiceAmountSep,
:invoiceAmountOct,
:invoiceAmountNov,
:invoiceAmountDec,

:invoiceNumberJan,
:invoiceNumberFeb,
:invoiceNumberMar,
:invoiceNumberApr,
:invoiceNumberMay,
:invoiceNumberJun,
:invoiceNumberJul,
:invoiceNumberAug,
:invoiceNumberSep,
:invoiceNumberOct,
:invoiceNumberNov,
:invoiceNumberDec,

:paymentReceivedDateJan,
:paymentReceivedDateFeb,
:paymentReceivedDateMar,
:paymentReceivedDateApr,
:paymentReceivedDateMay,
:paymentReceivedDateJun,
:paymentReceivedDateJul,
:paymentReceivedDateAug,
:paymentReceivedDateSep,
:paymentReceivedDateOct,
:paymentReceivedDateNov,
:paymentReceivedDateDec,

:invoiceAdjustmentJan,
:invoiceAdjustmentFeb,
:invoiceAdjustmentMar,
:invoiceAdjustmentApr,
:invoiceAdjustmentMay,
:invoiceAdjustmentJun,
:invoiceAdjustmentJul,
:invoiceAdjustmentAug,
:invoiceAdjustmentSep,
:invoiceAdjustmentOct,
:invoiceAdjustmentNov,
:invoiceAdjustmentDec,
:paymentCommentJan,
:paymentCommentFeb,
:paymentCommentMar,
:paymentCommentApr,
:paymentCommentMay,
:paymentCommentJun,
:paymentCommentJul,
:paymentCommentAug,
:paymentCommentSep,
:paymentCommentOct,
:paymentCommentNov,
:paymentCommentDec)
    end
end
