class TwersController < ApplicationController
  before_action :set_twer, only: [:show, :edit, :update, :destroy]

  # GET /twers
  # GET /twers.json
  def index
    @twers = Twer.all
  end

  # GET /twers/1
  # GET /twers/1.json
  def show
  end

  # GET /twers/new
  def new
    @twer = Twer.new
    @clientid = params[:clientid]
    @clients = Client.all
    @employees = Employee.all
    @employers = Employer.all
  end

  # GET /twers/1/edit
  def edit
  end

  # POST /twers
  # POST /twers.json
  def create
    @twer = Twer.new(twer_params)

    respond_to do |format|
      if @twer.save
        format.html { redirect_to @twer, notice: 'Twer was successfully created.' }
        format.json { render :show, status: :created, location: @twer }
      else
        format.html { render :new }
        format.json { render json: @twer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /twers/1
  # PATCH/PUT /twers/1.json
  def update
    respond_to do |format|
      if @twer.update(twer_params)
        format.html { redirect_to @twer, notice: 'Twer was successfully updated.' }
        format.json { render :show, status: :ok, location: @twer }
      else
        format.html { render :edit }
        format.json { render json: @twer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /twers/1
  # DELETE /twers/1.json
  def destroy
    @twer.destroy
    respond_to do |format|
      format.html { redirect_to twers_url, notice: 'Twer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_twer
      @twer = Twer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def twer_params
      params.require(:twer).permit(:client, :employer, :days, :hours, :week1, :final, :duties, :q1, :q2, :q3, :q4, :wp1, :wp2, :wp3, :wp4, :wp5, :wp6, :wp7, :ss1, :ss2, :ss3, :ss4, :ss5, :ss6, :ss7, :ss8, :c1, :c2, :c3, :c4, :coachsig, :sidgdate)
    end
end
