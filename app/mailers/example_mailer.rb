class ExampleMailer < ApplicationMailer
  default from: "quantumcommunities@cea-nj.org"

  def sample_email(user)
    @punch = params[:punch]

    #@user = user
    # mail(to: @user.email, subject: 'Sample Email')
    mail(to: 'sdolbec@cea-nj.org', subject: 'Sample Email')
  end
end
