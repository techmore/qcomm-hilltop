class UserMailer < ApplicationMailer
  default from: 'quantumcommunities@cea-nj.org'

  def welcome_email
    @punch = params[:punch]

    @activeUser = Employee.where(id: @punch.employee)
    @activeUser.each do |active| 
      @user = active.email
    end


    @url  = 'http://quantumcommunities.cea-nj.org:3000'
    mail(to: @user, subject: 'Intervention on ' + @punch.date.to_s.last(5) )
  end
end
