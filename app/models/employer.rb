class Employer < ApplicationRecord
	  acts_as_paranoid

   def self.import(file)
     CSV.foreach(file.path, headers: true) do | row |
        Employer.create! row.to_hash
     end
   end

   def self.to_csv
    attributes = %w{id employerID	employerName	employerCompanyMainContact	employerAddress1	employerAddress2	employerCity	employerCounty	employerState	employerZip	employerWorkPhone	employerFaxNumber	employerEmail}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.find_each do |user|
        csv << attributes.map{ |attr| user.send(attr) }
      end
    end
  end

  def with_id
    "#{id} #{employerName}"
  end


end
