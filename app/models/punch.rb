class Punch < ApplicationRecord
	  acts_as_paranoid

          def self.to_csv
    attributes = %w{
    id
    date
employee
intime
outtime
service
description
}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.each do |user|
        csv << attributes.map{ |attr| user.send(attr) }
      end
    end
  end

def self.import(file)
     CSV.foreach(file.path, headers: true) do | row |
        Punch.create! row.to_hash
        Client.create! row.to_hash
     end
   end


end
