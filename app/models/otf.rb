class Otf < ApplicationRecord
    validates :date, presence: true
  validates :startTime, presence: true
  validates :endTime, presence: true
  validates :authid, presence: true
  validates :employee, presence: true
  validates :employer, presence: true
end
