class Activityreport < ApplicationRecord
  def self.to_csv
    attributes = %w{ 
clientid
authid
voucher_num
name
date
reporting_period
invoice
hours_provided
dvrs_office
job_goal
completed_by
areas
activities
pp
comments
service
requested_hours
hours_wanted
emp_name
job_title
emp_address
telephone
emp_start
work_schedule
emp_sup
wages
medical
date_medical_start
med_provided
option1
option2
option3

}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.each do |user|
        csv << attributes.map{ |attr| user.send(attr) }
      end
    end
  end


   def self.import(file)
     CSV.foreach(file.path, headers: true) do | row |
        Activityreport.create! row.to_hash
     end
   end


end
