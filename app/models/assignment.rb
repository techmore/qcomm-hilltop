class Assignment < ApplicationRecord
	  acts_as_paranoid

def self.to_csv
    attributes = %w{
    ClientID
Employee
DateAssigned
DateUnassigned
}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.each do |user|
        csv << attributes.map{ |attr| user.send(attr) }
      end
    end
  end




   def self.import(file)
     CSV.foreach(file.path, headers: true) do | row |
        Assignment.create! row.to_hash
     end
   end

end
