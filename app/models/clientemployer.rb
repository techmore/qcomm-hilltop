class Clientemployer < ApplicationRecord
	  acts_as_paranoid


          def self.to_csv
    attributes = %w{
clientID
clientEmployerID
startDate
endDate
hoursPerMonth
JobTitle
SupervisorName
SupervisorPhone
WorkSchedule
WorkTransportation
Wage
WageUnit
GetMedical
MedicalType
DateMedicalStart
TerminationType
TerminationReason
EmployersNotes
OccupationCode
SEModel
TypeofJob

    }

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.each do |user|
        csv << attributes.map{ |attr| user.send(attr) }
      end
    end
  end


   def self.import(file)
     CSV.foreach(file.path, headers: true) do | row |
        Clientemployer.create! row.to_hash
     end
   end
end 
