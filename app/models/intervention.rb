class Intervention < ApplicationRecord
	  acts_as_paranoid

    validates :dateOfIntervention, presence: true
    validates :employeeID, presence: true
    validates :workAuthorizationID, presence: true

    def self.to_csv
    attributes = %w{
interventionID
employeeID
EmploymentHistoryID
employerID
workAuthorizationID
dateOfIntervention
startTime
endTime
jobRequirements
skillsStatus
interventionDescription
clientProgress
generalComments
prePlacementActivityID
ActivityConductedID
    }

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.each do |user|
        csv << attributes.map{ |attr| user.send(attr) }
      end
    end
  end


   def self.import(file)
     CSV.foreach(file.path, headers: true) do | row |
        Intervention.create! row.to_hash
     end
   end

end
