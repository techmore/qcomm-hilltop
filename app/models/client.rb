class Client < ApplicationRecord
   acts_as_paranoid
   has_one_attached :referral
   has_one_attached :intake

def self.to_csv
    attributes = %w{
id
clientID
clientFirstName
clientLastName
clientMiddleName
ClientRace
ClientGender
clientAddress1
clientAddress2
clientCity
clientCounty
clientState
clientZip
clientHomePhone
clientMobilePhone
ClientProgram
clientEmail
clientSSN
clientDOB
ClientStartDate
ClientFAStartDate
ClientServiceStopDate
ClientTypeOfTermination
ClientReferralDate
ClientReferredBy
ReferralSourceNumber
ClientPrimaryImpairment
ClientPrimarySource
ClientSecondaryImpairment
ClientSecondarySource
ClientNotes
participantID
DDDSerialNbr
EmergencyContact1Name
EmergencyContact1Relationship
EmergencyContact1Home
EmergencyContact1Work
EmergencyContact1WorkExt
EmergencyContact1Cell
EmergencyContact2Name
EmergencyContact2Relationship
EmergencyContact2Home
EmergencyContact2Work
EmergencyContact2WorkExt
EmergencyContact2Cell
GuardianName
GuardinaRelationship
GuardianHome
GuardianWork
GuardianWorkExt
GuardianCell
GuardianAddress1
GuardianAddress2
GuardianCity
GuardianState
GuardianZip

}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.each do |user|
        csv << attributes.map{ |attr| user.send(attr) }
      end
    end
  end


  def self.import(file)
     CSV.foreach(file.path, headers: true) do | row |
        Client.create! row.to_hash
     end
   end

  def fullname
    "#{clientLastName}, #{clientFirstName}"
  end

 def fullname_with_id
    "#{id} - #{clientLastName}, #{clientFirstName}"
  end
end
