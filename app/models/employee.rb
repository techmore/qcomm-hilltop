class Employee < ApplicationRecord
	acts_as_paranoid

def self.to_csv
    attributes = %w{employee
email
userType
start_date
end_date
}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.each do |user|
        csv << attributes.map{ |attr| user.send(attr) }
      end
    end
  end




   def self.import(file)
     CSV.foreach(file.path, headers: true) do | row |
        Employee.create! row.to_hash
     end
   end

   def employee_with_id
   	 "#{id} - #{employee}"
   end

      def fullname
      "#{clientLastName}, #{clientFirstName}"
  end
end
