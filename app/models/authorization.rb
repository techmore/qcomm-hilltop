class Authorization < ApplicationRecord
   acts_as_paranoid

def self.to_csv
    attributes = %w{
    id
funderID
clientID
voucherNumber
voucherOrAuthorizationDate
counselorName
serviceType
hoursAuthorized
hoursRemaining
totalHoursSupported
voucherInvoiceDate

VoucherInvoiceNumber
voucherPaymentReceivedDate
voucherPaymentReceived
voucherInvoiceAdjustment
voucherPaymentComment
voucherAmountInvoiced
VoucherCompleted
VoucherRate
VoucherGoal
VoucherReceivedDate
VoucherFACounty
VoucherStatus
FlatRateVoucher

invoiceDateJan
invoiceDateFeb
invoiceDateMar
invoiceDateApr
invoiceDateMay
invoiceDateJun
invoiceDateJul
invoiceDateAug
invoiceDateSep
invoiceDateOct
invoiceDateNov
invoiceDateDec

invoiceAmountJan
invoiceAmountFeb
invoiceAmountMar
invoiceAmountApr
invoiceAmountMay
invoiceAmountJun
invoiceAmountJul
invoiceAmountAug
invoiceAmountSep
invoiceAmountOct
invoiceAmountNov
invoiceAmountDec

invoiceNumberJan
invoiceNumberFeb
invoiceNumberMar
invoiceNumberApr
invoiceNumberMay
invoiceNumberJun
invoiceNumberJul
invoiceNumberAug
invoiceNumberSep
invoiceNumberOct
invoiceNumberNov
invoiceNumberDec
paymentReceivedDateJan
paymentReceivedDateFeb
paymentReceivedDateMar
paymentReceivedDateApr
paymentReceivedDateMay
paymentReceivedDateJun
paymentReceivedDateJul
paymentReceivedDateAug
paymentReceivedDateSep
paymentReceivedDateOct
paymentReceivedDateNov
paymentReceivedDateDec

invoiceAdjustmentJan
invoiceAdjustmentFeb
invoiceAdjustmentMar
invoiceAdjustmentApr
invoiceAdjustmentMay
invoiceAdjustmentJun
invoiceAdjustmentJul
invoiceAdjustmentAug
invoiceAdjustmentSep
invoiceAdjustmentOct
invoiceAdjustmentNov
invoiceAdjustmentDec

paymentCommentJan
paymentCommentFeb
paymentCommentMar
paymentCommentApr
paymentCommentMay
paymentCommentJun
paymentCommentJul
paymentCommentAug
paymentCommentSep
paymentCommentOct
paymentCommentNov
paymentCommentDec
}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.each do |user|
        csv << attributes.map{ |attr| user.send(attr) }
      end
    end
  end



   def self.import(file)
     CSV.foreach(file.path, headers: true) do | row |
        Authorization.create! row.to_hash
     end
   end
   def fullname
      "#{clientLastName}, #{clientFirstName}"
  end
end
