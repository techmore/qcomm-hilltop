json.extract! assignment, :id, :ClientID, :Employee, :DateAssigned, :DateUnassigned, :created_at, :updated_at
json.url assignment_url(assignment, format: :json)
