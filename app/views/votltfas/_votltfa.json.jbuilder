json.extract! votltfa, :id, :date, :provider, :client, :rate, :reason, :empsig, :providerstaff, :empname, :created_at, :updated_at
json.url votltfa_url(votltfa, format: :json)
