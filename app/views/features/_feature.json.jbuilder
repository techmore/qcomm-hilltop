json.extract! feature, :id, :name, :description, :status, :votes, :internal, :public, :priority, :created_at, :updated_at
json.url feature_url(feature, format: :json)
