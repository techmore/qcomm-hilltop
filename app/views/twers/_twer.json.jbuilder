json.extract! twer, :id, :client, :employer, :days, :hours, :week1, :final, :duties, :q1, :q2, :q3, :q4, :wp1, :wp2, :wp3, :wp4, :wp5, :wp6, :wp7, :ss1, :ss2, :ss3, :ss4, :ss5, :ss6, :ss7, :ss8, :c1, :c2, :c3, :c4, :coachsig, :sidgdate, :created_at, :updated_at
json.url twer_url(twer, format: :json)
