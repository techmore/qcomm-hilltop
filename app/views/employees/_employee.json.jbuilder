json.extract! employee, :id, :employee, :state_date, :end_date, :created_at, :updated_at
json.url employee_url(employee, format: :json)
