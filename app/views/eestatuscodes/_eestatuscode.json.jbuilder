json.extract! eestatuscode, :id, :client, :statuscode, :startdate, :enddate, :created_at, :updated_at
json.url eestatuscode_url(eestatuscode, format: :json)
