json.extract! intervention, :id, :interventionID, :employeeID, :EmploymentHistoryID, :employerID, :workAuthorizationID, :dateOfIntervention, :startTime, :endTime, :jobRequirements, :skillsStatus, :interventionDescription, :clientProgress, :generalComments, :prePlacementActivityID, :ActivityConductedID, :created_at, :updated_at
json.url intervention_url(intervention, format: :json)
