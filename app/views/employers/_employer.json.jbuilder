json.extract! employer, :id, :employerID, :employerName, :employerCompanyMainContact, :employerAddress1, :employerAddress2, :employerCity, :employerCounty, :employerState, :employerZip, :employerWorkPhone, :employerFaxNumber, :employerEmail, :created_at, :updated_at
json.url employer_url(employer, format: :json)
