json.extract! hourlyrate, :id, :client, :hourlyrate, :startdate, :enddate, :created_at, :updated_at
json.url hourlyrate_url(hourlyrate, format: :json)
