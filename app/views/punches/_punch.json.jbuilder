json.extract! punch, :id, :date, :employee, :intime, :outtime, :service, :description, :created_at, :updated_at
json.url punch_url(punch, format: :json)
