#!/bin/bash

git clone https://gitlab.com/techmore/qcomm-hilltop.git
cd qcomm-hilltop/
bundle install --jobs=16
yarn install --check-files
rails db:create
rails db:setup
rails db:migrate
rails db:seed
rails active_storage:install
rails db:migrate

echo "gem 'wicked_pdf'" >> Gemfile
echo "gem 'wkhtmltopdf-binary'" >> Gemfile

# cd qcomm-hilltop/
# bundle exec thin start -p 4000 & 