Rails.application.routes.draw do
  get 'servicelog/cbwe'
  get 'servicelog/twe'
  get 'servicelog/ee'
  get 'servicelog/ltfa'
  get 'servicelog/ijc'
  get 'servicelog/pp'
  get 'servicelog/ddd'
  get 'servicelog/dddpp'
  get 'servicelog/preets'
  get 'servicelog/schooleval'
  resources :votltfas
  resources :twers
  resources :otfs
  resources :features
  resources :otfcompletions
  resources :hourlyrates
  resources :eestatuscodes
  get 'ee/index'
  get 'ee/report'
  match '/users',   to: 'users#index',   via: 'get'

  get 'users/index'
  get 'preets/index'
  get 'restab/index'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

get 'ltfa/julynineteen'
get 'ltfa/junnineteen'
get 'ltfa/augnineteen'
get 'ltfa/sepnineteen'
get 'ltfa/sepnineteen'
get 'ltfa/octnineteen'
get 'ltfa/novnineteen'
get 'ltfa/novnineteen'
get 'ltfa/decnineteen'
get 'authorizations/deleted'
get 'interventions/deleted'
get 'employers/deleted'
get 'assignments/deleted'
get 'clientemployer/deleted'
get 'jobcoach/deleted'
get 'punches/deleted'

  get 'grants/index'
  get 'grant/home'


  resources :staffs do
          collection { post :import }
  end
  resources :devices do
          collection { post :import }
  end
  get 'monthlybilled/index'

  get 'monthlybilled/jan2021'
  get 'monthlybilled/jan2020'
  get 'monthlybilled/jan2019'
  get 'monthlybilled/jan2018'
  get 'monthlybilled/jan2017'
  get 'monthlybilled/jan2016'
  get 'monthlybilled/jan2015'
 
  get 'monthlybilled/feb2021'
  get 'monthlybilled/feb2020'
  get 'monthlybilled/feb2019'
  get 'monthlybilled/feb2018'
  get 'monthlybilled/feb2017'
  get 'monthlybilled/feb2016'
  get 'monthlybilled/feb2015'

  get 'monthlybilled/mar2021'
  get 'monthlybilled/mar2020'
  get 'monthlybilled/mar2019'
  get 'monthlybilled/mar2018'
  get 'monthlybilled/mar2017'
  get 'monthlybilled/mar2016'
  get 'monthlybilled/mar2015'

  get 'monthlybilled/apr2021'
  get 'monthlybilled/apr2020'
  get 'monthlybilled/apr2019'
  get 'monthlybilled/apr2018'
  get 'monthlybilled/apr2017'
  get 'monthlybilled/apr2016'
  get 'monthlybilled/apr2015'

  get 'monthlybilled/may2021'
  get 'monthlybilled/may2020'
  get 'monthlybilled/may2019'
  get 'monthlybilled/may2018'
  get 'monthlybilled/may2017'
  get 'monthlybilled/may2016'
  get 'monthlybilled/may2015'

  get 'monthlybilled/jun2021'
  get 'monthlybilled/jun2020'
  get 'monthlybilled/jun2019'
  get 'monthlybilled/jun2018'
  get 'monthlybilled/jun2017'
  get 'monthlybilled/jun2016'
  get 'monthlybilled/jun2015'

  get 'monthlybilled/jul2021'
  get 'monthlybilled/jul2020'
  get 'monthlybilled/jul2019'
  get 'monthlybilled/jul2018'
  get 'monthlybilled/jul2017'
  get 'monthlybilled/jul2016'
  get 'monthlybilled/jul2015'

  get 'monthlybilled/aug2021'
  get 'monthlybilled/aug2020'
  get 'monthlybilled/aug2019'
  get 'monthlybilled/aug2018'
  get 'monthlybilled/aug2017'
  get 'monthlybilled/aug2016'
  get 'monthlybilled/aug2015'

  get 'monthlybilled/sep2021'
  get 'monthlybilled/sep2020'
  get 'monthlybilled/sep2019'
  get 'monthlybilled/sep2018'
  get 'monthlybilled/sep2017'
  get 'monthlybilled/sep2016'
  get 'monthlybilled/sep2015'

  get 'monthlybilled/oct2021'
  get 'monthlybilled/oct2020'
  get 'monthlybilled/oct2019'
  get 'monthlybilled/oct2018'
  get 'monthlybilled/oct2017'
  get 'monthlybilled/oct2016'
  get 'monthlybilled/oct2015'

  get 'monthlybilled/nov2021'
  get 'monthlybilled/nov2020'
  get 'monthlybilled/nov2019'
  get 'monthlybilled/nov2018'
  get 'monthlybilled/nov2017'
  get 'monthlybilled/nov2016'
  get 'monthlybilled/nov2015'

  get 'monthlybilled/dec2021'
  get 'monthlybilled/dec2020'
  get 'monthlybilled/dec2019'
  get 'monthlybilled/dec2018'
  get 'monthlybilled/dec2017'
  get 'monthlybilled/dec2016'
  get 'monthlybilled/dec2015'

  get 'monthlybilled/year2015'
  get 'monthlybilled/year2016'
  get 'monthlybilled/year2017'
  get 'monthlybilled/year2018'
  get 'monthlybilled/year2019'
  get 'monthlybilled/year2020'
  get 'monthlybilled/year2021'



  get 'timesheets/index'

  get 'timesheets/jan2021'
  get 'timesheets/jan2020'
  get 'timesheets/jan2019'
  get 'timesheets/jan2018'
  get 'timesheets/jan2017'
  get 'timesheets/jan2016'
  get 'timesheets/jan2015'
  
  get 'timesheets/feb2021'
  get 'timesheets/feb2020'
  get 'timesheets/feb2019'
  get 'timesheets/feb2018'
  get 'timesheets/feb2017'
  get 'timesheets/feb2016'
  get 'timesheets/feb2015'

  get 'timesheets/mar2021'
  get 'timesheets/mar2020'
  get 'timesheets/mar2019'
  get 'timesheets/mar2018'
  get 'timesheets/mar2017'
  get 'timesheets/mar2016'
  get 'timesheets/mar2015'

  get 'timesheets/apr2021'
  get 'timesheets/apr2020'
  get 'timesheets/apr2019'
  get 'timesheets/apr2018'
  get 'timesheets/apr2017'
  get 'timesheets/apr2016'
  get 'timesheets/apr2015'

  get 'timesheets/may2021'
  get 'timesheets/may2020'
  get 'timesheets/may2019'
  get 'timesheets/may2018'
  get 'timesheets/may2017'
  get 'timesheets/may2016'
  get 'timesheets/may2015'

  get 'timesheets/jun2021'
  get 'timesheets/jun2020'
  get 'timesheets/jun2019'
  get 'timesheets/jun2018'
  get 'timesheets/jun2017'
  get 'timesheets/jun2016'
  get 'timesheets/jun2015'


  get 'timesheets/jul2021'
  get 'timesheets/jul2020'
  get 'timesheets/jul2019'
  get 'timesheets/jul2018'
  get 'timesheets/jul2017'
  get 'timesheets/jul2016'
  get 'timesheets/jul2015'

  get 'timesheets/aug2021'
  get 'timesheets/aug2020'
  get 'timesheets/aug2019'
  get 'timesheets/aug2018'
  get 'timesheets/aug2017'
  get 'timesheets/aug2016'
  get 'timesheets/aug2015'

  get 'timesheets/sep2021'
  get 'timesheets/sep2020'
  get 'timesheets/sep2019'
  get 'timesheets/sep2018'
  get 'timesheets/sep2017'
  get 'timesheets/sep2016'
  get 'timesheets/sep2015'

  get 'timesheets/oct2021'
  get 'timesheets/oct2020'
  get 'timesheets/oct2019'
  get 'timesheets/oct2018'
  get 'timesheets/oct2017'
  get 'timesheets/oct2016'
  get 'timesheets/oct2015'

  get 'timesheets/nov2021'
  get 'timesheets/nov2020'
  get 'timesheets/nov2019'
  get 'timesheets/nov2018'
  get 'timesheets/nov2017'
  get 'timesheets/nov2016'
  get 'timesheets/nov2015'

  get 'timesheets/dec2021'
  get 'timesheets/dec2020'
  get 'timesheets/dec2019'
  get 'timesheets/dec2018'
  get 'timesheets/dec2017'
  get 'timesheets/dec2016'
  get 'timesheets/dec2015'

  get 'timesheets/year2015'
  get 'timesheets/year2016'
  get 'timesheets/year2017'
  get 'timesheets/year2018'
  get 'timesheets/year2019'
  get 'timesheets/year2020'
  get 'timesheets/year2021'



  # get 'calendar/index'
  get 'calendar/jan2021'
  get 'calendar/jan2020'
  get 'calendar/jan2019'
  get 'calendar/jan2018'
  get 'calendar/jan2017'
  get 'calendar/jan2016'
  get 'calendar/jan2015'
  get 'calendar/jan'
  get 'calendar/feb'
  get 'calendar/feb2021'
  get 'calendar/feb2020'
  get 'calendar/feb2019'
  get 'calendar/feb2018'
  get 'calendar/feb2017'
  get 'calendar/feb2016'
  get 'calendar/feb2015'
  get 'calendar/mar'
  get 'calendar/mar2021'
  get 'calendar/mar2020'
  get 'calendar/mar2019'
  get 'calendar/mar2018'
  get 'calendar/mar2017'
  get 'calendar/mar2016'
  get 'calendar/mar2015'
  get 'calendar/apr'
  get 'calendar/apr2021'
  get 'calendar/apr2020'
  get 'calendar/apr2019'
  get 'calendar/apr2018'
  get 'calendar/apr2017'
  get 'calendar/apr2016'
  get 'calendar/apr2015'
  get 'calendar/may'
  get 'calendar/may2021'
  get 'calendar/may2020'
  get 'calendar/may2019'
  get 'calendar/may2018'
  get 'calendar/may2017'
  get 'calendar/may2015'
  get 'calendar/jun'
  get 'calendar/jun2021'
  get 'calendar/jun2020'
  get 'calendar/jun2019'
  get 'calendar/jun2018'
  get 'calendar/jun2017'
  get 'calendar/jun2016'
  get 'calendar/jun2015'
  get 'calendar/jul'
  get 'calendar/jul2021'
  get 'calendar/jul2020'
  get 'calendar/jul2019'
  get 'calendar/jul2018'
  get 'calendar/jul2017'
  get 'calendar/jul2016'
  get 'calendar/jul2015'
  get 'calendar/aug'
  get 'calendar/aug2021'
  get 'calendar/aug2020'
  get 'calendar/aug2019'
  get 'calendar/aug2018'
  get 'calendar/aug2017'
  get 'calendar/aug2016'
  get 'calendar/aug2015'
  get 'calendar/sep'
  get 'calendar/sep2021'
  get 'calendar/sep2020'
  get 'calendar/sep2019'
  get 'calendar/sep2018'
  get 'calendar/sep2017'
  get 'calendar/sep2016'
  get 'calendar/sep2015'
  get 'calendar/oct'
  get 'calendar/oct2021'
  get 'calendar/oct2020'
  get 'calendar/oct2019'
  get 'calendar/oct2018'
  get 'calendar/oct2017'
  get 'calendar/oct2016'
  get 'calendar/oct2015'
  get 'calendar/nov'
  get 'calendar/nov2021'
  get 'calendar/nov2020'
  get 'calendar/nov2019'
  get 'calendar/nov2018'
  get 'calendar/nov2017'
  get 'calendar/nov2016'
  get 'calendar/nov2015'
  get 'calendar/dec'
  get 'calendar/dec2021'
  get 'calendar/dec2020'
  get 'calendar/dec2019'
  get 'calendar/dec2018'
  get 'calendar/dec2017'
  get 'calendar/dec2016'
  get 'calendar/dec2015'
  get 'calendar/ddd_q2'
  get 'calendar/ddd_q3'
  get 'calendar/ddd_q4'
  get 'calendar/report'
  get 'calendar/report2021'
  get 'calendar/report2020'
  get 'calendar/report2019'
  get 'calendar/report2018'
  get 'calendar/report2017'
  get 'calendar/report2016'
  get 'calendar/report2015'

  resources :punches
  get 'ltfa/index'
  get 'ltfa/jan'
  get 'ltfa/feb'
  get 'ltfa/mar'
  get 'ltfa/apr'
  get 'ltfa/may'
  get 'ltfa/jun'
  get 'ltfa/jul'
  get 'ltfa/aug'
  get 'ltfa/sep'
  get 'ltfa/oct'
  get 'ltfa/nov'
  get 'ltfa/dec'
  get 'ltfa/ltfatwentytwenty'

  resources :activityreports
  get 'monthlybilled/index'
  devise_for :controllers
  resources :authorizations
  get 'unassigned/index'
  get 'logs/index'
  get 'ltfa/show'
  root 'pages#home'



  get 'ee/report'
  get 'authorizations/report_LTFA_TwentyTwenty'

    devise_for :users

  get 'openvoucher/index'
  get 'graphs/index'
  get 'reports/index'
  get 'monthly/index'
  get 'unpaid/index'
  get 'completed/index'
  get 'counselor/index'
  get 'pp/index'
  get 'csv/index'
  get 'timesheets/index'
  get 'cbwe/index'
  get 'ddd/index'
  get 'jobcoaches/index'
  get 'ltfa/index'
  get 'ltfa/namessn'
  get 'manager/index'
  get 'clients/deleted'
  get 'employees/aug'
  get 'jobcoaches/index'


  resources :employers do
          collection { post :import }
  end
  resources :clientemployers do
          collection { post :import }
  end
  resources :assignments do
          collection { post :import }
  end
  resources :employees do
          collection { post :import }
  end
  resources :clients do
          collection { post :import }
  end
  resources :authorizations do
          collection { post :import }
  end
  resources :interventions do
          collection { post :import }
  end
  resources :punches do
          collection { post :import }
  end
  resources :activityreports do
          collection { post :import }
  end
  resources :users do
          collection { post :import }
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
